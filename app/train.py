import numpy as np
import seaborn as sns
import yaml
import time
from collections import namedtuple
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
from sklearn.model_selection import KFold
from xgboost import XGBRegressor
from sklearn.metrics import mean_squared_error,r2_score, mean_absolute_error
from sklearn.svm import SVR
from sklearn.neural_network import MLPRegressor
from catboost import CatBoostRegressor
from sklearn.decomposition import PCA
from sklearn import preprocessing
from sklearn.linear_model import Lasso
from sklearn.model_selection import GridSearchCV

from utils.nmr_dataset import NmrDataset

def main():
  '''
  Loads a config file containing paths for train and holdout identifiers, with
  the path for feature set. Convert the config into a namedtuple so that
  keys can be used as function names and later create the NmrDataset used
  for training and evaluation
  '''
  with open('config.yml') as f:
    config = yaml.safe_load(f)
  
  config = namedtuple("NmrConfig", config.keys())(*config.values())
  nmr_dataset = NmrDataset(config=config)

  # Create train and holdout dataframes from train and holdout indexes
  train_dataset = nmr_dataset.get_train_dataset()
  holdout_dataset = nmr_dataset.get_holdout_dataset()

  # Create train and holdout features and labels from their dataframes
  train_features, holdout_features = nmr_dataset.get_features(train_dataset, 
    holdout_dataset)
  train_labels, holdout_labels = nmr_dataset.get_labels(train_dataset,
    holdout_dataset)
  # train_features = preprocessing.normalize(train_features, norm='l2')
  # holdout_features = preprocessing.normalize(holdout_features, norm='l2')
  # train_features, holdout_features = pca(train_features, holdout_features)

  model = train(train_features, train_labels.ravel())
  mae_holdout, r2_holdout, mse_holdout = predict(model, holdout_features, holdout_labels.ravel())
  mae_train, r2_train, mse_train = predict(model, train_features, train_labels.ravel())
  print("Holdout MAE={}; R2={}; MSE={}".format(mae_holdout, r2_holdout, mse_holdout))
  print("Train MAE={}; R2={}; MSE={}".format(mae_train, r2_train, mse_train))

  #cross_validate(nmr_dataset.full_dataset, 5)
  # corr(nmr_dataset.full_dataset)
  # pca(nmr_dataset.full_dataset)
  #grid_search(train_features, train_labels.ravel())

def create_model(hparams):
  model = CatBoostRegressor(
    learning_rate=h_params.learning_rate, 
    depth=h_params.depth, 
    n_estimators=h_params.n_estimators
  )
  return model

def train(features, labels):
  '''
  Train a GradientBoostingRegressor model using Gradient Boosted Trees
  with a number of estimators and max depth of trees and ratio of samples
  on features and labels
  '''
  c = 110
  ep = 5
  #model = XGBRegressor()
  #model = Lasso(alpha=0.0001)
  #model = RandomForestRegressor(n_estimators=1000)
  # model = GradientBoostingRegressor(n_estimators=500, max_depth=5, 
  #   subsample=0.8, loss='huber')
  model = GradientBoostingRegressor(n_estimators=500, max_depth=5, 
    subsample=0.8, loss='huber')
  # model = MLPRegressor(solver='adam', alpha=0.01, hidden_layer_sizes=(100, 50, 20, 10, 2), 
  #   random_state=0, learning_rate='adaptive', learning_rate_init=0.01,
  #   max_iter=10000, verbose=True, n_iter_no_change=50)
  #model = SVR(kernel="poly", C=100, gamma=0.1, epsilon=0.1)
  #model = CatBoostRegressor(learning_rate=0.1, depth=10, n_estimators=1000)
  model.fit(features, labels)
  return model

def grid_search(features, labels):
  param_test = {'max_depth':range(5,11,2), 'n_estimators': range(100,600,100)}
  g_search = GridSearchCV(estimator = GradientBoostingRegressor(subsample=0.8, loss='huber'), 
    param_grid = param_test, scoring='neg_root_mean_squared_error', n_jobs=4, cv=4, verbose=2)
  g_search.fit(features, labels)
  print(g_search.best_params_, g_search.best_score_)

def predict(model, features, true_labels=None):
  '''
  Given a set of features, use a predicted model to predict the labels
  and calculate mae, r2, and mse between true and predicted labels
  '''
  predicted_labels = model.predict(features)
  predicted_labels = np.ravel(predicted_labels.astype(float))
  mean_abs_error = mean_absolute_error(true_labels, predicted_labels)
  r_squared = r2_score(true_labels, predicted_labels)
  mean_sq_error = mean_squared_error(true_labels, predicted_labels)
  return (mean_abs_error, r_squared, mean_sq_error)

def cross_validate(dataset, fold):
  cv = KFold(n_splits=fold, random_state=1, shuffle=True)

  file_name_group_by_panda_series = dataset.groupby('HMDBID')
  file_name_group_by_dict = dict(list(file_name_group_by_panda_series))
  file_name = np.array(list(file_name_group_by_dict.keys()))
  file_name = file_name.reshape(file_name.shape[0],1)

  cv_scores = []
  cv_scores_train = []
  r2_scores = []
  r2_scores_train = []
  cv_scores_mse = []
  cv_scores_mse_train = []
  diff_btw_train_test_mean_score = {}

  for (inner_train_index_filename, inner_test_index_filename), j in zip(cv.split(file_name), range(fold)):
    file_name_inner = []

    file_name_inner_train = file_name[inner_train_index_filename]
    file_name_inner_test = file_name[inner_test_index_filename]

    X_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
    X_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))

    X_train_inner_temp = dataset[X_train_inner_new_filter] #this is always the datset format after cv
    X_test_inner_temp = dataset[X_test_inner_new_filter] #this is always the datset format after cv

    y_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
    y_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))

    
    y_train_inner_temp = dataset[y_train_inner_new_filter] #this is always the datset format after cv
    y_test_inner_temp = dataset[y_test_inner_new_filter] 

    X_train_inner = X_train_inner_temp.drop(columns=["ChemicalShift","HMDBID","CPosition"])
    X_train_inner = X_train_inner.iloc[: , :].values 
    X_train_inner = X_train_inner.astype(float)

    X_test_inner = X_test_inner_temp.drop(columns=["ChemicalShift","HMDBID","CPosition"])
    X_test_inner = X_test_inner.iloc[: , :].values 
    X_test_inner = X_test_inner.astype(float)

    y_train_inner = y_train_inner_temp["ChemicalShift"].values
    y_train_inner = y_train_inner.astype(float)
    y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
    

    y_test_inner = y_test_inner_temp["ChemicalShift"].values
    y_test_inner = y_test_inner.astype(float)
    y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)

    model_inner = GradientBoostingRegressor(n_estimators=500, max_depth=5, subsample=0.8, loss='huber')
    print("model creation done")

    t1 = time.time()
    model_inner.fit(X_train_inner, y_train_inner.ravel())
    print("model fitting done:{}".format(time.time()-t1))

    y_pred_inner = model_inner.predict(X_test_inner)
    y_pred_inner = y_pred_inner.astype(float)
    
    y_test_inner = y_test_inner.astype(float)
    y_test_inner = np.ravel(y_test_inner)
    
    meanAbsError_inner = mean_absolute_error(y_test_inner, y_pred_inner) # sum up of all the mae of y_pred and y_test
    cv_scores.append(meanAbsError_inner)

    r2 = r2_score(y_test_inner, y_pred_inner)
    r2_scores.append(r2)

    mSError_inner = mean_squared_error(y_test_inner, y_pred_inner)
    cv_scores_mse.append(mSError_inner)

    y_pred_inner_for_fold_train = model_inner.predict(X_train_inner)
    y_pred_inner_for_fold_train = y_pred_inner_for_fold_train.astype(float)

    meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner, y_pred_inner_for_fold_train) 
    cv_scores_train.append(meanAbsError_inner_fold_pred_for_train)

    mSError_inner_fold_pred_for_train = mean_squared_error(y_train_inner, y_pred_inner_for_fold_train) 
    cv_scores_mse_train.append(mSError_inner_fold_pred_for_train)

    r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner, y_pred_inner_for_fold_train)
    r2_scores_train.append(r2_scores_inner_fold_pred_for_train)

    print("for th:{} cv  MAE  is:{} and r2_score:{} MSE is:{}".format(j,cv_scores[j],r2_scores[j],cv_scores_mse[j]))
    print("for th:{} cv Train MAE  is:{} and r2_score:{} MSE is:{}".format(j,cv_scores_train[j],r2_scores_train[j],cv_scores_mse_train[j]))

  print("AVG MAE from cross validation:{}".format(np.mean(cv_scores)))
  print("AVG R2 scores from cross validation:{}".format(np.mean(r2_scores)))
  print("AVG MSE scores from cross validation:{}".format(np.mean(cv_scores_mse)))

  print("AVG Train MAE from cross validation:{}".format(np.mean(cv_scores_train)))
  print("AVG Train R2 scores from cross validation:{}".format(np.mean(r2_scores_train)))
  print("AVG Train MSE scores from cross validation:{}".format(np.mean(cv_scores_mse_train)))

def pca(train_features, holdout_features):
  pca = PCA(n_components=5)
  pca_train_feats = pca.fit_transform(train_features)
  pca_holdout_feats = pca.transform(holdout_features)
  print(pca.components_)
  print(pca.explained_variance_ratio_)
  return (pca_train_feats, pca_holdout_feats)

def corr(dataset):
  dataset = dataset.iloc[:,0:-3]
  corr = dataset.corr()
  sns.heatmap(corr, xticklabels=corr.columns, yticklabels=corr.columns, 
    annot=True, cmap=sns.diverging_palette(220, 20, as_cmap=True))

def normalize(train_features, holdout_features):
  tf = preprocessing.normalize(train_features, norm='l2')
  hf = preprocessing.normalize(holdout_features, norm='l2')
  return (tf, hf)

def export_cascade_dataset():
  with open('config.yml') as f:
    config = yaml.safe_load(f)

  config = namedtuple("NmrConfig", config.keys())(*config.values())
  nmr_dataset = NmrDataset(config=config)
  nmr_dataset.export_cascade_format(nmr_dataset.holdout_ids, 'holdout')

if __name__ == "__main__":
  #main()
  export_cascade_dataset()
