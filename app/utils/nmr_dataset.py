import pandas as pd
from pandas import DataFrame
from dataclasses import dataclass, field
from typing import List, Dict
from collections import namedtuple
from sklearn.decomposition import PCA
import gzip
import os

@dataclass
class NmrDataset:
  '''
  NmrDataset handles training and holdout identifiers and the feature set
  used for training and evaluation. It imports these identifiers and the feature set
  '''
  config: namedtuple
  train_ids: List[str] = field(default_factory=lambda: [])
  holdout_ids: List[str] = field(default_factory=lambda: [])
  full_dataset: DataFrame = field(default_factory=lambda: None)
  ids_type_map: Dict[str, List[str]] = field(default_factory=lambda: None)

  def __post_init__(self):
    self.import_identifiers()
    self.import_full_dataset()  
    self.drop_columns()
    self.drop_sparse_columns()

    self.ids_type_map = {
      'train': self.train_ids,
      'holdout': self.holdout_ids
    }

  def import_identifiers(self):
    '''
    Imports Identifiers used for both training and hold out set
    These identifiers are linked to a column in the feature set
    '''
    with open(self.config.train_identifiers_path) as f:
      self.train_ids = [line.strip() for line in f.readlines()]
    with open(self.config.holdout_identifiers_path) as f:
      self.holdout_ids = [line.strip() for line in f.readlines()]

  def import_full_dataset(self):
    '''
    Imports features for different Molecules with their chemical shifts.
    Creates a DataFrame
    '''
    self.full_dataset = pd.read_csv(self.config.full_dataset_path)

  def drop_columns(self):
    '''
    Drop columns that maybe wrongly calculated
    '''
    self.full_dataset = self.full_dataset.drop(columns=self.config.drop_columns)

  def drop_sparse_columns(self):
    '''
    Drop columns that have 0s for all rows in the full dataset
    These values do not influence prediction and have variance of 0
    '''
    self.full_dataset = self.full_dataset.loc[:, (self.full_dataset != 0).any(axis=0)]

  def get_dataset_slice(self, slice_type):
    '''
    Returns a pandas dataframe from a slice of full_dataset
    This slice can be 'train' or 'holdout' and depending on the slice name,
    the correct train or holdout ids will be matched with HMDBID column
    '''
    slice_ids = self.ids_type_map[slice_type]
    indexes = self.full_dataset[self.config.identifier_column].isin(slice_ids)
    return self.full_dataset.loc[indexes]

  def get_train_dataset(self):
    '''
    Return the train dataframe slice from full_dataset
    '''
    return self.get_dataset_slice('train')

  def get_holdout_dataset(self):
    '''
    Return the holdout dataframe slice from full_dataset
    '''
    return self.get_dataset_slice('holdout')

  def get_features(self, train_pd, holdout_pd):
    '''
    Get features from test and holdout dataframes in terms of float arrays
    '''
    train_features = train_pd.iloc[:,0:-3].values
    holdout_features = holdout_pd.iloc[:,0:-3].values
    return (train_features.astype(float), holdout_features.astype(float))

  def get_labels(self, train_pd, holdout_pd):
    '''
    Get Labels from test and holdout dataframes in terms of floats
    '''
    train_labels = train_pd.iloc[:,-3].values.astype(float)
    train_labels = train_labels.reshape(train_labels.shape[0],1)
    holdout_labels = holdout_pd.iloc[:,-3].values.astype(float)
    holdout_labels = holdout_labels.reshape(holdout_labels.shape[0],1)
    
    return (train_labels, holdout_labels)

  def export_cascade_format(self, identifiers, dtype='holdout'):
    '''
    Export holdout SDF files together and a csv file that contains respective chemical shifts
    '''
    # Get all filenames from molecules_sdf folder and merge all sdf files for holdout set
    all_mol_sdf = ''
    all_shift_dict = []

    for i in range(len(identifiers)):
      mol_sdf = self.config.molecules_path + identifiers[i] + '.sdf'
      shift_text = self.config.molecules_path + identifiers[i] + '.txt'
      name = ''

      # Both chemical shift text file and sdf file file must be present
      if os.path.isfile(mol_sdf) and os.path.isfile(shift_text):
        with open(mol_sdf, 'r') as f:
          content = f.read()
          name = content.split("\n")[0]
          if not name:
            name = identifiers[i] 
            content = name +  content
          elif 'GISSMO ID:' in name:
            name = identifiers[i]
            contents = content.split("\n")
            contents[0] = name
            content = "\n".join(contents)

        # End of every molecule for cascade need $$$
        # Discarding other properties after the M END since they are not
        # needed by CASCADE for calculations
        parsed = content.split("END")[0] + "END\n$$$$\n"
        all_mol_sdf += parsed
        print("Found: " + name)

        with open(shift_text, 'r') as f:
          lines = f.readlines()
          molecule_shifts = []
          for j in range(2, len(lines)):
            values = lines[j].split("\t")
            molecule_shifts.append(
              (int(values[0].strip()), 
              values[1].strip())
              )
          
          # Sort the chemical shifts in order of their atomic number
          # By first item in tuple, which is atomic number
          sorted_molecule_shifts = sorted(molecule_shifts)
          for m_index, shift in sorted_molecule_shifts:
            all_shift_dict.append({
              'mol_id': name, 
              'atom_type': self.config.atom_index,
              'atom_index': m_index, 
              'Shift': shift
              })
      else:
        print("Not found: " + identifiers[i])

    # Create a dataframe from array of dicts and export to csv
    df = DataFrame.from_dict(all_shift_dict)
    df.to_csv('dataset/wishart_' + dtype + '_' + self.config.atom + '_shifts.csv')

    # Export all sdf files as gzip
    with gzip.open('dataset/wishart_' + dtype + '_' + self.config.atom + '.sdf.gz', 'wb') as f:
      f.write(all_mol_sdf.encode("ascii"))
