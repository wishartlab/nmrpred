import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import StratifiedKFold
import copy
from sklearn.metrics import mean_squared_error,r2_score
import seaborn as sns
import plotly
from plotly import figure_factory as FF
# import chart_studio.plotly as py
import plotly.graph_objs as go
from decimal import *
# import pickle
import os, sys
import requests
import urllib, json, csv
# from fppackage import chiral_v2_CH2 as chiral
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import rdMolTransforms
import pandas as pd
import csv, math
from xgboost import XGBRFRegressor
import joblib
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
import time
from sklearn.model_selection import GroupShuffleSplit
from sklearn import preprocessing
from sklearn.ensemble import GradientBoostingRegressor




# f = f[f['HMDBID'].str.match('HMDB')]

def main():


    trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_two_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_modified_features_with_more_features_1st_phase_outliers_checking_correct_no_null.csv"
    

    dataset_full = pd.read_csv(trainingSetFile)
    print("dataset shape:{}".format(dataset_full.shape))
    dataset_full = dataset_full.drop(columns=['Unnamed: 0'])
    # dataset_full = dataset_full.drop(columns=['C'])
    # dataset_full = dataset_full.drop(columns=['C_1'])
    # dataset_full = dataset_full.drop(columns=['C_2'])
    dataset_full = dataset_full.drop(columns=['cripenLogP'])
    dataset_full = dataset_full.drop(columns=['Molar_Refractivity'])
    dataset_full = dataset_full.drop(columns=['TPSA'])
    dataset_full = dataset_full.drop(columns=['LASA'])
    print("dataset shape after dropping Unnamed:{}".format(dataset_full.shape))


    ####### divide train data and holdout data. train data = dataset, holdout = holdout_data####
    splitter = GroupShuffleSplit(test_size=.20, n_splits=1, random_state = 7)
    split = splitter.split(dataset_full, groups=dataset_full['HMDBID'])
    train_inds, test_inds = next(split)

    ## creating files with train and holdout
    file_name_group_by_panda_series_1 = dataset_full.groupby('HMDBID')
    file_name_group_by_dict_1 = dict(list(file_name_group_by_panda_series_1))
    file_name_1 = np.array(list(file_name_group_by_dict_1.keys()))
    file_name_1 = file_name_1.reshape(file_name_1.shape[0],1)
    print("# of files = {}".format(len(file_name_1)))
    hmdb_ids_1 = dataset_full.iloc[ :,-2].values
    hmdb_ids_1 = hmdb_ids_1.reshape(hmdb_ids_1.shape[0],1)
    hydrogen_positions_1 = dataset_full.iloc[ :,-1].values
    hydrogen_positions_1 = hydrogen_positions_1.reshape(hydrogen_positions_1.shape[0],1)
    hydrogen_positions_1 = hydrogen_positions_1.ravel()
    train_1 = dataset_full.iloc[train_inds]
    test_1 = dataset_full.iloc[test_inds]
    id_train = train_1["HMDBID"].values
    id_test = test_1["HMDBID"].values
    visited = set()
    dup_train = {i for i in id_train if i in visited or (visited.add(i) or False)}
    n_f = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/train_jeol_clean_tms_to_dss_sorted_an_split_prediction_chiral_without_normalization_1st_phase_outliers_two_bond_regression_startting_of_2nd_phase.txt"
    n_f_f = open(n_f,"w")
    for j in dup_train:
      n_f_f.write(str(j)+"\n")
    n_f_f.close()
    visited = set()
    dup_test = {i for i in id_test if i in visited or (visited.add(i) or False)}
    n_f = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/holdout_jeol_clean_tms_dss_sorted_an_split_prediction_chiral_without_normalization_1st_phase_outliers_two_bond_regression_starting_of_2nd_phase.txt"
    n_f_f = open(n_f,"w")
    for j in dup_test:
      n_f_f.write(str(j)+"\n")
    n_f_f.close()
    #### file creation done ###




    dataset = dataset_full.iloc[train_inds]# this is train data
    # new_dataset = dataset.copy()
    holdout_data = dataset_full.iloc[test_inds]# this is holdout data
    # new_holdout_data = holdout_data.copy()

    print("holdout dataset ID = {}".format(holdout_data["HMDBID"]))
    print("holdout dataset shape = {}".format(holdout_data.shape))

    ################################################################



    hmdb_ids = dataset.iloc[ :,-2].values
    hmdb_ids = hmdb_ids.reshape(hmdb_ids.shape[0],1)
    hydrogen_positions = dataset.iloc[ :,-1].values
    hydrogen_positions = hydrogen_positions.reshape(hydrogen_positions.shape[0],1)

    print("nan value: {}".format(dataset.isnull().sum()))
    X_training = dataset.iloc[ : , 0:-3].values
    X_training = X_training.astype(float)
    # scaler = preprocessing.StandardScaler().fit(X_training)
    # X_scaled = scaler.transform(X_training)
    # X_scaled_mean = scaler.mean_
    # X_scaled_scale = scaler.scale_
    # print("X_scaled_mean = {} and X_scaled_scale ={}".format(X_scaled_mean,X_scaled_scale))
    # new_dataset.iloc[:,:-3] = X_scaled

    y_training =  dataset.iloc[ :,-3].values
    y_training = y_training.astype(float)
    y_training = y_training.reshape(y_training.shape[0],1)
    Y_training = y_training
    # scaler_y = preprocessing.StandardScaler().fit(Y_training)
    # Y_scaled = scaler_y.transform(Y_training)
    # new_dataset.iloc[:,-3] = Y_scaled
    # Y_scaled_mean = scaler_y.mean_
    # Y_scaled_scale = scaler_y.scale_
    # print("Y_scaled_mean = {} and X_scaled_scale ={}".format(Y_scaled_mean,Y_scaled_scale))
    # # folds = 5
    # print("dataset = {} and new_dataset ={}".format(dataset,new_dataset))
    


    # folds = 10
    folds = 5

    cv = KFold(n_splits=folds, random_state=1, shuffle=True)

    file_name_group_by_panda_series = dataset.groupby('HMDBID')
    file_name_group_by_dict = dict(list(file_name_group_by_panda_series))
    file_name = np.array(list(file_name_group_by_dict.keys()))
    file_name = file_name.reshape(file_name.shape[0],1)

    cv_scores = []
    cv_scores_train = []
    r2_scores = []
    r2_scores_train = []
    cv_scores_mse = []
    cv_scores_mse_train = []

    # c = 100.0
    # ep = 0.1
    # c = 60.0
    # ep = 2.0
    # c = 100.0
    # ep = 2.0
    # c = 140 #number_of_estimators , test MAE 3.5
    # ep = 5 #m_depth
    # c = 85
    # ep = 5
    c = 130
    ep = 5
    # c = 110
    # ep = 5
    HEAD = ["MoleculeId", "CPosition", "TrueVal", "PredictedVal", "Fold"]

    # with open("/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold_600_10_3molecule_xuan_new_descriptor_all_features_2/1200_70_hmdb_onlybmrb_swapped_COH2_fixed_7point92_without_uncommon_no_null/outliers_1200_70_hmdb_onlybmrb_swapped_COH2_fixed_7point92_without_uncommon_no_null.csv", "w") as csv_file:
    # with open("/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold_600_10_3molecule_xuan_new_descriptor_all_features_2/500_50_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_3/outliers_700_50_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2_no_null.csv", "w") as csv_file:
    # with open("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/svr_c90_ep_2_only_HMDB/outliers_svr_c90_ep_2_only_HMDB.csv", "w") as csv_file:
    # with open("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/svr_c90_ep_2_only_HMDB/outliers_svr_c90_ep_2_only_HMDB_fixed_hmdb_swapped_excluded_atoms.csv", "w") as csv_file:
    # with open("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/svr_c90_ep_2_only_HMDB/outliers_svr_c90_ep_2_only_HMDB_fixed_hmdb_swapped_excluded_atoms_only_HMDB.csv", "w") as csv_file:
    # with open("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/svr_c100_ep_p1/outliers_svr_c100_ep_p1_one_bond_away.csv", "w") as csv_file:
    # with open("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/gradientboost/outliers_gb_ne85_md5_two_bond_away.csv", "w") as csv_file:
    # with open("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/gradientboost/outliers_gb_ne130_md5_two_bond_away.csv", "w") as csv_file:
    with open("/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/outliers_gb_ne130_md5_two_bond_away_fold_10_startimg_of_2nd_phase.csv", "w") as csv_file:
           
        writer = csv.writer(csv_file)
        writer.writerow(HEAD)

        for (inner_train_index_filename, inner_test_index_filename), j in zip(cv.split(file_name), range(folds)):
    
            file_name_inner = []

            file_name_inner_train = file_name[inner_train_index_filename]
            file_name_inner_test = file_name[inner_test_index_filename]
    
            X_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
            # X_train_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
            X_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
            # X_test_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
    
            X_train_inner_temp = dataset[X_train_inner_new_filter] #this is always the datset format after cv
            X_test_inner_temp = dataset[X_test_inner_new_filter] #this is always the datset format after cv
            # X_train_inner_temp = new_dataset[X_train_inner_new_filter] #this is always the datset format after cv
            # X_test_inner_temp = new_dataset[X_test_inner_new_filter] #this is always the datset format after cv
            y_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
            y_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
            # y_train_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
            # y_test_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
            
            y_train_inner_temp = dataset[y_train_inner_new_filter] #this is always the datset format after cv
            y_test_inner_temp = dataset[y_test_inner_new_filter] #this is always the datset format after cv
            # y_train_inner_temp = new_dataset[y_train_inner_new_filter] #this is always the datset format after cv
            # y_test_inner_temp = new_dataset[y_test_inner_new_filter] #this is always the datset format after cv
            

            X_train_inner = X_train_inner_temp.drop(columns=["ChemicalShift","HMDBID","CPosition"])
            X_train_inner = X_train_inner.iloc[: , :].values 
            # print("X_train_inner data type 274:{}".format(type(X_train_inner[0][274])))
            X_train_inner = X_train_inner.astype(float)
    
            X_test_inner = X_test_inner_temp.drop(columns=["ChemicalShift","HMDBID","CPosition"])
            X_test_inner = X_test_inner.iloc[: , :].values 
            # print("X_test_inner data type 274:{}".format(type(X_test_inner[0][274])))
            X_test_inner = X_test_inner.astype(float)
    
            y_train_inner = y_train_inner_temp["ChemicalShift"].values
            y_train_inner = y_train_inner.astype(float)
            y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
            
    
            y_test_inner = y_test_inner_temp["ChemicalShift"].values
            y_test_inner = y_test_inner.astype(float)
            y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)
            
    
            hmdb_ids_inner_fold_train = X_train_inner_temp["HMDBID"].values
            hmdb_ids_inner_fold_train = hmdb_ids_inner_fold_train.reshape(hmdb_ids_inner_fold_train.shape[0],1)
    
            hmdb_ids_inner_fold_test = X_test_inner_temp["HMDBID"].values
            hmdb_ids_inner_fold_test = hmdb_ids_inner_fold_test.reshape(hmdb_ids_inner_fold_test.shape[0],1)
            hmdb_ids_inner_fold_test = np.ravel(hmdb_ids_inner_fold_test)


            file_name_group_by_panda_series_inner = X_test_inner_temp.groupby('HMDBID')
            file_name_group_by_dict_inner = dict(list(file_name_group_by_panda_series_inner))
            file_name_inner = np.array(list(file_name_group_by_dict_inner.keys()))
            file_name_inner = file_name_inner.reshape(file_name_inner.shape[0],1)  # this is crossval holdout file names unique
            file_name_inner = np.ravel(file_name_inner)
            print("unique inner file name to be predicted:{}".format(file_name_inner))
    
    
            hydrogen_positions_inner_fold_train = X_train_inner_temp["CPosition"].values
            hydrogen_positions_inner_fold_train = hydrogen_positions_inner_fold_train.reshape(hydrogen_positions_inner_fold_train.shape[0],1)
    
            hydrogen_positions_inner_fold_test = X_test_inner_temp["CPosition"].values
            hydrogen_positions_inner_fold_test = hydrogen_positions_inner_fold_test.reshape(hydrogen_positions_inner_fold_test.shape[0],1)
            
            model_inner = GradientBoostingRegressor(n_estimators= c, max_depth= ep, subsample=0.9)
            print("model creation done")

            t1 = time.time()
            model_inner.fit(X_train_inner, y_train_inner.ravel())
            print("model fitting done:{}".format(time.time()-t1))

            y_pred_inner = model_inner.predict(X_test_inner)
            y_pred_inner = y_pred_inner.astype(float)
            # y_pred_inner_original = (y_pred_inner*Y_scaled_scale)+Y_scaled_mean
            
            y_test_inner = y_test_inner.astype(float)
            y_test_inner = np.ravel(y_test_inner)
            # y_test_inner_original = (y_test_inner*Y_scaled_scale)+Y_scaled_mean
            
            
            meanAbsError_inner = mean_absolute_error(y_test_inner, y_pred_inner) # sum up of all the mae of y_pred and y_test
            # meanAbsError_inner = mean_absolute_error(y_test_inner_original, y_pred_inner_original) # sum up of all the mae of y_pred and y_test
            cv_scores.append(meanAbsError_inner)

            r2 = r2_score(y_test_inner, y_pred_inner)
            # r2 = r2_score(y_test_inner_original, y_pred_inner_original)
            r2_scores.append(r2)
            # print("y_test_inner:{} and y_pred_inner:{}".format(y_test_inner,y_pred_inner))

            mSError_inner = mean_squared_error(y_test_inner, y_pred_inner)
            # mSError_inner = mean_squared_error(y_test_inner_original, y_pred_inner_original)
            cv_scores_mse.append(mSError_inner)

            y_pred_inner_for_fold_train = model_inner.predict(X_train_inner)
            y_pred_inner_for_fold_train = y_pred_inner_for_fold_train.astype(float)
            # y_pred_inner_for_fold_train_original = (y_pred_inner_for_fold_train*Y_scaled_scale)+Y_scaled_mean
            # y_train_inner_original = (y_train_inner*Y_scaled_scale)+Y_scaled_mean

            meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner, y_pred_inner_for_fold_train) 
            # meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner_original, y_pred_inner_for_fold_train_original) 
            cv_scores_train.append(meanAbsError_inner_fold_pred_for_train)
    
            mSError_inner_fold_pred_for_train = mean_squared_error(y_train_inner, y_pred_inner_for_fold_train) 
            cv_scores_mse_train.append(mSError_inner_fold_pred_for_train)

            r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner, y_pred_inner_for_fold_train)
            # r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner_original, y_pred_inner_for_fold_train_original)
            r2_scores_train.append(r2_scores_inner_fold_pred_for_train)


            print("started prediction for C:{}   epsilon:{}".format(c,ep))
            for k in range(len(y_pred_inner)):

                datarow = []
                print("Printing  prediction:{}------true:{}, HMDB_ID:{} and C position is:{}".format(y_pred_inner[k], y_test_inner[k],hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k]))
                if float(abs(y_pred_inner[k]-y_test_inner[k])) >= 3.0 :
                    datarow.append(hmdb_ids_inner_fold_test[k])
                    datarow.append(hydrogen_positions_inner_fold_test[k][0])
                    datarow.append(y_test_inner[k])
                    datarow.append(y_pred_inner[k])
                    datarow.append(j+1)
                    writer.writerow(datarow)
                    print("outliers{} {}".format(hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k][0]))
            #for k in range(len(y_pred_inner_original)):
#
            #    datarow = []
            #    print("Printing  prediction:{}------true:{}, HMDB_ID:{} and C position is:{}".format(y_pred_inner_original[k], y_test_inner_original[k],hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k]))
            #    if float(abs(y_pred_inner_original[k]-y_test_inner_original[k])) >= 3.0 :
            #        datarow.append(hmdb_ids_inner_fold_test[k])
            #        datarow.append(hydrogen_positions_inner_fold_test[k][0])
            #        datarow.append(y_test_inner_original[k])
            #        datarow.append(y_pred_inner_original[k])
            #        datarow.append(j+1)
            #        writer.writerow(datarow)
            #        print("outliers{} {} {} {}".format(hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k][0],y_test_inner_original[k],y_pred_inner_original[k]))    
            



            print("for th:{} cv  MAE  is:{} and r2_score:{} MSE is:{}".format(j,cv_scores[j],r2_scores[j],cv_scores_mse[j]))
            print("for th:{} cv Train MAE  is:{} and r2_score:{} MSE is:{}".format(j,cv_scores_train[j],r2_scores_train[j],cv_scores_mse_train[j]))


            
            ### this is using matplotlib #####
            # plt.plot(y_test_inner,y_pred_inner,'r+')
            # plt.plot([y_test_inner.min(),y_pred_inner.max()],[y_test_inner.min(),y_pred_inner.max()], 'b--', lw=2)
            # plt.title('True Vs Predicted', fontsize=18)
            # plt.xlabel('True')
            # plt.ylabel('Predicted')
            # plt.show()
            ###  #####
    
            #### this is using using plotly ##
            # file_name_graph = "/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold/fold"+str(j+1)+"/trueVspredict.html"
            file_name_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/fold"+str(j+1)+"/trueVspredict_fold_10_starting_of_2nd_phase.html"
            draw(y_test_inner,y_pred_inner,file_name_graph)
            file_name_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/fold"+str(j+1)+"/trueVspredict_reg_fold_10_startimg_of_2nd_phase"
            draw_reg(y_test_inner,y_pred_inner,file_name_graph)
            # draw(y_test_inner_original,y_pred_inner_original,file_name_graph)


            ########
    
            ##### this is using matplotlib ###
            # data_for_graph = pd.DataFrame(X_train_inner_temp['ChemicalShift'])
            # chemical_shift = data_for_graph['ChemicalShift'].values
            # chemical_shift = np.round(chemical_shift,2)
            # chemical_shift = chemical_shift*100
            # data_for_graph['ChemicalShift']=chemical_shift
            # sns.set(font_scale=0.5)
            # countplt=sns.countplot(x='ChemicalShift', data=data_for_graph, palette ='hls')
            # plt.title('Distribution of Chemical Shift Classes in training set', fontsize=18)
            # plt.xlabel('Chemical Shift Class', fontsize=16)
            # plt.ylabel('Frequency', fontsize=16)
            # plt.show()
            ######   #####
    
            ### this is using plotly #####
            data_for_graph = pd.DataFrame(X_train_inner_temp['ChemicalShift'])
            # table = FF.create_table(data_for_graph)
            # py.iplot(table, filename='chemical shift class frequency in training set')
            # file_name_graph_training_class = "/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold_600_10_3molecule_xuan_new_descriptor_all_features_2/500_50_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_3/fold"+str(j+1)+"/Chemicalshift-Training-freq-count.html"
            # classfrequency(data_for_graph,file_name_graph_training_class)
            ####  ########
    
    
            # min_chem_shift_train =  (data_for_graph['ChemicalShift'].values).min()
            # max_chem_shift_train =  (data_for_graph['ChemicalShift'].values).max()
            # print("for fold {} min Chemical Shift Classes in training set:{} and max Chemical Shift Classes in training set:{}".format(j,min_chem_shift_train,max_chem_shift_train))
    
            #### this is using matplotlib #####
            # data_for_graph = pd.DataFrame(y_test_inner_temp['ChemicalShift'])
            # chemical_shift = data_for_graph['ChemicalShift'].values
            # chemical_shift = np.round(chemical_shift,2)
            # chemical_shift = chemical_shift*100
            # data_for_graph['ChemicalShift']=chemical_shift
            # sns.set(font_scale=0.5)
            # countplt=sns.countplot(x='ChemicalShift', data=data_for_graph, palette ='hls')
            # plt.title('Distribution of Chemical Shift Classes in test set', fontsize=18)
            # plt.xlabel('Chemical Shift Class', fontsize=16)
            # plt.ylabel('Frequency', fontsize=16)
            # plt.show()
            #####   #########
    
            #### this is using plotly ####
            # data_for_graph = pd.DataFrame(y_test_inner_temp['ChemicalShift'])
            # file_name_graph_test_class = "/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold_600_10_3molecule_xuan_new_descriptor_all_features_2/500_50_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_3/fold"+str(j+1)+"/Chemicalshift-Test-freq-count.html"
            # classfrequency(data_for_graph,file_name_graph_test_class)
            # ########
    
            # min_chem_shift_test =  (data_for_graph['ChemicalShift'].values).min()
            # max_chem_shift_test =  (data_for_graph['ChemicalShift'].values).max()
            # print("for fold {} min Chemical Shift Classes in test set:{} and max Chemical Shift Classes in test set:{}".format(j,min_chem_shift_test,max_chem_shift_test))
      



        #final training
        # model = RandomForestClassifier(n_estimators=1000,max_depth=10) #change the value here
        print("AVG MAE from cross validation:{}".format(np.mean(cv_scores)))
        print("AVG R2 scores from cross validation:{}".format(np.mean(r2_scores)))
        print("AVG MSE scores from cross validation:{}".format(np.mean(cv_scores_mse)))

        print("AVG Train MAE from cross validation:{}".format(np.mean(cv_scores_train)))
        print("AVG Train R2 scores from cross validation:{}".format(np.mean(r2_scores_train)))
        print("AVG Train MSE scores from cross validation:{}".format(np.mean(cv_scores_mse_train)))
  
        # ##########
        # ##########
  
  
      
        #final training
        # model = SVR(kernel ='linear',C = c,epsilon = ep,verbose=False)
        model = GradientBoostingRegressor(n_estimators= c, max_depth= ep, subsample=0.9)
        
        #X_training_1 = new_dataset.iloc[ : , 0:-3].values
        ## X_training_1 = X_training_1.astype(float)
        #Y_training_1 = new_dataset.iloc[ :,-3].values
        ## Y_training_1 = np.ravel(Y_training_1)
        ## Y_training_1 = X_training_1.astype(float)
        # model.fit(X_training_1,Y_training_1.ravel())
        model.fit(X_training,Y_training.ravel())


        training_set_prediction = model.predict(X_training)
        training_set_prediction = training_set_prediction.astype(float)
        training_set_prediction = np.ravel(training_set_prediction)
        true_value_of_whole_train_set = Y_training.astype(float)
        true_value_of_whole_train_set = np.ravel(true_value_of_whole_train_set)
        training_set_mean_absolute_error = mean_absolute_error(true_value_of_whole_train_set, training_set_prediction)
        training_set_prediction_r2 = r2_score(true_value_of_whole_train_set, training_set_prediction)
        training_set_mse_error = mean_squared_error(true_value_of_whole_train_set, training_set_prediction)

        print(" WHOLE TAIN SET  MAE  is:{}".format(training_set_mean_absolute_error))
        print(" WHOLE TAIN SET  MSE  is:{} ".format(training_set_mse_error))
        print(" WHOLE TAIN SET  R2  is:{}".format(training_set_prediction_r2))

        file_name_train_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/TrueVsPredict_trainerror_fold_10_starting_of_2nd_phase"
        draw_train(true_value_of_whole_train_set,training_set_prediction,file_name_train_graph)
    

        ##### SAVE MODELS #####
        # with open('/Users/venisuresh/opt/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/saved_model_RFC_best_param_one_hot_700_35.pickle', 'wb') as handle:
        #   pickle.dump(saved_models, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
        # testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_no_null_knn_imputer.csv"
        # testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_Train2_fixed_HMDB_no_null_knn_imputer.csv"
        # testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_Train2_fixed_HMDB_swapped_excluded_atoms_no_null_knn_imputer.csv"
        # testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_Train2_fixed_HMDB_swapped_excluded_atoms_only_HMDB_no_null_knn_imputer.csv"
        # testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/holdout_clean_dataset1_no_null_knn_imputer.csv"
        #testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/holdout_clean_dataset1_less_feature_no_null_knn_imputer.csv"
        # testData = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_clean_dataset1_less_feature_no_null_knn_imputer.csv"
        # dataset = pd.read_csv(testData)
        # dataset = holdout_data
        dataset_holdout = holdout_data.copy()
        
        print("This is how the dataset looks like after converting to np array:{}".format(dataset_holdout))
        print("dataset shape:{}".format(dataset_holdout.shape))
        
        # dataset = dataset.drop(columns=['Unnamed: 0'])
        # dataset = dataset.drop(columns=['C'])
        # dataset = dataset.drop(columns=['cripenLogP'])
        # dataset = dataset.drop(columns=['Molar_Refractivity'])
        # dataset = dataset.drop(columns=['TPSA'])
        # dataset = dataset.drop(columns=['LASA'])
        print("dataset shape after dropping unnamed zero charge:{}".format(dataset_holdout.shape))
        

        file_name_group_by_panda_series_holdout = dataset_holdout.groupby('HMDBID')
        file_name_group_by_dict_hold = dict(list(file_name_group_by_panda_series_holdout))
        file_name_hold = np.array(list(file_name_group_by_dict_hold.keys()))
        file_name_hold = file_name_hold.reshape(file_name_hold.shape[0],1)  # this is crossval holdout file names unique
        file_name_hold = np.ravel(file_name_hold)
        print("unique holdout file name to be predicted:{}".format(file_name_hold))

        hmdb_ids = dataset_holdout.iloc[ :,-2].values
        hmdb_ids = hmdb_ids.reshape(hmdb_ids.shape[0],1)
        hmdb_ids = np.ravel(hmdb_ids)

        hydrogen_positions = dataset_holdout.iloc[ :,-1].values
        hydrogen_positions = hydrogen_positions.reshape(hydrogen_positions.shape[0],1)
        print("nan value: {}".format(dataset.isnull().sum()))

        #now normalize the dataset using train mean and scale values####
        X = dataset_holdout.iloc[ : , 0:-3].values
        X = X.astype(float)
        # X_scaled = (X-X_scaled_mean)/X_scaled_scale
        # dataset_holdout.iloc[:,:-3] = X_scaled
        # print("X portion of dataset:{}".format(X))

        y =  dataset_holdout.iloc[ :,-3].values
        y = y.reshape(y.shape[0],1)
        y = y.astype(float)
        Y = y
        # Y_scaled = (Y-Y_scaled_mean)/Y_scaled_scale
        # dataset_holdout.iloc[:,-3] = Y_scaled
        # print("Y after no classLabel:{}".format(Y))
        

        # X_test = dataset_holdout.iloc[ : , 0:-3].values # change here . uncommit when pca is not used
        X_test = X
        # y_test = dataset_holdout.iloc[ :,-3].values
        y_test = Y
        y_pred = model.predict(X_test)
    
        y_pred = y_pred.astype(float)
        # y_pred_original = (y_pred*Y_scaled_scale)+Y_scaled_mean
        
    
        y_test = y_test.astype(float)
        # y_test_original = (y_test*Y_scaled_scale)+Y_scaled_mean
        
        y_test = np.ravel(y_test)
        # y_test_original = np.ravel(y_test_original)
        meanAbsError = mean_absolute_error(y_test, y_pred)
        r2 = r2_score(y_test, y_pred)
        # meanAbsError = mean_absolute_error(y_test_original, y_pred_original)
        # r2 = r2_score(y_test_original, y_pred_original)
        mSError = mean_squared_error(y_test, y_pred)
        print("y_test={}".format(y_test))
        print("y_pred={}".format(y_pred))
        # mSError = mean_squared_error(y_test_original, y_pred_original)
        # print("y_test={}".format(y_test_original))
        # print("y_pred={}".format(y_pred_original))  

        # new_file = open("/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/prediction_print_latest_final.txt", "w")
        # outliers_hmdb_id = []
        # outliers_h_position = []
        hold_out_prediction_file_name = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/holdout/holdout_prediction_fold_10_strting_of_2nd_phase.txt"
        hold_out_prediction_file = open(hold_out_prediction_file_name,"w")
        hold_out_prediction_file.write("Table\nAssignment")
        for k in range(len(y_pred)):
        # for k in range(len(y_pred_original)):
            data_row_holdout = []
            print("Printing holdout prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred[k], y_test[k],hmdb_ids[k],hydrogen_positions[k][0]))
            # new_file.write(str(hydrogen_positions[k][0])+"\t"+str(y_pred[k])+"\t"+str(hmdb_ids[k][0]+"\n"))
            if float(abs(y_pred[k]-y_test[k])) >= 3.0 :
                data_row_holdout.append(hmdb_ids[k])
                data_row_holdout.append(hydrogen_positions[k][0])
                data_row_holdout.append(y_test[k])
                data_row_holdout.append(y_pred[k])
                data_row_holdout.append("Holdout")
                writer.writerow(data_row_holdout)
                # outliers_hmdb_id.append(hmdb_ids[k][0])
                # outliers_h_position.append(hydrogen_positions[k][0])

            # print("Printing holdout prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred_original[k], y_test_original[k],hmdb_ids[k],hydrogen_positions[k][0]))
            # new_file.write(str(hydrogen_positions[k][0])+"\t"+str(y_pred[k])+"\t"+str(hmdb_ids[k][0]+"\n"))
            #if float(abs(y_pred_original[k]-y_test_original[k])) >= 3.0 :
            #    data_row_holdout.append(hmdb_ids[k])
            #    data_row_holdout.append(hydrogen_positions[k][0])
            #    data_row_holdout.append(y_test_original[k])
            #    data_row_holdout.append(y_pred_original[k])
            #    data_row_holdout.append("Holdout")
            #    writer.writerow(data_row_holdout)

            ### start creating holdout file####
            hold_out_prediction_file.write("\n"+str(hmdb_ids[k])+"\t"+str(hydrogen_positions[k][0])+"\t"+str(y_test[k])+"\t"+str(y_pred[k]))
            # hold_out_prediction_file.write("\n"+str(hmdb_ids[k])+"\t"+str(hydrogen_positions[k][0])+"\t"+str(y_test_original[k])+"\t"+str(y_pred_original[k]))
        hold_out_prediction_file.close()
        print("MAE={}".format(meanAbsError))
        print("R2={}".format(r2))
        print("mSError={}".format(mSError))


        
        #########
    
        #print("\n\n######   started printing outliers for Holdoutset #######\n\n")
    
        # print("outliers id:{} and position:{}".format(outliers_hmdb_id,outliers_h_position))
        # for l in range(len(outliers_hmdb_id)):
          # print("outliers id:{} and position:{}".format(outliers_hmdb_id[l],outliers_h_position[l]))
    
        # new_file.colse()
    
        ###### using matplotlib #####
        # plt.plot(y_test,y_pred,'r+')
        # plt.plot([y_test.min(),y_pred.max()],[y_test.min(),y_pred.max()], 'b--', lw=2)
        # plt.title('True Vs Predicted for Hold Out Set', fontsize=18)
        # plt.xlabel('True')
        # plt.ylabel('Predicted')
        # plt.show()
        #### ####
    
    
        #### using plotly #####
        file_name_holdout_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/holdout/TrueVsPredict_fold_10_starting_of_2nd_phase.html"
        # draw(y_test_original,y_pred_original,file_name_holdout_graph)
        draw(y_test,y_pred,file_name_holdout_graph)
        file_name_holdout_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_three/algorithms/GDB/holdout/TrueVsPredict_reg_fold_10_starting_of_2nd_phase"
        # draw(y_test_original,y_pred_original,file_name_holdout_graph)
        draw_test(y_test,y_pred,file_name_holdout_graph)
        
        ####  ########







def classLabel(y_value):      
  cls = [] 
  for i in range(1,1001): #for 1000 class        
    cls.append({"value": float(i)/float(100), "class": i})       
  y_value_temp = y_value[:]
  y_value_classlabel = np.zeros(len(y_value_temp))
  for i in range(len(y_value_temp)):
    y_value_temp[i] = np.round(y_value_temp[i],2) # changed it from 1 to 2
  cls_value = [cls[i]["value"] for i in range(len(cls))]
  cls_class = [cls[i]["class"] for i in range(len(cls))]
  cls_value_tuple = tuple(cls_value)
  cls_class_tuple = tuple(cls_class)
  cls_class_value_dict = dict(zip(cls_class_tuple,cls_value_tuple))
  for i in range(len(y_value_temp)):
    for k, v in cls_class_value_dict.items():
      if v == y_value_temp[i]:
        y_value_classlabel[i] = k
        break
      else:
        y_value_classlabel[i] = 1001.0 
  # print("y value has been transferred into class:{}".format(y_value_classlabel)) 
  return y_value_classlabel


def draw(y_test_data,y_pred_data,file_name_graph):
    trace_data = []


    tracer_scatter = go.Scatter(x = y_test_data,y = y_pred_data,mode = 'markers',marker = dict(color = "OrangeRed"))
                        # mode = "lines + markers")
    tracer_line    = go.Scatter(x = y_test_data,y = y_test_data,mode = 'lines',line = dict(color = 'blue'))
    trace_data.append(tracer_scatter)
    trace_data.append(tracer_line)
    layout = go.Layout(title="True Vs Prediction",xaxis={'title':'True'},yaxis={'title':'Prediction'})

    prediction_fig = go.Figure(data=trace_data,layout=layout)

    plotly.offline.plot(prediction_fig, filename=file_name_graph,auto_open=False)
    print("R2 drawing is done")

def classfrequency(data_for_graph, file_name_frequency):
    x = data_for_graph['ChemicalShift'].values
    x = np.round(x,2)
    x = x*100
    x = x.tolist()
    # x_data = np.sort([str(r)+"c_s" for r in x])
    # trace = go.Histogram(x=[str(r)+"c_s" for r in x],
    trace = go.Histogram(x=x,
        xbins=dict(start=np.min(x),
            size=0.25,end=np.max(x)),
        marker=dict(color='rgb(25, 25, 100)'))
    # layout = go.Layout(title="Distribution of Chemical Shift Classes in training set",xaxis={'title':'Chemical Class','tickmode':'linear'},yaxis={'title':'Frequency'})
    layout = go.Layout(title="Distribution of Chemical Shift Classes in training set",xaxis=dict(title='Chemical Class'),yaxis=dict(title='Frequency'))
    fig = go.Figure(data=go.Data([trace]), layout=layout)
    # py.iplot(fig, filename=file_name_graph_training_class)
    plotly.offline.plot(fig, filename=file_name_frequency,auto_open=False)
    print("chemical shift drawing is done")


def ReadShiftFile1(file_name): # with path
  # print("h_position is:{} and length:{}".format(h_position,len(h_position)))
  H_position_and_chemicalshift_in_shift_file = [] # this returns after checking "O" and "N" as neighbor atoms
  
  hydrogen = []
  shift_pred = []
  shift_true = []
  with open(file_name, "r") as fp:
    lines = fp.readlines()
    for i in range(2,len(lines)):
      splited_line = lines[i].split("\t")
      len_of_splitted_line = len(splited_line)
      # print("H:{}".format(splited_line[0].strip()))
      # print("S:{}".format(splited_line[len_of_splitted_line-1].strip()))
      hydrogen.append(int(splited_line[0].strip()))
      shift_temp1 = float(splited_line[len_of_splitted_line-2].strip())
      shift_temp2 = float(splited_line[len_of_splitted_line-1].strip())
      shift_pred.append(round(shift_temp1,2))
      shift_true.append(round(shift_temp2,2))
  
  print("returning H_position:{} , predicted chemicalshift:{}, True chemicalshift:{}".format(hydrogen,shift_pred,shift_true))
  return hydrogen, shift_pred, shift_true


def draw_reg(y_test_data,y_pred_data,file_name_graph):

    # sns.regplot(x,y, scatter_kws={"color": "red", "alpha": 0.3}, line_kws={"color": "green"},marker="+",label="Training Data")

    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":1})
    data = {"test": y_test_data, "pred": y_pred_data}
    sns.set_style('ticks')
    df = pd.DataFrame(data)

    g = sns.lmplot(x = "test", y="pred", data =df,markers = "x",scatter_kws={"color": "red","alpha": 0.3},line_kws={"color":"blue","lw":3},ci=None)
    plt.xlabel("True Chemical Shift Value in ppm",fontweight ="bold")
    plt.ylabel("Predicted Chemical Shift Value in ppm",fontweight ="bold")
    plt.title("True Vs Predicted Chemical Shift",fontweight ="bold", color ="purple")
    plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    plt.yticks(np.arange(0.0, 11.0, 0.5))
    g.figure.set_size_inches(18.5, 10.5)
    sns.despine()
    plt.savefig(file_name_graph)

    print("graph save done")


    # plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    # plt.yticks(np.arange(0.0, 11.0, 0.5))
    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":0.5})
    # plt.xlabel("True Chemical Shift Value")
    # plt.ylabel("Predicted Chemical Shift Value")
    # plt.title("True Vs Predicted Chemical Shift")
    # plt.savefig(file_name_graph)

    # print("graph save done")


def draw_train(y_test_data,y_pred_data,file_name_graph):

    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red"}, line_kws={"color": "blue"},marker="+",label="True Vs Predicted Chemical Shift Plot for Using Training Dataset")
    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":1})

    data = {"test": y_test_data, "pred": y_pred_data}
    sns.set_style('ticks')
    df = pd.DataFrame(data)

    g = sns.lmplot(x = "test", y="pred", data =df,markers = "x",scatter_kws={"color": "red","alpha": 0.3},line_kws={"color":"blue","lw":3},ci=None)
    plt.xlabel("True Chemical Shift Value in ppm",fontweight ="bold")
    plt.ylabel("Predicted Chemical Shift Value in ppm",fontweight ="bold")
    plt.title("True Vs Predicted Chemical Shift Plot Using Training Dataset",fontweight ="bold", color ="purple")
    plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    plt.yticks(np.arange(0.0, 11.0, 0.5))
    g.figure.set_size_inches(18.5, 10.5)
    sns.despine()
    plt.savefig(file_name_graph)

    print("graph save done")
    

    # plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    # plt.yticks(np.arange(0.0, 11.0, 0.5))
    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":0.5})
    # plt.xlabel("True Chemical Shift Value")
    # plt.ylabel("Predicted Chemical Shift Value")
    # plt.title("True Vs Predicted Chemical Shift Plot for Using Training Dataset")
    # plt.savefig(file_name_graph)

    # print("graph save done")

def draw_test(y_test_data,y_pred_data,file_name_graph):

    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red"}, line_kws={"color": "blue"},marker="+",label="True Vs Predicted Chemical Shift Plot for Using Test Dataset")
    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":1})
    
    data = {"test": y_test_data, "pred": y_pred_data}
    sns.set_style('ticks')
    df = pd.DataFrame(data)

    # g = sns.lmplot(x = "test", y="pred", data =df,markers = "x",height=8, aspect=1.5,ci=None)
    g = sns.lmplot(x = "test", y="pred", data =df,markers = "x",scatter_kws={"color": "red","alpha": 0.7},line_kws={"color":"blue","lw":3},ci=None)
    plt.xlabel("True Chemical Shift Value in ppm",fontweight ="bold")
    plt.ylabel("Predicted Chemical Shift Value in ppm",fontweight ="bold")
    plt.title("True Vs Predicted Chemical Shift Plot Using Holdout Dataset",fontweight ="bold", color ="purple")
    plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    plt.yticks(np.arange(0.0, 11.0, 0.5))
    g.figure.set_size_inches(18.5, 10.5)
    sns.despine()
    plt.savefig(file_name_graph)

    print("graph save done")









if __name__ == '__main__':
  main()
