
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.datasets import load_digits
from sklearn.model_selection import validation_curve
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error,r2_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
import copy
from sklearn.model_selection import KFold
from decimal import *
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
import time
from sklearn.model_selection import GroupShuffleSplit
# from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import GradientBoostingRegressor
# from iteration_utilities import duplicates
import seaborn as sns
import plotly.graph_objects as go
import plotly
from operator import itemgetter
from collections import OrderedDict


# import matplotlib.pyplot as plt
# import numpy as np
# import math
# x = np.arange(0, math.pi*2, 0.05)
# fig = plt.figure()
# ax = fig.add_axes([0.1, 0.1, 0.8, 0.8]) # main axes
# y = np.sin(x)
# ax.plot(x, y)
# ax.set_xlabel(‘angle’)
# ax.set_title('sine')
# ax.set_yticks([-1,0,1])
# ax.set_xticks([0,2,4,6])
# ax.set_xticklabels(['zero','two','four','six'])
# plt.show()
# numpy.isnan(number)


def main():
   
    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_with_34_f_grp_no_null_knn_imputation.csv"
    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_no_null_knn_imputer.csv"
    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/train_clean_dataset1_no_null_knn_imputer.csv"
    #trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/train_clean_dataset1_less_feature_no_null_knn_imputer.csv"
    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_less_feature_no_null_knn_imputer.csv"
    # trainingSetFile = "/home/ubuntu/rdkit/carbon/hmdbmol/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_no_null_knn_imputer.csv"

    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_2_corrected_no_null_knn_imputer.csv"

    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_no_null_knn_imputer.csv"
    trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_two_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_modified_features_with_more_features_1st_phase_outliers_checking_correct_no_null.csv"
    # trainingSetFile = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_two_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_modified_features_with_more_features_starting_of_second_phase_no_null.csv"
    dataset_full = pd.read_csv(trainingSetFile)
    print("dataset shape:{}".format(dataset_full.shape))
    dataset_full = dataset_full.drop(columns=['Unnamed: 0'])
    # dataset_full = dataset_full.drop(columns=['C'])
    # dataset_full = dataset_full.drop(columns=['C_1'])
    # dataset_full = dataset_full.drop(columns=['C_2'])
    dataset_full = dataset_full.drop(columns=['cripenLogP'])
    dataset_full = dataset_full.drop(columns=['Molar_Refractivity'])
    dataset_full = dataset_full.drop(columns=['TPSA'])
    dataset_full = dataset_full.drop(columns=['LASA'])
    print("dataset shape after dropping Unnamed:{}".format(dataset_full.shape))


    ####### divide train data and holdout data. train data = dataset, holdout = holdout_data####
    splitter = GroupShuffleSplit(test_size=.20, n_splits=1, random_state = 7)
    split = splitter.split(dataset_full, groups=dataset_full['HMDBID'])
    train_inds, test_inds = next(split)

    ## creating files with train and holdout
    file_name_group_by_panda_series_1 = dataset_full.groupby('HMDBID')
    file_name_group_by_dict_1 = dict(list(file_name_group_by_panda_series_1))
    file_name_1 = np.array(list(file_name_group_by_dict_1.keys()))
    file_name_1 = file_name_1.reshape(file_name_1.shape[0],1)
    print("# of files = {}".format(len(file_name_1)))
    hmdb_ids_1 = dataset_full.iloc[ :,-2].values
    hmdb_ids_1 = hmdb_ids_1.reshape(hmdb_ids_1.shape[0],1)
    hydrogen_positions_1 = dataset_full.iloc[ :,-1].values
    hydrogen_positions_1 = hydrogen_positions_1.reshape(hydrogen_positions_1.shape[0],1)
    hydrogen_positions_1 = hydrogen_positions_1.ravel()
    train_1 = dataset_full.iloc[train_inds]
    test_1 = dataset_full.iloc[test_inds]
    id_train = train_1["HMDBID"].values
    id_test = test_1["HMDBID"].values
    visited = set()
    dup_train = {i for i in id_train if i in visited or (visited.add(i) or False)}
    n_f = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_jeol_clean_tms_to_dss_sorted_an_split_starting_of_2nd_phase_deleted_few_features.txt" ### change here
    n_f_f = open(n_f,"w")
    for j in dup_train:
      n_f_f.write(str(j)+"\n")
    n_f_f.close()
    visited = set()
    dup_test = {i for i in id_test if i in visited or (visited.add(i) or False)}
    n_f = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/holdout_jeol_clean_tms_dss_sorted_an_split_starting_of_2nd_phase_deleted_few_features.txt" #change here
    n_f_f = open(n_f,"w")
    for j in dup_test:
      n_f_f.write(str(j)+"\n")
    n_f_f.close()
    #### file creation done ###





    dataset = dataset_full.iloc[train_inds] # this is train data
    new_dataset = dataset.copy()
    holdout_data = dataset_full.iloc[test_inds] # this is holdout data
    new_holdout_data = holdout_data.copy()
    print("holdout dataset ID = {}".format(holdout_data["HMDBID"]))
    print("holdout dataset shape = {}".format(holdout_data.shape))

    ################################################################

    ### dropping the holdout data from BMRB/GISSMO datatset ###
    # dataset = dataset[~dataset['HMDBID'].isin(['bmse000300_simulation_1_nmredata','bmse000322_simulation_1_nmredata','bmse000330_simulation_1_nmredata','bmse000184_simulation_1_nmredata','bmse000389_simulation_1_nmredata','bmse000435_simulation_1_nmredata','bmse000856_simulation_1_nmredata','bmse000364_simulation_1_nmredata','bmse000337_simulation_1_nmredata','bmse000044_simulation_1_nmredata','bmse000915_simulation_1_nmredata','bmse000404_simulation_1_nmredata'])]
    # print("dataset shape after dropping common holdout:{}".format(dataset.shape))
    #################################



    hmdb_ids = dataset.iloc[ :,-2].values
    hmdb_ids = hmdb_ids.reshape(hmdb_ids.shape[0],1)
    hydrogen_positions = dataset.iloc[ :,-1].values
    hydrogen_positions = hydrogen_positions.reshape(hydrogen_positions.shape[0],1)

    print("nan value: {}".format(dataset.isnull().sum()))
    X = dataset.iloc[ : , 0:-3].values
    X = X.astype(float)
    ### removing normalization ##
    # scaler = preprocessing.StandardScaler().fit(X)
    # X_scaled = scaler.transform(X)
    # X_scaled_mean = scaler.mean_
    # X_scaled_scale = scaler.scale_
    # print("X_scaled_mean = {} and X_scaled_scale ={}".format(X_scaled_mean,X_scaled_scale))
    # new_dataset.iloc[:,:-3] = X_scaled
    #########
    
    y =  dataset.iloc[ :,-3].values
    y = y.reshape(y.shape[0],1)
    y = y.astype(float)
    Y = y
    ### removing normalization
    # scaler_y = preprocessing.StandardScaler().fit(Y)
    # Y_scaled = scaler_y.transform(Y)
    # new_dataset.iloc[:,-3] = Y_scaled
    # Y_scaled_mean = scaler_y.mean_
    # Y_scaled_scale = scaler_y.scale_
    # print("Y_scaled_mean = {} and X_scaled_scale ={}".format(Y_scaled_mean,Y_scaled_scale))
    # # folds = 5
    # print("dataset = {} and new_dataset ={}".format(dataset,new_dataset))
    ##########
    folds = 5
    # folds = 10
    # how to unscale---> unscaled = scaler.inverse_transform(scaled)
    cv = KFold(n_splits=folds, random_state=1, shuffle=True)
    parameter_name1 = "C"
    # parameter_range1= [100.0,10.0,1.0,0.1,0.01,0.001,0.0001]
    # parameter_range1= [0.0001,0.001,0.01,0.1,1.0,10.0,100.0]
    # parameter_range1 = [0.0001,0.01]
    # parameter_range2 = [0.11,0.12]
  
    # parameter_range1 = [0.0001,0.001,0.01,0.1,1.0,10,20,30,40,50,60,70,80,90,100]
    # parameter_range2 = [0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.2,0.01,1.0]

    # parameter_range1 = [10.0,100.0]
    # parameter_range2 = [0.1,10,20,40,50]

    # parameter_range1 = [10.0,100.0]
    # parameter_range2 = [0.20,0.30,0.40,0.50]

    # parameter_range1 = [10.0,100.0]
    # parameter_range2 = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]

    # parameter_range1 = [10.0,100.0,1000.0]
    # parameter_range2 = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]

    # parameter_range1 = [10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0]
    # parameter_range2 = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0]


    # parameter_range1 = [0.0001,0.001,0.01,0.1,1.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0,100.0,1000.0]
    # parameter_range2 = [0.1,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,100.0,1000.0,10000.0]
    # parameter_range1 = [1.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0,100.0,1000.0]
    # # parameter_range1 = [40.0,50.0,60.0,70.0,80.0,90.0,100.0,1000.0]
    # parameter_range2 = [0.1,0.2,1.0,2.0,3.0,4.0]
    # parameter_range1 = [30.0,40.0,50.0,60.0,70.0,80.0,90.0,100.0,1000.0]
    # parameter_range2 = [0.1,0.2,0.3]
    # parameter_range1= [10,20,30,40,50,100,150,200,250,300,350,400,450,500]
    # parameter_range2= [5,6,7,8,9,10,11,12,13,14,15]
    # parameter_range1= [10,20]
    # parameter_range2= [5,6]
    # parameter_range1= [10,15,20,25,30,35,40,50,55,60,65,70,80,85,90,95,100,105,110,115,120,125,130,140,145,150,155,160,165,170,180,185,190,195,200,205,210,215,250,300,350,400,450,500]
    # parameter_range2= [5,6,7,8,9,10,11,12,13,14,15,20,25,30,35,40,45,50]
    parameter_range1= [10,15,20,25,30,35,40,50,55,60,65,70,80,85,90,95,100,105,110,115,120,125,130,140,145,150,155,160,165,170,180,185,190,195,200,220,240,260,300,350,400,450,500]
    parameter_range2= [5,6,7,8,9,10,11,12]
    

    file_name_group_by_panda_series = dataset.groupby('HMDBID')
    file_name_group_by_dict = dict(list(file_name_group_by_panda_series))
    file_name = np.array(list(file_name_group_by_dict.keys()))
    file_name = file_name.reshape(file_name.shape[0],1)


    # itertation = len(parameter_range2)
    # itertation = len(parameter_range1)
    mean_scores ={}
    mean_scores_mse = {}
    mean_scores_r2 = {}
    mean_scores_train_r2 = {}
    mean_scores_train ={}
    mean_scores_mse_train ={}
    saved_models = {}
    diff_btw_train_test_mean_score = {}
    for ep in parameter_range2: #max_depth
      # for i in range(itertation):
      y_train_mae_vales = []
      y_test_mae_vales = []
      for c in parameter_range1: # of estimator
        cv_scores = []
        r2_scores = []
        cv_scores_mse = []
        saved_models_cv = []
        cv_scores_fold_pred_for_train = []
        cv_scores_mse_fold_pred_for_train = []
        r2_scores_fold_pred_for_train = []
        for (inner_train_index_filename, inner_test_index_filename), j in zip(cv.split(file_name), range(folds)):
          file_name_inner_train = file_name[inner_train_index_filename]
          file_name_inner_test = file_name[inner_test_index_filename]
  
          X_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
          # X_train_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
          # X_test_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
          X_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
  
          X_train_inner_temp = dataset[X_train_inner_new_filter]
          # X_train_inner_temp = new_dataset[X_train_inner_new_filter] #this is always the datset format after cv
          # X_test_inner_temp = new_dataset[X_test_inner_new_filter] #this is always the datset format after cv
          X_test_inner_temp = dataset[X_test_inner_new_filter]
          # y_train_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
          y_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
          # y_test_inner_new_filter = new_dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
          y_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
  
          # y_train_inner_temp = new_dataset[y_train_inner_new_filter] #this is always the datset format after cv
          y_train_inner_temp = dataset[y_train_inner_new_filter]
          # y_test_inner_temp = new_dataset[y_test_inner_new_filter] #this is always the datset format after cv
          y_test_inner_temp = dataset[y_test_inner_new_filter]

          X_train_inner = X_train_inner_temp.drop(columns=["ChemicalShift","HMDBID","CPosition"])
          X_train_inner = X_train_inner.iloc[: , :].values 
          X_train_inner = X_train_inner.astype(float)
  
  
          X_test_inner = X_test_inner_temp.drop(columns=["ChemicalShift","HMDBID","CPosition"])
          X_test_inner = X_test_inner.iloc[: , :].values 
          X_test_inner = X_test_inner.astype(float)
          # print("X_test_inner = {}".format(X_test_inner))
  
          y_train_inner = y_train_inner_temp["ChemicalShift"].values
          y_train_inner = y_train_inner.astype(float)
          y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
          # print("y_train_inner = {}".format(y_train_inner))
  
          y_test_inner = y_test_inner_temp["ChemicalShift"].values
          y_test_inner = y_test_inner.astype(float)
          y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)
          # print("y_test_inner = {}".format(y_test_inner))
  
  
  
          hmdb_ids_inner_fold_train = X_train_inner_temp["HMDBID"].values
          hmdb_ids_inner_fold_train = hmdb_ids_inner_fold_train.reshape(hmdb_ids_inner_fold_train.shape[0],1)
  
          hmdb_ids_inner_fold_test = X_test_inner_temp["HMDBID"].values
          hmdb_ids_inner_fold_test = hmdb_ids_inner_fold_test.reshape(hmdb_ids_inner_fold_test.shape[0],1)
  
  
          hydrogen_positions_inner_fold_train = X_train_inner_temp["CPosition"].values
          hydrogen_positions_inner_fold_train = hydrogen_positions_inner_fold_train.reshape(hydrogen_positions_inner_fold_train.shape[0],1)
  
          hydrogen_positions_inner_fold_test = X_test_inner_temp["CPosition"].values
          hydrogen_positions_inner_fold_test = hydrogen_positions_inner_fold_test.reshape(hydrogen_positions_inner_fold_test.shape[0],1)
  
          print("fold {} X_train_inner shape ={} and y_test_inner shape ={} ".format(j,X_train_inner.shape,y_test_inner.shape))
          model_inner = GradientBoostingRegressor(n_estimators= c, max_depth= ep, subsample=0.9)
          print("model creation done")
          # print(type(X_train_inner))
          # print(type(X_train_inner[:5,:]))
          t1 = time.time()
          model_inner.fit(X_train_inner, y_train_inner.ravel())
          print("model fitting done:{}".format(time.time()-t1))
          y_pred_inner = model_inner.predict(X_test_inner)
          y_pred_inner = y_pred_inner.astype(float)
          # y_pred_inner_original = (y_pred_inner*Y_scaled_scale)+Y_scaled_mean
          # y_pred_inner = y_pred_inner/float(100)
          y_test_inner = y_test_inner.astype(float)
          # y_test_inner_original = (y_test_inner*Y_scaled_scale)+Y_scaled_mean
          # y_test_inner = y_test_inner/float(100)
          meanAbsError_inner = mean_absolute_error(y_test_inner, y_pred_inner) # sum up of all the mae of y_pred and y_test
          # meanAbsError_inner = mean_absolute_error(y_test_inner_original, y_pred_inner_original) # sum up of all the mae of y_pred and y_test
          cv_scores.append(meanAbsError_inner)
  
          mSError_inner = mean_squared_error(y_test_inner, y_pred_inner)
          # mSError_inner = mean_squared_error(y_test_inner_original, y_pred_inner_original)
          cv_scores_mse.append(mSError_inner)
  
          r2 = r2_score(y_test_inner, y_pred_inner)
          # r2 = r2_score(y_test_inner_original, y_pred_inner_original)
          r2_scores.append(r2)
  
          y_pred_inner_for_fold_train = model_inner.predict(X_train_inner)
          y_pred_inner_for_fold_train = y_pred_inner_for_fold_train.astype(float)
          # y_pred_inner_for_fold_train_original = (y_pred_inner_for_fold_train*Y_scaled_scale)+Y_scaled_mean
          # y_train_inner_original = (y_train_inner*Y_scaled_scale)+Y_scaled_mean
          
          meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner, y_pred_inner_for_fold_train)
          # meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner_original, y_pred_inner_for_fold_train_original) 
          cv_scores_fold_pred_for_train.append(meanAbsError_inner_fold_pred_for_train)
  
          mSError_inner_fold_pred_for_train = mean_squared_error(y_train_inner, y_pred_inner_for_fold_train)
          # mSError_inner_fold_pred_for_train = mean_squared_error(y_train_inner_original, y_pred_inner_for_fold_train_original) 
          cv_scores_mse_fold_pred_for_train.append(mSError_inner_fold_pred_for_train)
  
          r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner, y_pred_inner_for_fold_train)
          # r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner_original, y_pred_inner_for_fold_train_original)
          r2_scores_fold_pred_for_train.append(r2_scores_inner_fold_pred_for_train)
  
          print("started prediction for n_estimators:{}   max_depth:{}".format(c,ep))
          for k in range(len(y_pred_inner)):
            print("Printing  prediction:{}------true:{}, HMDB_ID:{} and C position is:{}".format(y_pred_inner[k], y_test_inner[k],hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k]))
            if float(abs(y_pred_inner[k]-y_test_inner[k])) >= 3.0 :
              print("outliers{} {} true:{}  pred:{}".format(hmdb_ids_inner_fold_test[k][0],hydrogen_positions_inner_fold_test[k][0],y_test_inner[k],y_pred_inner[k]))
          print("for th:{} cv of n_estimators:{}   max_depth:{}, MAE  is:{} ".format(j,c,ep,cv_scores[j]))

          #for k in range(len(y_pred_inner_original)):
          #  print("Printing  prediction:{}------true:{}, HMDB_ID:{} and C position is:{}".format(y_pred_inner_original[k], y_test_inner_original[k],hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k]))
          #  if float(abs(y_pred_inner_original[k]-y_test_inner_original[k])) >= 3.0 :
          #    print("outliers{} {} true:{}  pred:{}".format(hmdb_ids_inner_fold_test[k][0],hydrogen_positions_inner_fold_test[k][0],y_test_inner_original[k],y_pred_inner_original[k]))
          #print("for th:{} cv of n_estimators:{}   max_depth:{}, MAE  is:{} ".format(j,c,ep,cv_scores[j]))
  
  
        mean_scores[str(c)+"_"+str(ep)] = np.mean(cv_scores)
        print("And ALL MAEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores))
        print("And AVG MAE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores)))
        y_test_mae_vales.append(np.mean(cv_scores))
        print("y_test_mae_vales = {}".format(y_test_mae_vales))

        mean_scores_mse[str(c)+"_"+str(ep)] = np.mean(cv_scores_mse)
        print("And ALL MSEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores_mse))
        print("And AVG MSE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores_mse)))
        
        
        mean_scores_r2[str(c)+"_"+str(ep)] = np.mean(r2_scores)
        print("And ALL R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,r2_scores))
        print("And AVG R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(r2_scores))) 

        mean_scores_train_r2[str(c)+"_"+str(ep)] = np.mean(r2_scores_fold_pred_for_train)
        print("And ALL Train R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,r2_scores_fold_pred_for_train))
        print("And AVG Train R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(r2_scores_fold_pred_for_train))) 
  
        mean_scores_train[str(c)+"_"+str(ep)] = np.mean(cv_scores_fold_pred_for_train)
        print("And ALL Train MAEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores_fold_pred_for_train))
        print("And AVG Train MAE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores_fold_pred_for_train)))
        y_train_mae_vales.append(np.mean(cv_scores_fold_pred_for_train))
        print("y_train_mae_vales = {}".format(y_train_mae_vales))

        mean_scores_mse_train[str(c)+"_"+str(ep)] = np.mean(cv_scores_mse_fold_pred_for_train)
        print("And ALL Train MSEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores_mse_fold_pred_for_train))
        print("And AVG Train MSE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores_mse_fold_pred_for_train)))

        train_mean = np.mean(cv_scores_fold_pred_for_train)
        test_mean = np.mean(cv_scores)
        diff_btw_train_test_mean_score[str(c)+"_"+str(ep)] = float(abs(train_mean-test_mean))
        print("And ALL diff_btw_train_test_mean_score for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,diff_btw_train_test_mean_score))

      print("for max_depth = {}, the graph is getting created:".format(ep))
      x_axis = parameter_range1
      y_train_mae_vales = [ np.round(i,2) for i in y_train_mae_vales]
      y_test_mae_vales = [ np.round(i,2) for i in y_test_mae_vales]
      print("inside graph y_train_mae_vales = {}".format(y_train_mae_vales))
      print("inside graph y_test_mae_vales = {}".format(y_test_mae_vales))
      # plt.figure(figsize=(5, 6))

    #  plt.plot(x_axis,y_train_mae_vales, label='Train Error')
    #  plt.plot(x_axis,y_test_mae_vales, label='Test Error')
#
    #  plt.xlabel('n_estimators Values',color='magenta',fontsize=12)
    #  plt.ylabel('MAE',color='magenta',fontsize=12)
#
    #  plt.title("max_depth = {}".format(ep), fontsize=18)
    #  plt.xticks([0,10,20],rotation=90, ha="center", fontsize =8)
    #  plt.yticks(np.arange(0,11,step = 0.5))
#
    #  # plt.ylim([1,50])
    #  plt.legend()
    #  # plt.show()
#
#
#
    #  plt.savefig('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/graphs_gradientboost/sorted/'+'{}.png'.format(ep),dpi = 1028)
#
    #  # plt.show()
      fig = go.Figure()
      fig.add_trace(go.Scatter(x=x_axis, y=y_train_mae_vales,
                      mode='lines+markers',
                      name='Train MAE'))
      fig.add_trace(go.Scatter(x=x_axis, y=y_test_mae_vales,
                      mode='lines+markers',
                      name='Test MAE'))
      # ≈.update_layout(
        # xaxis = dict(
          # tickmode = 'array',
          # tickvals = [0,10,20]
          # ticktext = ['One', 'Three', 'Five', 'Seven', 'Nine', 'Eleven']
        # )
      # )
      # fig.update_layout(
        # yaxis = dict(
          # tickmode = 'array',
          # tickvals = [0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0,7.5,8.0,8.5,9.0,9.5,10.0,10]
        # )
      # )
      # fig.update_xaxes(range=[10,20],showgrid=True,tickangle=45)
      # fig.update_yaxes(range = [0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0,7.5,8.0,8.5,9.0,9.5,10.0,10.5,11.0,11.5,12.0,12.5,13.0,13.5,14.0,14.5,15.0,15.5,16.0,16.5,17.0,17.5,18.0,18.5,19.0,19.5,20.0])
      # fig.update_yaxes(range = ,showgrid=True)
      fig.update_xaxes(showgrid=True,tickmode ='array',tickvals = x_axis)
      fig.update_yaxes(showgrid=True,tickangle=45,tickmode ='array',tickvals = [0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5,7.0,7.5,8.0,8.5,9.0,9.5,10.0,10.5,11.0,11.5,12.0,12.5,13.0,13.5,14.0,14.5,15.0,15.5,16.0,16.5,17.0,17.5,18.0,18.5,19.0,19.5,20.0])
      file_name_graph = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/graphs_gradientboost/sorted/"+str(ep)+".html"
      plotly.offline.plot(fig, filename=file_name_graph,auto_open=False)




    keys_of_mean_scores = list(mean_scores.keys())
    values_of_mean_scores = list(mean_scores.values())
    values_of_mean_scores_temp = copy.copy(values_of_mean_scores)
    values_of_mean_scores_temp.sort()
    index_of_lowest_mae = values_of_mean_scores.index(values_of_mean_scores_temp[0])
    best_mae = values_of_mean_scores[index_of_lowest_mae]
    print("index of lowest MAE ={}".format(index_of_lowest_mae))
    print("BEST AVG MAE:{}".format(best_mae))
    print("BEST max_depth:{}".format(keys_of_mean_scores[index_of_lowest_mae].split("_")[1]))
    print("BEST n_estimators:{}".format(keys_of_mean_scores[index_of_lowest_mae].split("_")[0]))
    print("All MAEs from all parametes:{}".format(mean_scores))
    print("All Train MAEs from all parametes:{}".format(mean_scores_train))
    print("All R2s from all parametes:{}".format(mean_scores_r2))
    sorted_d = OrderedDict(sorted(diff_btw_train_test_mean_score.items(), key=lambda t: t[1])) 
    again_d = dict(sorted_d)
    print("The differences between Train and Test AVG MAEs: {}".format(again_d))




      
    ##### SAVE MODELS #####
    # with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/saved_model_RFC_best_param.pickle', 'wb') as handle:
    #   pickle.dump(saved_models, handle, protocol=pickle.HIGHEST_PROTOCOL)









if __name__ == '__main__':
  main()
