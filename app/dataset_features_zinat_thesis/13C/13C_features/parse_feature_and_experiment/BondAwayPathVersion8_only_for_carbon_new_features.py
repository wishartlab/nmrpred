
import glob
import numpy as np
import pandas as pd
import itertools
from rdkit.Chem import AllChem as Chem
from collections import OrderedDict
from io import StringIO
import sys







# mol = Chem.MolFromMolFile("HMDB0000001.sdf", sanitize=True, removeHs=False)
# Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
# conf = mol.GetConformer(0)
# ringinfo = mol.GetRingInfo()
# ringinfo.NumAtomRings(2)


###### 1 bond away path ##
def OneBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond_atom_number = set()
  new_1bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(atom[0]).GetNeighbors())
  new_1bond_atom_number = new_1bond_atom_number.union(set(atom))
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond_atom_number))
  return new_1bond_atom_number

def OneBondAwayAtoms_carbon(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond_atom_number = set()
  new_1bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(atom[0]).GetNeighbors())
  # new_1bond_atom_number = new_1bond_atom_number.union(set(atom))
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond_atom_number))
  return new_1bond_atom_number


def TwoBondAwayAtoms_carbon(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  y = set()
  y = {atom_number}
  new_1bond = OneBondAwayAtoms_carbon(atom[0],mol)
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond))
  new_2bond_atom_number = set()
  for s in new_1bond:
    new_2bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("new_2bond_atom_number = {}".format(new_2bond_atom_number))
  # print(new_2bond_atom_number - set(new_1bond)) # without all atom along the path
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond_atom_number))
  # return new_2bond_atom_number
  final_atoms = new_2bond_atom_number - set(new_1bond)
  final_atoms2 = final_atoms - y
  print("returning 2 bond away atom numbers = {}".format(final_atoms2))
  return  final_atoms2


#### 2 bond away ###
def TwoBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond = OneBondAwayAtoms(atom[0],mol)
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond))
  new_2bond_atom_number = set()
  for s in new_1bond:
    new_2bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())

  print(new_2bond_atom_number - set(new_1bond)) # without all atom along the path
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond_atom_number))
  return new_2bond_atom_number
  return  new_2bond_atom_number - set(new_1bond)


def ThreeBondAwayAtoms_carbon(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  y = set()
  y = {atom_number}
  new_2bond = TwoBondAwayAtoms_carbon(atom[0],mol)
  new_1bond = OneBondAwayAtoms_carbon(atom[0],mol)
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond))
  new_3bond_atom_number = set()
  for s in new_2bond:
    new_3bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("new_3bond_atom_number = {}".format(new_3bond_atom_number))
  # print(new_2bond_atom_number - set(new_1bond)) # without all atom along the path
  print("atoms in three bond away for atom:{} ={}".format(atom[0],new_3bond_atom_number))
  # return new_2bond_atom_number
  final_atoms = new_3bond_atom_number - set(new_2bond)
  final_atoms2 = final_atoms - set(new_1bond)
  final_atoms3 = final_atoms2 - y
  print("returning 3 bond away atom numbers = {}".format(final_atoms3))
  return  final_atoms3

### 3 bond away ###
def ThreeBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_2bond = TwoBondAwayAtoms(atom[0],mol)
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond))
  new_3bond_atom_number = set()
  for s in new_2bond:
    new_3bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in three bond away for atom:{} ={}".format(atom[0],new_3bond_atom_number))
  print(new_3bond_atom_number - set(new_2bond))
  return new_3bond_atom_number
  # return  new_3bond_atom_number - set(new_2bond)

### 4 bond away ###
def FourBondAwayAtoms(atom_number,mol):
  atom = [atom_number]
  new_3bond = ThreeBondAwayAtoms(atom[0],mol)
  new_4bond_atom_number = set()
  for s in new_3bond:
    new_4bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in four bond away for atom:{} ={}".format(atom[0],new_4bond_atom_number))

  print(new_4bond_atom_number - set(new_3bond))
  return new_4bond_atom_number
  # return  new_4bond_atom_number - set(new_3bond)

#### Create Finger Prints ####

#Total 44
SMARTS_FUNCTIONS = OrderedDict()
SMARTS_FUNCTIONS["Aldehyde"]="[$([CX3H1]=[OX1]);!$([CX3](=[OX1])[OX2H1])]"
SMARTS_FUNCTIONS["Ketones"]="C(=O)(C)(C)"
SMARTS_FUNCTIONS["Carboxylic_Acid"]="[CX3](=[OX1])[OX2H1]"
SMARTS_FUNCTIONS["Ester"]="C(=[OX1])[OX2;H0]"
SMARTS_FUNCTIONS["Amide"]="C(=[OX1])[NX3H2]"
SMARTS_FUNCTIONS["Urethanes"]="[NX3H2][CX3](=[OX1])[OX2]"
SMARTS_FUNCTIONS["Olefinic"]="[CX3;R0]=[CX3;R0]"
SMARTS_FUNCTIONS["Nitrile"]="C#N"
# SMARTS_FUNCTIONS["Anomeric"]="C([OH])O"
SMARTS_FUNCTIONS["Anomeric"]="[CR]([OH])[OR]"
# SMARTS_FUNCTIONS["CR2OH"]="[CX4][OX2H1]"
SMARTS_FUNCTIONS["CR2OH"]="[CX4H0][OX2H1]"
SMARTS_FUNCTIONS["CH2OH"]="[CX4H2][OX2H1]"
SMARTS_FUNCTIONS["CH3OH"]="[CX4H3][OX2H1]"
SMARTS_FUNCTIONS["COH"]="[CX4H0][OX2H1]"
SMARTS_FUNCTIONS["C-O"]="[$(C[OX2]);!$([CX3](=[OX1])[OX2H1])]"
SMARTS_FUNCTIONS["C-N"]="C[NX3]"
SMARTS_FUNCTIONS["Carbonyl"]="[$([CX3H0]=[OX1]);!$([CX3](=[OX1])[OX2H1])]" #"[$([C][!OX2])]=[OX1]" "[]"
# SMARTS_FUNCTIONS["C"]="C[X4]"
# SMARTS_FUNCTIONS["C"]="[CX4H0]"
SMARTS_FUNCTIONS["CH"]="[CX4H1]"
SMARTS_FUNCTIONS["CH2"]="[CX4H2]"
SMARTS_FUNCTIONS["CH3"]="[CX4H3]"
SMARTS_FUNCTIONS["[Cl]"]="[Cl]"
SMARTS_FUNCTIONS["[F]"]="[F]"
SMARTS_FUNCTIONS["[Br]"]="[Br]"
SMARTS_FUNCTIONS["[I]"]="[I]"
# SMARTS_FUNCTIONS["connected_with_aromatic_atom"]="[aR1]"
# SMARTS_FUNCTIONS["connected_with_aliphatic_ring_atom"]="[AR1]"
SMARTS_FUNCTIONS["connected_with_aromatic_atom"]="[aR]"
SMARTS_FUNCTIONS["connected_with_aliphatic_ring_atom"]="[AR]"
# SMARTS_FUNCTIONS["glucose"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])CO1"
# SMARTS_FUNCTIONS["ribose"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])O1"
# SMARTS_FUNCTIONS["sugar1"] = "O1[CH]([OH])CCCC1"
# SMARTS_FUNCTIONS["sugar2"] = "O1[CH]([OH])CCC1"
SMARTS_FUNCTIONS["S_O"] = "[SX3](=[OX1])"
SMARTS_FUNCTIONS["SCH3"] = "S[CX4H3]"
SMARTS_FUNCTIONS["SCH2"] = "S[CX4H2]"
SMARTS_FUNCTIONS["CH3_with_N+"] = "[N+][CX4H3]"
SMARTS_FUNCTIONS["pyridine"]="[cR1]1[nR1][cR1][cR1][cR1][cR1]1"
SMARTS_FUNCTIONS["C=N"] = "C=N"
SMARTS_FUNCTIONS["N_CH3"]="[CX4H3]N" 
SMARTS_FUNCTIONS["Two_N_ring_=O_NH2"] = "[cR1]1[cR1][nR1][cR1](=O)[nR1][cR1]1([NX3H2])" #2N in benzene with =O and -NH2
SMARTS_FUNCTIONS["NH"]="[NX3H1]"
SMARTS_FUNCTIONS["NH2"]="[NX3H2]"
SMARTS_FUNCTIONS["NH3"]="[NX3H3]"
SMARTS_FUNCTIONS["N"]="[NX3H0]"
SMARTS_FUNCTIONS["PO4"]="[PX4](=[OX1])(-[OX2])(-[OX2])(-[OX2])"  
SMARTS_FUNCTIONS["PO4-"]="[PX4](=[OX1])(-[O-])(-[OX2])(-[OX2])"
SMARTS_FUNCTIONS["Hydroxyl"]="[$([OX2H1]);!$([CX3](=[OX1])[OX2H1])]"
# SMARTS_FUNCTIONS["Hydroxyl_with_aromatic"]="[$(c[OX2H1]);!$([CX3](=[OX1])[OX2H1])]"
SMARTS_FUNCTIONS["Alkene_in_ring"]="[CX3;R]=[CX3;R]"
SMARTS_FUNCTIONS["Alkyne"]="[CX2]#[CX2]"
SMARTS_FUNCTIONS["CHCOOH"] = "[CX4H1][CX3](=[OX1])[OX2H1]"
SMARTS_FUNCTIONS["COO-"]="[CX3](=[OX1])([-OX1])"
SMARTS_FUNCTIONS["CH2N+"]="[CX4H2][N+]"
SMARTS_FUNCTIONS["N_in_6_ring"] = "[Nr6]"
SMARTS_FUNCTIONS["NHCO"]="[NX3H1][CX3]=[OX1]"
SMARTS_FUNCTIONS["O_inside_6_ring"]="[CR1]1[CR1][CR1][CR1][CR1][O]1" 
# SMARTS_FUNCTIONS["Imidazole"]="[nH1]1[cR1][nR1][cR1][cR1]1"
SMARTS_FUNCTIONS["CHNH2"] = "[CH1][NX3H2]"
SMARTS_FUNCTIONS["CH2NH2"] = "[CH2][NX3H2]"
SMARTS_FUNCTIONS["CH2NH"]="[CX4H2][NX3H1]"
SMARTS_FUNCTIONS["C=NOH"]="C=[NX2H0][OX2H1]"
SMARTS_FUNCTIONS["PO_OH2"]="[PX4](=[OX1])(-[OX2H1])(-[OX2H1])"
SMARTS_FUNCTIONS["SO2OH"]="[SX4](=[OX1])(=[OX1])(-[OX2H1])"
SMARTS_FUNCTIONS["Benzene"]="[cR1]1[cR1][cR1][cR1][cR1][cR1]1"
SMARTS_FUNCTIONS["N_3CH3"]="[NX3]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["N+_3CH3"]="[N+]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["O_inside_5_ring"]="[CR1]1[CR1][CR1][CR1][O]1"
SMARTS_FUNCTIONS["Two_N_in_aromatic_with_=O_and_-NH2"] = "[cR1]1[cR1][nR1][cR1](=O)[nR1][cR1]1([NX3H2])"
SMARTS_FUNCTIONS["Furan"]="[cR1]1[cR1][cR1][cR1][oR1]1"
SMARTS_FUNCTIONS["6_ring_without_any_double_bond"]="[CR1]1[CR1][CR1][CR1][CR1][CR1]1" 
SMARTS_FUNCTIONS["As=O-OH"]="[AsX4](=[OX1])([OX2H1])"
SMARTS_FUNCTIONS["5_ring_with_2N_6_ring_with_2N"]= "c12cncnc1ncn2" 
SMARTS_FUNCTIONS["glucose_4OH"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])C([OH])O1"
SMARTS_FUNCTIONS["5_ring_with_one_nitroen"]="[CR1]1[NR1][CR1][CR1][CR1]1"
SMARTS_FUNCTIONS["Imidazole"]="[nH1]1[cR1][nR1][cR1][cR1]1"
SMARTS_FUNCTIONS["Two_N_in_aromatic_with_2_=O"] = "[cR1]1(=O)[nR1H1][cR1](=O)[nR1H0][cR1][cR1]1"
SMARTS_FUNCTIONS["Two_N_in_5_ring"]="[cR1]1[cR1][nR1H0][cR1][nR1H0]1"
# SMARTS_FUNCTIONS["Two_N_in_aromatic_with_=O_and_-NH2"] = "[cR1]1[cR1][nR1][cR1](=O)[nR1][cR1]1([NX3H2])"
SMARTS_FUNCTIONS["C-S-C"] = "C[SX2;R0]C"
SMARTS_FUNCTIONS["5ring_with_2N_6_ring_with_2N"]= "c12cncnc1ncn2"
SMARTS_FUNCTIONS["infused_benzene_ring_one_with_5ring_N"]= "c12nccc1cccc2"
# SMARTS_FUNCTIONS["glucose"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])CO1"### think later

# SMARTS_FUNCTIONS["C1CC2NC1CCC2"] = "C1CC2NC1CCC2"### think later
# SMARTS_FUNCTIONS["S=O"]="[SX3](=[OX1])" ### think later






# SMARTS_FUNCTIONS["Carbonyl"]="[$([CX3]=[OX1]);!$([CX3](=[OX1])[OX2H1])]" #"[$([C][!OX2])]=[OX1]" "[]"
# SMARTS_FUNCTIONS["-CH3"]="[CX4H3]"
# SMARTS_FUNCTIONS["CH2"]="[CX4H2]"
# SMARTS_FUNCTIONS["CH"]="[CX4H1]"
# SMARTS_FUNCTIONS["-CH2OH"]="[CX4H2][OX2H1]"
# SMARTS_FUNCTIONS["phenyl"]="[cH0]1[cH1][cH1][cH1][cH1][cH1]1"
# SMARTS_FUNCTIONS["-OH"]="[OX2H1]"
# SMARTS_FUNCTIONS["-NH2"]="[NX3H2]"
# SMARTS_FUNCTIONS["-COOH"]="[CX3](=[OX1])[OX2H1]"
# SMARTS_FUNCTIONS["-CONH2"]="[CX3](=[OX1])[NX3H2]"
# SMARTS_FUNCTIONS["-C=C-"]="[CX3]=[CX3]"
# SMARTS_FUNCTIONS["-C triple bond C-"]="[CX3]#[CX3]"
# SMARTS_FUNCTIONS["-CO-"]="[CX3](=[O])"
# SMARTS_FUNCTIONS["-COOR"]="[CX3](=[OX1])[OX2]"
# SMARTS_FUNCTIONS["-C-O-"]="[CX4]([OX2])"
# SMARTS_FUNCTIONS["[-C-Cl]"]="[CX4][Cl]"
# SMARTS_FUNCTIONS["[-C-Br]"]="[CX4][Br]"
# SMARTS_FUNCTIONS["[-C-I]"]="[CX4][I]"
# SMARTS_FUNCTIONS["[-C-]"]="[CX4]"






def OneBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index, mol)
  one_bond_finger_print = str()
  if atoms_one_bond_away:
    print("There are atoms one bond away:{}".format(atoms_one_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      one_bond_finger_print = one_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(one_bond_finger_print))
  else:
    one_bond_finger_print = str(999)
  print("returning one bond away finger print={}".format(one_bond_finger_print))
  one_bond_finger_print1 = str()
  one_bond_finger_print2 = str()
  one_bond_finger_print3 = str()
  one_bond_finger_print4 = str()
  one_bond_finger_print5 = str()
  one_bond_finger_print6 = str()
  one_bond_finger_print7 = str()
  one_bond_finger_print8 = str()
  for s in range(len(one_bond_finger_print)):
    one_bond_finger_print1 = one_bond_finger_print1 + one_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(one_bond_finger_print)):
    one_bond_finger_print2 = one_bond_finger_print2 + one_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(one_bond_finger_print)):
    one_bond_finger_print3 = one_bond_finger_print3 + one_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(one_bond_finger_print)):
    one_bond_finger_print4 = one_bond_finger_print4 + one_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(one_bond_finger_print)):
    one_bond_finger_print5 = one_bond_finger_print5 + one_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(one_bond_finger_print)):
    one_bond_finger_print6 = one_bond_finger_print6 + one_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(one_bond_finger_print)):
    one_bond_finger_print7 = one_bond_finger_print7 + one_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(one_bond_finger_print)):
    one_bond_finger_print8 = one_bond_finger_print8 + one_bond_finger_print[s]
    
  return one_bond_finger_print1,one_bond_finger_print2,one_bond_finger_print3,one_bond_finger_print4,one_bond_finger_print5,one_bond_finger_print6,one_bond_finger_print7,one_bond_finger_print8


def TwoBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_two_bond_away = TwoBondAwayAtoms(atom_number_with_zero_index, mol)
  two_bond_finger_print = str()
  if atoms_two_bond_away:
    print("There are atoms one bond away:{}".format(atoms_two_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_two_bond_away - (atoms_two_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      two_bond_finger_print = two_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(two_bond_finger_print))
  else:
    two_bond_finger_print = str(999)
  print("returning two bond away finger print={}".format(two_bond_finger_print))
  two_bond_finger_print1 = str()
  two_bond_finger_print2 = str()
  two_bond_finger_print3 = str()
  two_bond_finger_print4 = str()
  two_bond_finger_print5 = str()
  two_bond_finger_print6 = str()
  two_bond_finger_print7 = str()
  two_bond_finger_print8 = str()
  for s in range(len(two_bond_finger_print)):
    two_bond_finger_print1 = two_bond_finger_print1 + two_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(two_bond_finger_print)):
    two_bond_finger_print2 = two_bond_finger_print2 + two_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(two_bond_finger_print)):
    two_bond_finger_print3 = two_bond_finger_print3 + two_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(two_bond_finger_print)):
    two_bond_finger_print4 = two_bond_finger_print4 + two_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(two_bond_finger_print)):
    two_bond_finger_print5 = two_bond_finger_print5 + two_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(two_bond_finger_print)):
    two_bond_finger_print6 = two_bond_finger_print6 + two_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(two_bond_finger_print)):
    two_bond_finger_print7 = two_bond_finger_print7 + two_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(two_bond_finger_print)):
    two_bond_finger_print8 = two_bond_finger_print8 + two_bond_finger_print[s]
    
  return two_bond_finger_print1,two_bond_finger_print2,two_bond_finger_print3,two_bond_finger_print4,two_bond_finger_print5,two_bond_finger_print6,two_bond_finger_print7,two_bond_finger_print8,
  # return two_bond_finger_print


def ThreeBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_three_bond_away = ThreeBondAwayAtoms(atom_number_with_zero_index, mol)
  three_bond_finger_print = str()
  if atoms_three_bond_away:
    print("There are atoms one bond away:{}".format(atoms_three_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_three_bond_away - (atoms_three_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      three_bond_finger_print = three_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(three_bond_finger_print))
  else:
    three_bond_finger_print = str(999)
  print("returning three bond away finger print={}".format(three_bond_finger_print))
  three_bond_finger_print1 = str()
  three_bond_finger_print2 = str()
  three_bond_finger_print3 = str()
  three_bond_finger_print4 = str()
  three_bond_finger_print5 = str()
  three_bond_finger_print6 = str()
  three_bond_finger_print7 = str()
  three_bond_finger_print8 = str()
  for s in range(len(three_bond_finger_print)):
    three_bond_finger_print1 = three_bond_finger_print1 + three_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(three_bond_finger_print)):
    three_bond_finger_print2 = three_bond_finger_print2 + three_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(three_bond_finger_print)):
    three_bond_finger_print3 = three_bond_finger_print3 + three_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(three_bond_finger_print)):
    three_bond_finger_print4 = three_bond_finger_print4 + three_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(three_bond_finger_print)):
    three_bond_finger_print5 = three_bond_finger_print5 + three_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(three_bond_finger_print)):
    three_bond_finger_print6 = three_bond_finger_print6 + three_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(three_bond_finger_print)):
    three_bond_finger_print7 = three_bond_finger_print7 + three_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(three_bond_finger_print)):
    three_bond_finger_print8 = three_bond_finger_print8 + three_bond_finger_print[s]
    
  return three_bond_finger_print1,three_bond_finger_print2,three_bond_finger_print3,three_bond_finger_print4,three_bond_finger_print5,three_bond_finger_print6,three_bond_finger_print7,three_bond_finger_print8,
  # return three_bond_finger_print



# def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
#   mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
#   atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
#   four_bond_finger_print = []
#   if atoms_four_bond_away:
#     print("There are atoms one bond away:{}".format(atoms_four_bond_away))
#     for i in SMARTS_FUNCTIONS.keys():
#       temp = False
#       print("started ======={}=======".format(i))
#       match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
#       match_atoms = mol.GetSubstructMatches(match)
#       if match_atoms:
#         print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
#         for j in range(len(match_atoms)):
#           commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
#           if commonalities:
#             print("found match")
#             temp = True
#             break
#           else:
#             print("no match")
#             temp = False
#       else:
#         print("no match atom for this key")
#         temp = False
#       four_bond_finger_print.append(int(temp))
#       print("final finger print in this key:{}".format(four_bond_finger_print))
#   else:
#     four_bond_finger_print.append(999)
#   print("returning four bond away finger print={}".format(four_bond_finger_print))
#   ###### not required ####
#   # four_bond_finger_print1 = str()
#   # four_bond_finger_print2 = str()
#   # four_bond_finger_print3 = str()
#   # four_bond_finger_print4 = str()
#   # four_bond_finger_print5 = str()
#   # four_bond_finger_print6 = str()
#   # four_bond_finger_print7 = str()
#   # four_bond_finger_print8 = str()
#   # for s in range(len(four_bond_finger_print)):
#   #   four_bond_finger_print1 = four_bond_finger_print1 + four_bond_finger_print[s]
#   #   if s == 6:
#   #     break
#   # for s in range(6,len(four_bond_finger_print)):
#   #   four_bond_finger_print2 = four_bond_finger_print2 + four_bond_finger_print[s]
#   #   if s == 12:
#   #     break
#   # for s in range(12,len(four_bond_finger_print)):
#   #   four_bond_finger_print3 = four_bond_finger_print3 + four_bond_finger_print[s]
#   #   if  s==18:
#   #     break
#   # for s in range(18,len(four_bond_finger_print)):
#   #   four_bond_finger_print4 = four_bond_finger_print4 + four_bond_finger_print[s]
#   #   if  s==24:
#   #     break
#   # for s in range(24,len(four_bond_finger_print)):
#   #   four_bond_finger_print5 = four_bond_finger_print5 + four_bond_finger_print[s]
#   #   if  s==30:
#   #     break
#   # for s in range(30,len(four_bond_finger_print)):
#   #   four_bond_finger_print6 = four_bond_finger_print6 + four_bond_finger_print[s]
#   #   if  s==36:
#   #     break
#   # for s in range(36,len(four_bond_finger_print)):
#   #   four_bond_finger_print7 = four_bond_finger_print7 + four_bond_finger_print[s]
#   #   if  s==42:
#   #     break
#   # for s in range(42,len(four_bond_finger_print)):
#   #   four_bond_finger_print8 = four_bond_finger_print8 + four_bond_finger_print[s]
    
#   # return four_bond_finger_print1,four_bond_finger_print2,four_bond_finger_print3,four_bond_finger_print4,four_bond_finger_print5,four_bond_finger_print6,four_bond_finger_print7,four_bond_finger_print8,
#   #### not required #####
#   return four_bond_finger_print

def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("There are atoms one bond away:{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      print("i = {}".format(i))
      if i != "[Cl]" and i != "[F]" and i != "[Br]" and i != "[I]" and i != "Hydroxyl":
        temp = 0
        print("started ======={}=======".format(i))
        match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
        match_atoms = mol.GetSubstructMatches(match)
        if match_atoms:
          print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
          for j in range(len(match_atoms)):
            commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
            if commonalities:
              print("found match")
              temp = temp + 1
              # break
            else:
              print("no match")
              temp = temp + 0
        else:
          print("no match atom for this key")
          temp = temp + 0
        four_bond_finger_print.append(int(temp))
        print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(79)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print


def carbon_fp(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index}
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index,mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      print("match ={}".format(match))
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          if i == "C Aromatic N":
            commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          else:
            commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if commonalities:
            print("found match")
            temp = temp + 1
            break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      four_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(9)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print


def carbon_fp_zero_bond(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_zero_bond_away = {atom_number_with_zero_index}
  # atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index,mol)
  zero_bond_finger_print = []
  if atoms_zero_bond_away:
    print("The atom :{}".format(atoms_zero_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      if i != "[Cl]" and i != "[F]" and i != "[Br]" and i != "[I]" and i != "Hydroxyl" and i != "S_O" and i != "NH" and i != "NH3" and i != "NH2" and i != "N" and i != "PO4" and i != "PO4-" and i != "N_in_6_ring" and i != "PO_OH2" and i != "SO2OH" and i != "As=O-OH":
        temp = 0
        print("started ======={}=======and temp ={}".format(i,str(temp)))
        match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
        print("match ={}".format(match))
        match_atoms = mol.GetSubstructMatches(match)
        if match_atoms:
          print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
          for j in match_atoms:
            if atom_number_with_zero_index in j:
              temp = 1
              # commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
              # print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
              print("atom_number_with_zero_index {} for group:{} is found and temp ={}".format(atom_number_with_zero_index,SMARTS_FUNCTIONS[i],str(temp)))
              break
            else:
              # commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
              # print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
              temp = 0
            # if commonalities:
            #   print("found match")
            #   temp = temp + 1
            #   break
            # else:
            #   print("no match")
            #   temp = temp + 0
        else:
          print("no match atom for this key")
          temp = 0
        zero_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(zero_bond_finger_print))
  else:
    zero_bond_finger_print = [999.0 for i in range(9)]
  print("returning zero bond away finger print={}".format(zero_bond_finger_print))
  print("returning zero bond away finger print len={}".format(len(zero_bond_finger_print)))
  return zero_bond_finger_print

def carbon_fp_one_bond(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index}
  atoms_one_bond_away = OneBondAwayAtoms_carbon(atom_number_with_zero_index,mol) #atoms in one bond away for atom:1 ={0, 2, 3, 4}
  one_bond_finger_print = []
  if atoms_four_bond_away:
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS2.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      print("match ={}".format(match))
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          # if i == "C Aromatic N":
          #   commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          # else:
          #   commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if commonalities:
            print("found match")
            temp = temp + 1
            # break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      print("final temp = {}".format(temp))
      one_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(one_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(9)]
  print("returning four bond away finger print={}".format(one_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(one_bond_finger_print)))
  return one_bond_finger_print

def carbon_fp_one_bond_correct(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index}
  atoms_one_bond_away = OneBondAwayAtoms_carbon(atom_number_with_zero_index,mol) #atoms in one bond away for atom:1 ={0, 2, 3, 4}
  print("atom of interest = {}".format(atoms_four_bond_away))
  print("atoms_one_bond_away = {}".format(atoms_one_bond_away))
  one_bond_finger_print = []
  if atoms_four_bond_away:
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      print("match ={}".format(match))
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          # if i == "C Aromatic N":
          #   commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          # else:
          #   commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if atom_number_with_zero_index not in set(match_atoms[j]):

            commonalities = atoms_one_bond_away.intersection(set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
            if commonalities:
              print("found match")
              temp = temp + 1
              # break
            else:
              print("no match")
              temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      print("final temp = {}".format(temp))
      one_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(one_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(9)]
  print("returning one bond away finger print={}".format(one_bond_finger_print))
  print("returning one bond away finger print len={}".format(len(one_bond_finger_print)))
  return one_bond_finger_print


def carbon_fp_two_bond_correct(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index} #the actual atom
  print("atom of interest = {}".format(atoms_four_bond_away))
  atoms_two_bond_away = TwoBondAwayAtoms_carbon(atom_number_with_zero_index,mol) #atoms in one bond away for atom:1 ={0, 2, 3, 4}
  print("atoms_two_bond_away = {}".format(atoms_two_bond_away))
  two_bond_finger_print = []
  if atoms_four_bond_away: # the original atom
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      print("match ={}".format(match))
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          # if i == "C Aromatic N":
          #   commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          # else:
          #   commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if atom_number_with_zero_index not in set(match_atoms[j]):

            commonalities = atoms_two_bond_away.intersection(set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
            if commonalities:
              print("found match")
              temp = temp + 1
              # break
              print("and temp = {}".format(temp))
            else:
              print("no match")
              temp = temp + 0
              print("and temp = {}".format(temp))
      else:
        print("no match atom for this key")
        temp = temp + 0
      print("final temp = {}".format(temp))
      two_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(two_bond_finger_print))
  else:
    two_bond_finger_print = [999.0 for i in range(9)]
  print("returning two bond away finger print={}".format(two_bond_finger_print))
  print("returning two bond away finger print len={}".format(len(two_bond_finger_print)))
  return two_bond_finger_print


def carbon_fp_three_bond_correct(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index} #the actual atom
  print("atom of interest = {}".format(atoms_four_bond_away))
  atoms_three_bond_away = ThreeBondAwayAtoms_carbon(atom_number_with_zero_index,mol) #atoms in one bond away for atom:1 ={0, 2, 3, 4}
  print("atoms_three_bond_away = {}".format(atoms_three_bond_away))
  three_bond_finger_print = []
  if atoms_four_bond_away: # the original atom
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      print("match ={}".format(match))
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          # if i == "C Aromatic N":
          #   commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          # else:
          #   commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          #   print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if atom_number_with_zero_index not in set(match_atoms[j]):

            commonalities = atoms_three_bond_away.intersection(set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
            if commonalities:
              print("found match")
              temp = temp + 1
              # break
              print("and temp = {}".format(temp))
            else:
              print("no match")
              temp = temp + 0
              print("and temp = {}".format(temp))
      else:
        print("no match atom for this key")
        temp = temp + 0
      print("final temp = {}".format(temp))
      three_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(three_bond_finger_print))
  else:
    three_bond_finger_print = [999.0 for i in range(9)]
  print("returning two bond away finger print={}".format(three_bond_finger_print))
  print("returning two bond away finger print len={}".format(len(three_bond_finger_print)))
  return three_bond_finger_print

def bond_type_one_bond_away(atom_id,mol):
  one = OneBondAwayAtoms_carbon(atom_id,mol)
  path_one = {(Chem.GetShortestPath(mol,atom_id,chi)) for chi in one}
  b_type = []
  for i in path_one:
    if atom_id in i:
      l = list(i)
      bond_type = str(mol.GetBondBetweenAtoms(l[0],l[1]).GetBondType())
      if bond_type == 'SINGLE':
        b_type_temp = 1
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
      elif bond_type == 'DOUBLE':
        b_type_temp = 2
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
      elif bond_type == 'TRIPLE':
        b_type_temp = 3
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
      elif bond_type == 'AROMATIC':
        b_type_temp = 4
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))     
  return b_type

def bond_type_one_bond_away_is_aromatic(atom_id,mol):
  one = OneBondAwayAtoms_carbon(atom_id,mol)
  path_one = {(Chem.GetShortestPath(mol,atom_id,chi)) for chi in one}
  b_type = []
  for i in path_one:
    if atom_id in i:
      l = list(i)
      if mol.GetBondBetweenAtoms(l[0],l[1]).GetIsAromatic():
        b_type_temp = 1
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
      else:
        b_type_temp = 0
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
  return b_type

def bond_type_two_bond_away(atom_id,mol):
  two = TwoBondAwayAtoms_carbon(atom_id,mol)
  path_two = {(Chem.GetShortestPath(mol,atom_id,chi)) for chi in two}
  b_type = []
  for i in path_two:
    if atom_id in i:
      l = list(i)
      bond_type = str(mol.GetBondBetweenAtoms(l[1],l[2]).GetBondType())
      if bond_type == 'SINGLE':
        b_type_temp = 1
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
      elif bond_type == 'DOUBLE':
        b_type_temp = 2
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
        return b_type
      elif bond_type == 'TRIPLE':
        b_type_temp = 3
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
      elif bond_type == 'AROMATIC':
        b_type_temp = 4
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))     
  return b_type

def bond_type_two_bond_away_is_aromatic(atom_id,mol):
  two = TwoBondAwayAtoms_carbon(atom_id,mol)
  path_two = {(Chem.GetShortestPath(mol,atom_id,chi)) for chi in two}
  b_type = []
  for i in path_two:
    if atom_id in i:
      l = list(i)
      if mol.GetBondBetweenAtoms(l[1],l[2]).GetIsAromatic():
        b_type_temp = 1
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
        return b_type
      else:
        b_type_temp = 0
        b_type.append(b_type_temp)
        print("for bond {} the bond type is {}".format(l,b_type))
  return b_type






  











