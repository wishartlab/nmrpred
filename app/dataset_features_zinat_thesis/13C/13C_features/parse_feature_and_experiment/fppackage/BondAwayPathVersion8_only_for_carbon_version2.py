
import glob
import numpy as np
import pandas as pd
import itertools
from rdkit.Chem import AllChem as Chem
from collections import OrderedDict







# mol = Chem.MolFromMolFile("HMDB0000001.sdf", sanitize=True, removeHs=False)
# Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
# conf = mol.GetConformer(0)
# ringinfo = mol.GetRingInfo()
# ringinfo.NumAtomRings(2)


###### 1 bond away path ##
def OneBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond_atom_number = set()
  new_1bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(atom[0]).GetNeighbors())
  new_1bond_atom_number = new_1bond_atom_number.union(set(atom))
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond_atom_number))
  return new_1bond_atom_number


#### 2 bond away ###
def TwoBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond = OneBondAwayAtoms(atom[0],mol)
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond))
  new_2bond_atom_number = set()
  for s in new_1bond:
    new_2bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())

  print(new_2bond_atom_number - set(new_1bond)) # without all atom along the path
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond_atom_number))
  return new_2bond_atom_number
  return  new_2bond_atom_number - set(new_1bond)

### 3 bond away ###
def ThreeBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_2bond = TwoBondAwayAtoms(atom[0],mol)
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond))
  new_3bond_atom_number = set()
  for s in new_2bond:
    new_3bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in three bond away for atom:{} ={}".format(atom[0],new_3bond_atom_number))
  print(new_3bond_atom_number - set(new_2bond))
  return new_3bond_atom_number
  # return  new_3bond_atom_number - set(new_2bond)

### 4 bond away ###
def FourBondAwayAtoms(atom_number,mol):
  atom = [atom_number]
  new_3bond = ThreeBondAwayAtoms(atom[0],mol)
  new_4bond_atom_number = set()
  for s in new_3bond:
    new_4bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in four bond away for atom:{} ={}".format(atom[0],new_4bond_atom_number))

  print(new_4bond_atom_number - set(new_3bond))
  return new_4bond_atom_number
  # return  new_4bond_atom_number - set(new_3bond)

#### Create Finger Prints ####

## in to tal 112 ###
SMARTS_FUNCTIONS = OrderedDict()
SMARTS_FUNCTIONS["Carbonyl"]="[$([CX3]=[OX1]);!$([CX3](=[OX1])[OX2H1])]" #"[$([C][!OX2])]=[OX1]" "[]"
SMARTS_FUNCTIONS["-CH3"]="[CX4H3]"
SMARTS_FUNCTIONS["CH2"]="[CX4H2]"
SMARTS_FUNCTIONS["CH"]="[CX4H1]"
SMARTS_FUNCTIONS["Aldehyde"]="[CX3H1]=[OX1]"
SMARTS_FUNCTIONS["C=N"]="[CX3]=[NX2]"
SMARTS_FUNCTIONS["-NC"]="*-[N+]#[C-]"
SMARTS_FUNCTIONS["-C one bond N"]="*-C#N"
SMARTS_FUNCTIONS["aromatic CH"]="[cH1]"
SMARTS_FUNCTIONS["C Aromatic N"]="C[n]"
SMARTS_FUNCTIONS["-CH2CH3"]="[CX4H2][CX4H3]"
SMARTS_FUNCTIONS["-CH(CH3)2"]="[CX4H]([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-CH2CH2CH2CH3"]="[CX4H2][CX4H2][CX4H2][CX4H3]"
SMARTS_FUNCTIONS["-C(CH3)3"]="[CX4]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["Cyclopropyl"]="[CX3H2]1[CX3H2][CX3H]1"
SMARTS_FUNCTIONS["CH2Cl"]="[CX3H2][Cl]"
SMARTS_FUNCTIONS["[CH2Br"]="[CX3H2][Br]"
SMARTS_FUNCTIONS["CF3"]="[CX4](F)(F)(F)"
SMARTS_FUNCTIONS["CCl3"]="[CX4](Cl)(Cl)(Cl)"
SMARTS_FUNCTIONS["-CH2OH"]="[CX4H2][OX2H1]"
SMARTS_FUNCTIONS["-CHOCH2"]="[CX4H1](=[OX1])[CX4H2]" 
SMARTS_FUNCTIONS["-CH2NH2"]="[CX4H2][NX3H2]"
SMARTS_FUNCTIONS["-CH2SCH3"]="[CX4H2][S][CX4H3]"
SMARTS_FUNCTIONS["-CH2SOCH3"]="[CX4H2][SX3](=[OX1])[CX4H3]"
SMARTS_FUNCTIONS["-CH2CN"]="[CX4H2][C]#[N]"
SMARTS_FUNCTIONS["[-CH=CH2"]="[CX3H]=[CX3H2]"
SMARTS_FUNCTIONS["-C triple bond CH"]="[CX2]#[CX1H]"
SMARTS_FUNCTIONS["phenyl"]="[cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["[-F]"]="[FX1]"
SMARTS_FUNCTIONS["[-Cl]"]="[Cl]"
SMARTS_FUNCTIONS["[-Br]"]="[Br]"
SMARTS_FUNCTIONS["[-I]"]="[I]"
SMARTS_FUNCTIONS["-OH"]="[OX2H1]"
SMARTS_FUNCTIONS["O-"]="[O-]" #instead of ["-ONa"]
SMARTS_FUNCTIONS["-OCH3"]="[OX2][CX4H3]"
SMARTS_FUNCTIONS["-OCH=CH2"]="[OX2][CX3H1]=[CX3H2]"
SMARTS_FUNCTIONS["-Ophenyle"]="[OX2][cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-OCOCH3"]="[OX2][CX3](=[OX1])[CX4H3]"
SMARTS_FUNCTIONS["-OSi(CH3)3"]="[OX2][SiX4]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-OPO(OPhenyle)2"]="[OX2][PX4](=[OX2])([OX2][cH0]1[cH1][cH1][cH1][cH1][cH1]1)([OX2][cH0]1[cH1][cH1][cH1][cH1][cH1]1)"
SMARTS_FUNCTIONS["-OCN"]="[OX2][CX2]#[N]"
SMARTS_FUNCTIONS["-NH2"]="[NX3H2]"
SMARTS_FUNCTIONS["-NHCH3"]="[NX3H1][CX4H3]"
SMARTS_FUNCTIONS["-N(CH3)2"]="[NX3]([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-NHphenyle"]="[NX3H1][cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-N(phenyle)2"]="[NX3]([cH0]1[cH1][cH1][cH1][cH1][cH1]1)([cH0]1[cH1][cH1][cH1][cH1][cH1]1)"
SMARTS_FUNCTIONS["-NH3+"]="[N+H3]"
SMARTS_FUNCTIONS["-N+(CH3)3"]="[N+X4]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-NHCOCH3"]="[NX3H1][CX3](=[O])[CX4H3]"
SMARTS_FUNCTIONS["-NHNH2"]="[NX3H1][NX3H2]"
SMARTS_FUNCTIONS["-N=N-Phenyle"]="[NX2]=[NX2][cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-N+ triple bond N"]="[N+X2]#[N]"
SMARTS_FUNCTIONS["-NCO"]="*-N=C=O"
SMARTS_FUNCTIONS["-NCS"]="*-N=C=[SX1]"
SMARTS_FUNCTIONS["-NO"]="[NX2](=[O])"
SMARTS_FUNCTIONS["-NO2"]="*-[NX3](=O)=O"
SMARTS_FUNCTIONS["-SH"]="[SX2H1]"
SMARTS_FUNCTIONS["-SCH3"]="[SX2][CX4H3]"
SMARTS_FUNCTIONS["-SC(CH3)3"]="[SX2][CX4]([CXH3])([CXH3])([CXH3])"
SMARTS_FUNCTIONS["-SPhenyle"]="[SX2][cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-SOCH3"]="[SX2][OX2][CX4H3]"
SMARTS_FUNCTIONS["-SO2CH3"]="[SX4](=[O])(=[O])[CX4H3]"
SMARTS_FUNCTIONS["-SO2Cl"]="[SX4](=[O])(=[O])[Cl]"
SMARTS_FUNCTIONS["-SO3H"]="[SX4](=[O])(=[O])([OX2H1])"
SMARTS_FUNCTIONS["-SO2OCH3"]="[SX4](=[O])(=[O])([OX2][CX4H3])"
SMARTS_FUNCTIONS["-SCN"]="[SX2][CX2]#[N]"
SMARTS_FUNCTIONS["-COCH3"]="[CX3](=[O])[CX4H3]"
SMARTS_FUNCTIONS["-COCF3"]="[CX3](=[O])[CX4]([F])([F])([F])"
SMARTS_FUNCTIONS["-COPhenyl"]="[CX3](=[O])[cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-COOH"]="[CX3](=[OX1])[OX2H1]"
SMARTS_FUNCTIONS["-COO(-)"]="[CX3](=[OX1])([-OX1])"
SMARTS_FUNCTIONS["-COOCH3"]="[CX3](=[OX1])[OX2][CX4H3]"
SMARTS_FUNCTIONS["-CONH2"]="[CX3](=[OX1])[NX3H2]"
SMARTS_FUNCTIONS["-CON(CH3)2"]="[CX3](=[OX1])[NX3]([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-COCl"]="[CX3](=[OX1])[Cl]"
SMARTS_FUNCTIONS["-CSPhenyl"]="[CX3](=[S])[cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-CN"]="[CX3]#[N]"
SMARTS_FUNCTIONS["-P(CH3)2"]="[PX3]([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-P(Phenyle)2"]="[PX3]([cH0]1[cH1][cH1][cH1][cH1][cH1]1)([cH0]1[cH1][cH1][cH1][cH1][cH1]1)"
SMARTS_FUNCTIONS["-PO(OCH2CH3)2"]="[PX4](=[O])([OX2][CX4H2][CX4H3])([OX2][CX4H2][CX4H3])"
SMARTS_FUNCTIONS["-PS(OCH2CH3)2"]="[PX4](=[S])([OX2][CX4H2][CX4H3])([OX2][CX4H2][CX4H3])"
SMARTS_FUNCTIONS["-SiH3"]="[SiX4H3]"
SMARTS_FUNCTIONS["-Si(CH3)3"]="[SiX4]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-Sn(CH3)3"]="[SnX4]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-Pb(CH3)3"]="[PbX4]([CX4H3])([CX4H3])([CX4H3])"
SMARTS_FUNCTIONS["-C///"]="[CX4]"
SMARTS_FUNCTIONS["- ???"]="*-[CX4]1[CX4][OX2]1"
SMARTS_FUNCTIONS["-C=C-"]="[CX3]=[CX3]"
SMARTS_FUNCTIONS["-C triple bond C-"]="[CX3]#[CX3]"
SMARTS_FUNCTIONS["-O-"]="[OX2]"
SMARTS_FUNCTIONS["-O-CO-"]="[OX2][CX3](=[OX2])"
SMARTS_FUNCTIONS["-O-NO-"]="*-ON=O"
SMARTS_FUNCTIONS["-N//"]="[NX3]"
SMARTS_FUNCTIONS["-N(+)///"]="[N+X4]"
SMARTS_FUNCTIONS["-S-"]="[SX2]"
SMARTS_FUNCTIONS["-S-CO-"]="[SX2][CX3](=[O])"
SMARTS_FUNCTIONS["-SO-"]="[SX2][OX2]"
SMARTS_FUNCTIONS["-SO2-"]="[SX4](=[O])(=[O])"
SMARTS_FUNCTIONS["-CO-"]="[CX3](=[O])"
SMARTS_FUNCTIONS["-COO-"]="[CX3](=[O])[OX2]"
SMARTS_FUNCTIONS["-CON//"]="[CX3](=[O])[NX3]"
SMARTS_FUNCTIONS["-CS-N//"]="*-C[SX2][NX3]"
SMARTS_FUNCTIONS["-Sn ///"]="Sn[X4]"
SMARTS_FUNCTIONS["-CH2CH2CH3"]="[CX4H2][CX4H2][CX4H3]"
SMARTS_FUNCTIONS["-CH2I"]="[CX3H2][I]"
SMARTS_FUNCTIONS["-CH2OCH2CH3"]="[CX4H2][OX2][CX4H2][CX4H3]"
SMARTS_FUNCTIONS["-OCH2CH3"]="[OX2][CX4H2][CX4H3]"
SMARTS_FUNCTIONS["-OCH2CH2CH2CH3"]="[OX2][CX4H2][CX4H2][CX4H2][CX4H3]"
SMARTS_FUNCTIONS["-N-Pyrrolidonyl"]="*-N1C(=O)CCC1"
SMARTS_FUNCTIONS["-SCH2-Phenyle"]="[SX2][CX4H2][cH0]1[cH1][cH1][cH1][cH1][cH1]1"
SMARTS_FUNCTIONS["-SO2CH=CH2"]="[SX4](=[O])(=[O])[CX3H]=[CX3H2]"
SMARTS_FUNCTIONS["-SiCl3"]="[SiX4]([Cl])([Cl])([Cl])"






def OneBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index, mol)
  one_bond_finger_print = str()
  if atoms_one_bond_away:
    print("There are atoms one bond away:{}".format(atoms_one_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      one_bond_finger_print = one_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(one_bond_finger_print))
  else:
    one_bond_finger_print = str(999)
  print("returning one bond away finger print={}".format(one_bond_finger_print))
  one_bond_finger_print1 = str()
  one_bond_finger_print2 = str()
  one_bond_finger_print3 = str()
  one_bond_finger_print4 = str()
  one_bond_finger_print5 = str()
  one_bond_finger_print6 = str()
  one_bond_finger_print7 = str()
  one_bond_finger_print8 = str()
  for s in range(len(one_bond_finger_print)):
    one_bond_finger_print1 = one_bond_finger_print1 + one_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(one_bond_finger_print)):
    one_bond_finger_print2 = one_bond_finger_print2 + one_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(one_bond_finger_print)):
    one_bond_finger_print3 = one_bond_finger_print3 + one_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(one_bond_finger_print)):
    one_bond_finger_print4 = one_bond_finger_print4 + one_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(one_bond_finger_print)):
    one_bond_finger_print5 = one_bond_finger_print5 + one_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(one_bond_finger_print)):
    one_bond_finger_print6 = one_bond_finger_print6 + one_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(one_bond_finger_print)):
    one_bond_finger_print7 = one_bond_finger_print7 + one_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(one_bond_finger_print)):
    one_bond_finger_print8 = one_bond_finger_print8 + one_bond_finger_print[s]
    
  return one_bond_finger_print1,one_bond_finger_print2,one_bond_finger_print3,one_bond_finger_print4,one_bond_finger_print5,one_bond_finger_print6,one_bond_finger_print7,one_bond_finger_print8


def TwoBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_two_bond_away = TwoBondAwayAtoms(atom_number_with_zero_index, mol)
  two_bond_finger_print = str()
  if atoms_two_bond_away:
    print("There are atoms one bond away:{}".format(atoms_two_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_two_bond_away - (atoms_two_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      two_bond_finger_print = two_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(two_bond_finger_print))
  else:
    two_bond_finger_print = str(999)
  print("returning two bond away finger print={}".format(two_bond_finger_print))
  two_bond_finger_print1 = str()
  two_bond_finger_print2 = str()
  two_bond_finger_print3 = str()
  two_bond_finger_print4 = str()
  two_bond_finger_print5 = str()
  two_bond_finger_print6 = str()
  two_bond_finger_print7 = str()
  two_bond_finger_print8 = str()
  for s in range(len(two_bond_finger_print)):
    two_bond_finger_print1 = two_bond_finger_print1 + two_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(two_bond_finger_print)):
    two_bond_finger_print2 = two_bond_finger_print2 + two_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(two_bond_finger_print)):
    two_bond_finger_print3 = two_bond_finger_print3 + two_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(two_bond_finger_print)):
    two_bond_finger_print4 = two_bond_finger_print4 + two_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(two_bond_finger_print)):
    two_bond_finger_print5 = two_bond_finger_print5 + two_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(two_bond_finger_print)):
    two_bond_finger_print6 = two_bond_finger_print6 + two_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(two_bond_finger_print)):
    two_bond_finger_print7 = two_bond_finger_print7 + two_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(two_bond_finger_print)):
    two_bond_finger_print8 = two_bond_finger_print8 + two_bond_finger_print[s]
    
  return two_bond_finger_print1,two_bond_finger_print2,two_bond_finger_print3,two_bond_finger_print4,two_bond_finger_print5,two_bond_finger_print6,two_bond_finger_print7,two_bond_finger_print8,
  # return two_bond_finger_print


def ThreeBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_three_bond_away = ThreeBondAwayAtoms(atom_number_with_zero_index, mol)
  three_bond_finger_print = str()
  if atoms_three_bond_away:
    print("There are atoms one bond away:{}".format(atoms_three_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_three_bond_away - (atoms_three_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      three_bond_finger_print = three_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(three_bond_finger_print))
  else:
    three_bond_finger_print = str(999)
  print("returning three bond away finger print={}".format(three_bond_finger_print))
  three_bond_finger_print1 = str()
  three_bond_finger_print2 = str()
  three_bond_finger_print3 = str()
  three_bond_finger_print4 = str()
  three_bond_finger_print5 = str()
  three_bond_finger_print6 = str()
  three_bond_finger_print7 = str()
  three_bond_finger_print8 = str()
  for s in range(len(three_bond_finger_print)):
    three_bond_finger_print1 = three_bond_finger_print1 + three_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(three_bond_finger_print)):
    three_bond_finger_print2 = three_bond_finger_print2 + three_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(three_bond_finger_print)):
    three_bond_finger_print3 = three_bond_finger_print3 + three_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(three_bond_finger_print)):
    three_bond_finger_print4 = three_bond_finger_print4 + three_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(three_bond_finger_print)):
    three_bond_finger_print5 = three_bond_finger_print5 + three_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(three_bond_finger_print)):
    three_bond_finger_print6 = three_bond_finger_print6 + three_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(three_bond_finger_print)):
    three_bond_finger_print7 = three_bond_finger_print7 + three_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(three_bond_finger_print)):
    three_bond_finger_print8 = three_bond_finger_print8 + three_bond_finger_print[s]
    
  return three_bond_finger_print1,three_bond_finger_print2,three_bond_finger_print3,three_bond_finger_print4,three_bond_finger_print5,three_bond_finger_print6,three_bond_finger_print7,three_bond_finger_print8,
  # return three_bond_finger_print



# def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
#   mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
#   atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
#   four_bond_finger_print = []
#   if atoms_four_bond_away:
#     print("There are atoms one bond away:{}".format(atoms_four_bond_away))
#     for i in SMARTS_FUNCTIONS.keys():
#       temp = False
#       print("started ======={}=======".format(i))
#       match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
#       match_atoms = mol.GetSubstructMatches(match)
#       if match_atoms:
#         print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
#         for j in range(len(match_atoms)):
#           commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
#           if commonalities:
#             print("found match")
#             temp = True
#             break
#           else:
#             print("no match")
#             temp = False
#       else:
#         print("no match atom for this key")
#         temp = False
#       four_bond_finger_print.append(int(temp))
#       print("final finger print in this key:{}".format(four_bond_finger_print))
#   else:
#     four_bond_finger_print.append(999)
#   print("returning four bond away finger print={}".format(four_bond_finger_print))
#   ###### not required ####
#   # four_bond_finger_print1 = str()
#   # four_bond_finger_print2 = str()
#   # four_bond_finger_print3 = str()
#   # four_bond_finger_print4 = str()
#   # four_bond_finger_print5 = str()
#   # four_bond_finger_print6 = str()
#   # four_bond_finger_print7 = str()
#   # four_bond_finger_print8 = str()
#   # for s in range(len(four_bond_finger_print)):
#   #   four_bond_finger_print1 = four_bond_finger_print1 + four_bond_finger_print[s]
#   #   if s == 6:
#   #     break
#   # for s in range(6,len(four_bond_finger_print)):
#   #   four_bond_finger_print2 = four_bond_finger_print2 + four_bond_finger_print[s]
#   #   if s == 12:
#   #     break
#   # for s in range(12,len(four_bond_finger_print)):
#   #   four_bond_finger_print3 = four_bond_finger_print3 + four_bond_finger_print[s]
#   #   if  s==18:
#   #     break
#   # for s in range(18,len(four_bond_finger_print)):
#   #   four_bond_finger_print4 = four_bond_finger_print4 + four_bond_finger_print[s]
#   #   if  s==24:
#   #     break
#   # for s in range(24,len(four_bond_finger_print)):
#   #   four_bond_finger_print5 = four_bond_finger_print5 + four_bond_finger_print[s]
#   #   if  s==30:
#   #     break
#   # for s in range(30,len(four_bond_finger_print)):
#   #   four_bond_finger_print6 = four_bond_finger_print6 + four_bond_finger_print[s]
#   #   if  s==36:
#   #     break
#   # for s in range(36,len(four_bond_finger_print)):
#   #   four_bond_finger_print7 = four_bond_finger_print7 + four_bond_finger_print[s]
#   #   if  s==42:
#   #     break
#   # for s in range(42,len(four_bond_finger_print)):
#   #   four_bond_finger_print8 = four_bond_finger_print8 + four_bond_finger_print[s]
    
#   # return four_bond_finger_print1,four_bond_finger_print2,four_bond_finger_print3,four_bond_finger_print4,four_bond_finger_print5,four_bond_finger_print6,four_bond_finger_print7,four_bond_finger_print8,
#   #### not required #####
#   return four_bond_finger_print

def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("There are atoms one bond away:{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = temp + 1
            # break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      four_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(79)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print


def carbon_fp(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index}
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index,mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      print("match ={}".format(match))
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          if i == "C Aromatic N":
            commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          else:
            commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if commonalities:
            print("found match")
            temp = temp + 1
            break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      four_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(9)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print
  











