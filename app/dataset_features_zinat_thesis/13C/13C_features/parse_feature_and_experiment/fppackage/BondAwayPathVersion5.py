
import glob
import numpy as np
import pandas as pd
import itertools
from rdkit.Chem import AllChem as Chem
from collections import OrderedDict







# mol = Chem.MolFromMolFile("HMDB0000001.sdf", sanitize=True, removeHs=False)
# Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
# conf = mol.GetConformer(0)
# ringinfo = mol.GetRingInfo()
# ringinfo.NumAtomRings(2)


###### 1 bond away path ##
def OneBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond_atom_number = set()
  new_1bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(atom[0]).GetNeighbors())
  new_1bond_atom_number = new_1bond_atom_number.union(set(atom))
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond_atom_number))
  return new_1bond_atom_number


#### 2 bond away ###
def TwoBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond = OneBondAwayAtoms(atom[0],mol)
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond))
  new_2bond_atom_number = set()
  for s in new_1bond:
    new_2bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())

  print(new_2bond_atom_number - set(new_1bond)) # without all atom along the path
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond_atom_number))
  return new_2bond_atom_number
  return  new_2bond_atom_number - set(new_1bond)

### 3 bond away ###
def ThreeBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_2bond = TwoBondAwayAtoms(atom[0],mol)
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond))
  new_3bond_atom_number = set()
  for s in new_2bond:
    new_3bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in three bond away for atom:{} ={}".format(atom[0],new_3bond_atom_number))
  print(new_3bond_atom_number - set(new_2bond))
  return new_3bond_atom_number
  # return  new_3bond_atom_number - set(new_2bond)

### 4 bond away ###
def FourBondAwayAtoms(atom_number,mol):
  atom = [atom_number]
  new_3bond = ThreeBondAwayAtoms(atom[0],mol)
  new_4bond_atom_number = set()
  for s in new_3bond:
    new_4bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in four bond away for atom:{} ={}".format(atom[0],new_4bond_atom_number))

  print(new_4bond_atom_number - set(new_3bond))
  return new_4bond_atom_number
  # return  new_4bond_atom_number - set(new_3bond)

#### Create Finger Prints ####

## in to tal 71###
SMARTS_FUNCTIONS = OrderedDict()
# SMARTS_FUNCTIONS["aromatic atom"]="[*;a]"
# SMARTS_FUNCTIONS["aliphatic ring atom"]="[*;AR1]"
# SMARTS_FUNCTIONS["Hetero aromatic"]="[a;!c]"
# SMARTS_FUNCTIONS["Hetero aliphatic"]="[AR1;!CR1]"
SMARTS_FUNCTIONS["Benzene ring"]="[cR1]1[cR1][cR1][cR1][cR1][cR1]1"
# SMARTS_FUNCTIONS["Carbonyl"]="[CX3]=[OX1]"-------
SMARTS_FUNCTIONS["Carbonyl"]="[$([CX3]=[OX1]);!$([CX3](=[OX1])[OX2H1])]" #"[$([C][!OX2])]=[OX1]" "[]"
# SMARTS_FUNCTIONS["Carbonyl with aromatic"]="a[CX3]=[OX1]"
SMARTS_FUNCTIONS["Carbonyl with aromatic"]="a[$([CX3]=[OX1]);!$([CX3](=[OX1])[OX2H1])]"
SMARTS_FUNCTIONS["COO-"]="[CX3](=[OX1])([-OX1])"
SMARTS_FUNCTIONS["CH3 with aromatic +N"] = "[CX4H3][+nX3r6]"
SMARTS_FUNCTIONS["CHCOOH"] = "[CX4H1][CX3](=[OX1])[OX2H1]"
SMARTS_FUNCTIONS["Carboxylic acid"] = "[CX3](=[OX1])[OX2H1]" 
SMARTS_FUNCTIONS["Carboxylic acid with aromatic"] = "a[CX3](=[OX1])[OX2H1]"
SMARTS_FUNCTIONS["CHNHC=O"] = "[CX4H1][NX3H1][CX3](=[OX1])" 
SMARTS_FUNCTIONS["CH2CONH"] = "[CX4H2][CX3](=[OX1])[NX3H1]"
SMARTS_FUNCTIONS["Aldehyde"]="[CX3H1]=[OX1]"
SMARTS_FUNCTIONS["Aldehyde with aromatic"]="a[CX3H1]=[OX1]"
SMARTS_FUNCTIONS["Alkene without explicit H"]="[CX3;R0]=[CX3;R0]"
SMARTS_FUNCTIONS["Alkyne"]="[CX2]#[CX2]"
SMARTS_FUNCTIONS["Halozens"]="[F,Cl,Br,I]"
SMARTS_FUNCTIONS["Halozens with aromatic"]="a[F,Cl,Br,I]" 
SMARTS_FUNCTIONS["CH2"]="[CX4H2]"
SMARTS_FUNCTIONS["CH"]="[CX4H1]" 
SMARTS_FUNCTIONS["CH3 with N"]="[CX4H3]N" 
SMARTS_FUNCTIONS["CH3 with aromatic"]="a[CX4H3]"
SMARTS_FUNCTIONS["Hydroxyl"]="[$(C[OX2H1]);!$([CX3](=[OX1])[OX2H1])]"
SMARTS_FUNCTIONS["Hydroxyl with aromatic"]="[$(c[OX2H1]);!$([CX3](=[OX1])[OX2H1])]"
# SMARTS_FUNCTIONS["R-C(=O)CH"]="[CX3](=[OX1])[CX4H1]"
SMARTS_FUNCTIONS["NH"]="[NX3H1]"
SMARTS_FUNCTIONS["NH2"]="[NX3H2]"
SMARTS_FUNCTIONS["NH3"]="[NX3H3]"
SMARTS_FUNCTIONS["N"]="[NX3H0]"
SMARTS_FUNCTIONS["Amide, NH2C=O"]="[CX3](=[OX1])[NX3H2]"
SMARTS_FUNCTIONS["CH2NH"]="[CX4H2][NX3H1]"
SMARTS_FUNCTIONS["NHCO"]="[NX3H1][CX3]=[OX1]"
SMARTS_FUNCTIONS["NNO"]="[NX3][NX2](=[OX1])"
SMARTS_FUNCTIONS["Amino acid"]="[CX4]([NX3H2])C(=[OX1])[OX2H]"
SMARTS_FUNCTIONS["CNNN"]="[CX3](=N)(N)N"
SMARTS_FUNCTIONS["double bond O as aromatic branch"]="a=[OX1]"
SMARTS_FUNCTIONS["PO4"]="[PX4](=[OX1])(-[OX2])(-[OX2])(-[OX2])" 
SMARTS_FUNCTIONS["PO4-"]="[PX4](=[OX1])(-[-O])(-[OX2])(-[OX2])"
SMARTS_FUNCTIONS["benzene ring with N+"]="[cR1]1[+nR1][cR1][cR1][cR1][cR1]1" #N+ in benzene
SMARTS_FUNCTIONS["pyridine, benzene ring with N"]="[cR1]1[nR1][cR1][cR1][cR1][cR1]1" #N in benzene
SMARTS_FUNCTIONS["benzene ring with N =O"]="[cR1]1(=O)[nR1][cR1][cR1][cR1][cR1]1" # N in benzene with =O...doesn't work
SMARTS_FUNCTIONS["Two N in ring, aromatic"]="[cR1]1[cR1][nR1][cR1][nR1][cR1]1" #2N in benzene
SMARTS_FUNCTIONS["Two N in ring, double aromatic"]= "[cR2]1[cR1][nR1][cR1][nR1][cR2]1" #2N in double aromatic
SMARTS_FUNCTIONS["Two N in aromatic with =O and -NH2"] = "[cR1]1[cR1][nR1][cR1](=O)[nR1][cR1]1([NX3H2])" #2N in benzene with =O and -NH2
SMARTS_FUNCTIONS["Two N in aromatic with 2 =O"] = "[cR1]1(=O)[nR1][cR1][cR1][cR1](=O)[nR1]1" #2N in benzene with 2 =O 
SMARTS_FUNCTIONS["Two N in 5 ring"]="[cR1]1[cR1][nR1H0][cR1][nR1H0]1" # 2N in a 5 ring 
SMARTS_FUNCTIONS["two ring, benzene with 2N and O, 2N in 5ring"]="c12cncnc1ncn2" ###### excluding the O =>c12c(=O)ncnc1ncn2
SMARTS_FUNCTIONS["Imidazole, aromatic 5 rign with two nitrozens"]="[nH1]1[cR1][nR1][cR1][cR1]1" # 2N in 5 ring
SMARTS_FUNCTIONS["O inside 5 ring"]="[CR1]1[CR1][CR1][CR1][O]1" 
SMARTS_FUNCTIONS["O inside 5 ring with 3 =O"]="[CR1]1(=[OX1])[CR1](=[OX1])[CR1](=[OX1])[CR1][O]1" 
SMARTS_FUNCTIONS["O inside 5 ring with two OH"]="[CR1]1(C)[CR1]([OX2H1])[CR1]([OX2H1])[CR1][O]1" #O in a 5 ring with 2 OH and -C
SMARTS_FUNCTIONS["O inside 6 ring"]="[CR1]1[CR1][CR1][CR1][CR1][O]1" 
SMARTS_FUNCTIONS["O inside 6 ring with =O"]="[CR1]1(=O)[CR1][CR1][CR1][CR1][O]1" 
SMARTS_FUNCTIONS["Furan, 5 ring aromatic with O"]="[cR1]1[cR1][cR1][cR1][oR1]1"
# SMARTS_FUNCTIONS["Aliphatic atom conneted with benzene ring with N+"]="[cR1]1[+nR1][cR1][cR1][cR1][cR1]1"
SMARTS_FUNCTIONS["5 ring with one nitroen"]="[CR1]1[NR1][CR1][CR1][CR1]1" 
SMARTS_FUNCTIONS["5 ring with N+ S"]="[cR1]1[+nR1][cR1][cR1][sR1]1" 
SMARTS_FUNCTIONS["5 ring with S,5 ring with 2 N"]="C12CSCC1NC(=O)N2" 
SMARTS_FUNCTIONS["S in a ring"]="[SR1]"
SMARTS_FUNCTIONS["N in 6 ring"] = "[nX2r6]" 
SMARTS_FUNCTIONS["N in 5 ring"] = "[nX2r5]" 
# SMARTS_FUNCTIONS["sugar"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])CO1"
SMARTS_FUNCTIONS["glucose"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])CO1"
SMARTS_FUNCTIONS["ribose"] = "C1([CX4H2][OX2H1])C([OH])C([OH])C([OH])O1"
SMARTS_FUNCTIONS["sugar1"] = "O1[CH]([OH])CCCC1"
SMARTS_FUNCTIONS["sugar2"] = "O1[CH]([OH])CCC1"


# SMARTS_FUNCTIONS["N+ in a ring"]="[+NR1]"
# SMARTS_FUNCTIONS["N+ in a aromatic"]="[+NR1]"
# SMARTS_FUNCTIONS["N in a aromatic"]="[nR1]"
# SMARTS_FUNCTIONS["N in a ring"]="[NR1]"
# SMARTS_FUNCTIONS["glucoes"] = "C1([OX2H1])([H])CCCCO1"
# SMARTS_FUNCTIONS["glucoes"] = "C1CCCC([OX2H1])([H])O1"
SMARTS_FUNCTIONS["6 ring with one double bond"]="[CR1]1=[CR1][CR1][CR1][CR1][CR1]1" 
SMARTS_FUNCTIONS["6 ring without any double bond"]="[CR1]1[CR1][CR1][CR1][CR1][CR1]1" 
SMARTS_FUNCTIONS["6 ring without any double bond with O"]="[CR1]1(=O)[CR1][CR1][CR1][CR1][CR1]1" 
SMARTS_FUNCTIONS["infused benzene ring, one ring with nitrozen"]="c12cccnc1cccc2" 
SMARTS_FUNCTIONS["infused benzene ring, one ring with nitrozen and O"]="c12c(=O)ccnc1cccc2"
SMARTS_FUNCTIONS["infused benzene ring"]="c12ccccc1cccc2" 
# SMARTS_FUNCTIONS["infused benzene ring, 5 ring with 2N, 6 ring with 2N"]= "c12cncnc1ncn2" 
# SMARTS_FUNCTIONS["infused benzene ring, 5 ring with 2N, 6 ring with 2N O1"]= "c12c(=O)ncnc1ncn2" 
SMARTS_FUNCTIONS["Nitrate Ester 1"]="[+NX3](=[OX1])[-O]" 
# SMARTS_FUNCTIONS["benzene Nitrate Ester 1"]="[+NX3](=[OX1])[-O]" 
# SMARTS_FUNCTIONS["Nitrate Ester"]="[+NX3](=[+OX1])[-O]"
SMARTS_FUNCTIONS["oxygen with C and P"]="C[OX2]P" 
SMARTS_FUNCTIONS["Ester"]="C(=[OX1])[OX2;H0]" 
SMARTS_FUNCTIONS["HMDB640"]="[CX4][O][CX4]" 
SMARTS_FUNCTIONS["O=S=O"]="[SX4](=[OX1])=[OX1]" 
SMARTS_FUNCTIONS["6 ring with two O"]="[CR1]1(=[OX1])[CR1]=[CR1][CR1](=[OX1])[CR1]=[CR1]1" 
SMARTS_FUNCTIONS["C-S-C"] = "C[SX2;R0]C"
SMARTS_FUNCTIONS["CH2NH2"] = "[CH2][NX3H2]"
# SMARTS_FUNCTIONS["CHO"] = "[CH1][OX2H1]"
SMARTS_FUNCTIONS["SCH3"] = "S[CX4H3]"
SMARTS_FUNCTIONS["CH3 with N+"] = "[+N][CX4H3]"
SMARTS_FUNCTIONS["methane"]="[CX4H3]"
SMARTS_FUNCTIONS["infused benzene ring, both with 2N"]= "c12cncnc1nccn2"
SMARTS_FUNCTIONS["connected with aromatic atom"]="[aR1]"
SMARTS_FUNCTIONS["aliphatic ring atom"]="[AR1]"
SMARTS_FUNCTIONS["n and s in a 5 ring"]="c1scnc1"
SMARTS_FUNCTIONS["-C=N-OH"]="[CX3]=[NX2]([OX2H1])"
SMARTS_FUNCTIONS["SO3"]="[SX4](=[OX1])(=[OX1])(-[OX2])"
SMARTS_FUNCTIONS["Two N in 5 ring with =O"]="[cR1]1[cR1][nR1][nR1][cR1]1(=[OX1])"
SMARTS_FUNCTIONS["N and O in a 6 ring"]="[CR1]1[CR1][NR1][CR1][CR1][OR1]1"
SMARTS_FUNCTIONS["PO3"]="[$([PX4](=[OX1])(-[OX2])(-[OX2]));!$([PX4](=[OX1])(-[OX2])(-[OX2])(-[OX2]))]"
SMARTS_FUNCTIONS["Two N and S in 5 ring with =O"]="[cR1]1[nR1][nR1][cR1][sR1]1"
SMARTS_FUNCTIONS["C triple bond N"]="C#N"




def OneBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index, mol)
  one_bond_finger_print = str()
  if atoms_one_bond_away:
    print("There are atoms one bond away:{}".format(atoms_one_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      one_bond_finger_print = one_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(one_bond_finger_print))
  else:
    one_bond_finger_print = str(999)
  print("returning one bond away finger print={}".format(one_bond_finger_print))
  one_bond_finger_print1 = str()
  one_bond_finger_print2 = str()
  one_bond_finger_print3 = str()
  one_bond_finger_print4 = str()
  one_bond_finger_print5 = str()
  one_bond_finger_print6 = str()
  one_bond_finger_print7 = str()
  one_bond_finger_print8 = str()
  for s in range(len(one_bond_finger_print)):
    one_bond_finger_print1 = one_bond_finger_print1 + one_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(one_bond_finger_print)):
    one_bond_finger_print2 = one_bond_finger_print2 + one_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(one_bond_finger_print)):
    one_bond_finger_print3 = one_bond_finger_print3 + one_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(one_bond_finger_print)):
    one_bond_finger_print4 = one_bond_finger_print4 + one_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(one_bond_finger_print)):
    one_bond_finger_print5 = one_bond_finger_print5 + one_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(one_bond_finger_print)):
    one_bond_finger_print6 = one_bond_finger_print6 + one_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(one_bond_finger_print)):
    one_bond_finger_print7 = one_bond_finger_print7 + one_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(one_bond_finger_print)):
    one_bond_finger_print8 = one_bond_finger_print8 + one_bond_finger_print[s]
    
  return one_bond_finger_print1,one_bond_finger_print2,one_bond_finger_print3,one_bond_finger_print4,one_bond_finger_print5,one_bond_finger_print6,one_bond_finger_print7,one_bond_finger_print8


def TwoBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_two_bond_away = TwoBondAwayAtoms(atom_number_with_zero_index, mol)
  two_bond_finger_print = str()
  if atoms_two_bond_away:
    print("There are atoms one bond away:{}".format(atoms_two_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_two_bond_away - (atoms_two_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      two_bond_finger_print = two_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(two_bond_finger_print))
  else:
    two_bond_finger_print = str(999)
  print("returning two bond away finger print={}".format(two_bond_finger_print))
  two_bond_finger_print1 = str()
  two_bond_finger_print2 = str()
  two_bond_finger_print3 = str()
  two_bond_finger_print4 = str()
  two_bond_finger_print5 = str()
  two_bond_finger_print6 = str()
  two_bond_finger_print7 = str()
  two_bond_finger_print8 = str()
  for s in range(len(two_bond_finger_print)):
    two_bond_finger_print1 = two_bond_finger_print1 + two_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(two_bond_finger_print)):
    two_bond_finger_print2 = two_bond_finger_print2 + two_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(two_bond_finger_print)):
    two_bond_finger_print3 = two_bond_finger_print3 + two_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(two_bond_finger_print)):
    two_bond_finger_print4 = two_bond_finger_print4 + two_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(two_bond_finger_print)):
    two_bond_finger_print5 = two_bond_finger_print5 + two_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(two_bond_finger_print)):
    two_bond_finger_print6 = two_bond_finger_print6 + two_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(two_bond_finger_print)):
    two_bond_finger_print7 = two_bond_finger_print7 + two_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(two_bond_finger_print)):
    two_bond_finger_print8 = two_bond_finger_print8 + two_bond_finger_print[s]
    
  return two_bond_finger_print1,two_bond_finger_print2,two_bond_finger_print3,two_bond_finger_print4,two_bond_finger_print5,two_bond_finger_print6,two_bond_finger_print7,two_bond_finger_print8,
  # return two_bond_finger_print


def ThreeBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_three_bond_away = ThreeBondAwayAtoms(atom_number_with_zero_index, mol)
  three_bond_finger_print = str()
  if atoms_three_bond_away:
    print("There are atoms one bond away:{}".format(atoms_three_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_three_bond_away - (atoms_three_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      three_bond_finger_print = three_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(three_bond_finger_print))
  else:
    three_bond_finger_print = str(999)
  print("returning three bond away finger print={}".format(three_bond_finger_print))
  three_bond_finger_print1 = str()
  three_bond_finger_print2 = str()
  three_bond_finger_print3 = str()
  three_bond_finger_print4 = str()
  three_bond_finger_print5 = str()
  three_bond_finger_print6 = str()
  three_bond_finger_print7 = str()
  three_bond_finger_print8 = str()
  for s in range(len(three_bond_finger_print)):
    three_bond_finger_print1 = three_bond_finger_print1 + three_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(three_bond_finger_print)):
    three_bond_finger_print2 = three_bond_finger_print2 + three_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(three_bond_finger_print)):
    three_bond_finger_print3 = three_bond_finger_print3 + three_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(three_bond_finger_print)):
    three_bond_finger_print4 = three_bond_finger_print4 + three_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(three_bond_finger_print)):
    three_bond_finger_print5 = three_bond_finger_print5 + three_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(three_bond_finger_print)):
    three_bond_finger_print6 = three_bond_finger_print6 + three_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(three_bond_finger_print)):
    three_bond_finger_print7 = three_bond_finger_print7 + three_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(three_bond_finger_print)):
    three_bond_finger_print8 = three_bond_finger_print8 + three_bond_finger_print[s]
    
  return three_bond_finger_print1,three_bond_finger_print2,three_bond_finger_print3,three_bond_finger_print4,three_bond_finger_print5,three_bond_finger_print6,three_bond_finger_print7,three_bond_finger_print8,
  # return three_bond_finger_print



# def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
#   mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
#   atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
#   four_bond_finger_print = []
#   if atoms_four_bond_away:
#     print("There are atoms one bond away:{}".format(atoms_four_bond_away))
#     for i in SMARTS_FUNCTIONS.keys():
#       temp = False
#       print("started ======={}=======".format(i))
#       match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
#       match_atoms = mol.GetSubstructMatches(match)
#       if match_atoms:
#         print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
#         for j in range(len(match_atoms)):
#           commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
#           if commonalities:
#             print("found match")
#             temp = True
#             break
#           else:
#             print("no match")
#             temp = False
#       else:
#         print("no match atom for this key")
#         temp = False
#       four_bond_finger_print.append(int(temp))
#       print("final finger print in this key:{}".format(four_bond_finger_print))
#   else:
#     four_bond_finger_print.append(999)
#   print("returning four bond away finger print={}".format(four_bond_finger_print))
#   ###### not required ####
#   # four_bond_finger_print1 = str()
#   # four_bond_finger_print2 = str()
#   # four_bond_finger_print3 = str()
#   # four_bond_finger_print4 = str()
#   # four_bond_finger_print5 = str()
#   # four_bond_finger_print6 = str()
#   # four_bond_finger_print7 = str()
#   # four_bond_finger_print8 = str()
#   # for s in range(len(four_bond_finger_print)):
#   #   four_bond_finger_print1 = four_bond_finger_print1 + four_bond_finger_print[s]
#   #   if s == 6:
#   #     break
#   # for s in range(6,len(four_bond_finger_print)):
#   #   four_bond_finger_print2 = four_bond_finger_print2 + four_bond_finger_print[s]
#   #   if s == 12:
#   #     break
#   # for s in range(12,len(four_bond_finger_print)):
#   #   four_bond_finger_print3 = four_bond_finger_print3 + four_bond_finger_print[s]
#   #   if  s==18:
#   #     break
#   # for s in range(18,len(four_bond_finger_print)):
#   #   four_bond_finger_print4 = four_bond_finger_print4 + four_bond_finger_print[s]
#   #   if  s==24:
#   #     break
#   # for s in range(24,len(four_bond_finger_print)):
#   #   four_bond_finger_print5 = four_bond_finger_print5 + four_bond_finger_print[s]
#   #   if  s==30:
#   #     break
#   # for s in range(30,len(four_bond_finger_print)):
#   #   four_bond_finger_print6 = four_bond_finger_print6 + four_bond_finger_print[s]
#   #   if  s==36:
#   #     break
#   # for s in range(36,len(four_bond_finger_print)):
#   #   four_bond_finger_print7 = four_bond_finger_print7 + four_bond_finger_print[s]
#   #   if  s==42:
#   #     break
#   # for s in range(42,len(four_bond_finger_print)):
#   #   four_bond_finger_print8 = four_bond_finger_print8 + four_bond_finger_print[s]
    
#   # return four_bond_finger_print1,four_bond_finger_print2,four_bond_finger_print3,four_bond_finger_print4,four_bond_finger_print5,four_bond_finger_print6,four_bond_finger_print7,four_bond_finger_print8,
#   #### not required #####
#   return four_bond_finger_print

def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("There are atoms one bond away:{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = temp + 1
            # break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      four_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(79)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print
  











