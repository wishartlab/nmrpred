
import glob
import numpy as np
import pandas as pd
import itertools
from rdkit.Chem import AllChem as Chem
from collections import OrderedDict







# mol = Chem.MolFromMolFile("HMDB0000001.sdf", sanitize=True, removeHs=False)
# Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
# conf = mol.GetConformer(0)
# ringinfo = mol.GetRingInfo()
# ringinfo.NumAtomRings(2)


###### 1 bond away path ##
def OneBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond_atom_number = set()
  new_1bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(atom[0]).GetNeighbors())
  new_1bond_atom_number = new_1bond_atom_number.union(set(atom))
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond_atom_number))
  return new_1bond_atom_number


#### 2 bond away ###
def TwoBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_1bond = OneBondAwayAtoms(atom[0],mol)
  print("atoms in one bond away for atom:{} ={}".format(atom[0],new_1bond))
  new_2bond_atom_number = set()
  for s in new_1bond:
    new_2bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())

  print(new_2bond_atom_number - set(new_1bond)) # without all atom along the path
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond_atom_number))
  return new_2bond_atom_number
  return  new_2bond_atom_number - set(new_1bond)

### 3 bond away ###
def ThreeBondAwayAtoms(atom_number,mol): #it will take atom number, where numbering starts with zero
  atom = [atom_number]
  new_2bond = TwoBondAwayAtoms(atom[0],mol)
  print("atoms in two bond away for atom:{} ={}".format(atom[0],new_2bond))
  new_3bond_atom_number = set()
  for s in new_2bond:
    new_3bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in three bond away for atom:{} ={}".format(atom[0],new_3bond_atom_number))
  print(new_3bond_atom_number - set(new_2bond))
  return new_3bond_atom_number
  # return  new_3bond_atom_number - set(new_2bond)

### 4 bond away ###
def FourBondAwayAtoms(atom_number,mol):
  atom = [atom_number]
  new_3bond = ThreeBondAwayAtoms(atom[0],mol)
  new_4bond_atom_number = set()
  for s in new_3bond:
    new_4bond_atom_number.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
  print("atoms in four bond away for atom:{} ={}".format(atom[0],new_4bond_atom_number))

  print(new_4bond_atom_number - set(new_3bond))
  return new_4bond_atom_number
  # return  new_4bond_atom_number - set(new_3bond)

#### Create Finger Prints ####

## in to tal 71###
SMARTS_FUNCTIONS = OrderedDict()
# SMARTS_FUNCTIONS["aromatic atom"]="[*;a]"
# SMARTS_FUNCTIONS["aliphatic ring atom"]="[*;AR1]"
# SMARTS_FUNCTIONS["Hetero aromatic"]="[a;!c]"
# SMARTS_FUNCTIONS["Hetero aliphatic"]="[AR1;!CR1]"
SMARTS_FUNCTIONS["Carbonyl"]="[$([CX3]=[OX1]);!$([CX3](=[OX1])[OX2H1])]" #"[$([C][!OX2])]=[OX1]" "[]"
SMARTS_FUNCTIONS["CH3"]="[CX4H3]"
SMARTS_FUNCTIONS["CH2"]="[CX4H2]"
SMARTS_FUNCTIONS["CH"]="[CX4H1]"
SMARTS_FUNCTIONS["Aldehyde"]="[CX3H1]=[OX1]"
SMARTS_FUNCTIONS["C double bond N"]="C=N"
SMARTS_FUNCTIONS["C single bond N"]="C-N" 
SMARTS_FUNCTIONS["aromatic CH"]="[cH1]"
SMARTS_FUNCTIONS["C Aromatic N"]="[n]"



def OneBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index, mol)
  one_bond_finger_print = str()
  if atoms_one_bond_away:
    print("There are atoms one bond away:{}".format(atoms_one_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      one_bond_finger_print = one_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(one_bond_finger_print))
  else:
    one_bond_finger_print = str(999)
  print("returning one bond away finger print={}".format(one_bond_finger_print))
  one_bond_finger_print1 = str()
  one_bond_finger_print2 = str()
  one_bond_finger_print3 = str()
  one_bond_finger_print4 = str()
  one_bond_finger_print5 = str()
  one_bond_finger_print6 = str()
  one_bond_finger_print7 = str()
  one_bond_finger_print8 = str()
  for s in range(len(one_bond_finger_print)):
    one_bond_finger_print1 = one_bond_finger_print1 + one_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(one_bond_finger_print)):
    one_bond_finger_print2 = one_bond_finger_print2 + one_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(one_bond_finger_print)):
    one_bond_finger_print3 = one_bond_finger_print3 + one_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(one_bond_finger_print)):
    one_bond_finger_print4 = one_bond_finger_print4 + one_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(one_bond_finger_print)):
    one_bond_finger_print5 = one_bond_finger_print5 + one_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(one_bond_finger_print)):
    one_bond_finger_print6 = one_bond_finger_print6 + one_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(one_bond_finger_print)):
    one_bond_finger_print7 = one_bond_finger_print7 + one_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(one_bond_finger_print)):
    one_bond_finger_print8 = one_bond_finger_print8 + one_bond_finger_print[s]
    
  return one_bond_finger_print1,one_bond_finger_print2,one_bond_finger_print3,one_bond_finger_print4,one_bond_finger_print5,one_bond_finger_print6,one_bond_finger_print7,one_bond_finger_print8


def TwoBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_two_bond_away = TwoBondAwayAtoms(atom_number_with_zero_index, mol)
  two_bond_finger_print = str()
  if atoms_two_bond_away:
    print("There are atoms one bond away:{}".format(atoms_two_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_two_bond_away - (atoms_two_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      two_bond_finger_print = two_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(two_bond_finger_print))
  else:
    two_bond_finger_print = str(999)
  print("returning two bond away finger print={}".format(two_bond_finger_print))
  two_bond_finger_print1 = str()
  two_bond_finger_print2 = str()
  two_bond_finger_print3 = str()
  two_bond_finger_print4 = str()
  two_bond_finger_print5 = str()
  two_bond_finger_print6 = str()
  two_bond_finger_print7 = str()
  two_bond_finger_print8 = str()
  for s in range(len(two_bond_finger_print)):
    two_bond_finger_print1 = two_bond_finger_print1 + two_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(two_bond_finger_print)):
    two_bond_finger_print2 = two_bond_finger_print2 + two_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(two_bond_finger_print)):
    two_bond_finger_print3 = two_bond_finger_print3 + two_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(two_bond_finger_print)):
    two_bond_finger_print4 = two_bond_finger_print4 + two_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(two_bond_finger_print)):
    two_bond_finger_print5 = two_bond_finger_print5 + two_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(two_bond_finger_print)):
    two_bond_finger_print6 = two_bond_finger_print6 + two_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(two_bond_finger_print)):
    two_bond_finger_print7 = two_bond_finger_print7 + two_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(two_bond_finger_print)):
    two_bond_finger_print8 = two_bond_finger_print8 + two_bond_finger_print[s]
    
  return two_bond_finger_print1,two_bond_finger_print2,two_bond_finger_print3,two_bond_finger_print4,two_bond_finger_print5,two_bond_finger_print6,two_bond_finger_print7,two_bond_finger_print8,
  # return two_bond_finger_print


def ThreeBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_three_bond_away = ThreeBondAwayAtoms(atom_number_with_zero_index, mol)
  three_bond_finger_print = str()
  if atoms_three_bond_away:
    print("There are atoms one bond away:{}".format(atoms_three_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = False
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_three_bond_away - (atoms_three_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = True
            break
          else:
            print("no match")
            temp = False
      else:
        print("no match atom for this key")
        temp = False
      three_bond_finger_print = three_bond_finger_print + str(int(temp))
      print("final finger print in this key:{}".format(three_bond_finger_print))
  else:
    three_bond_finger_print = str(999)
  print("returning three bond away finger print={}".format(three_bond_finger_print))
  three_bond_finger_print1 = str()
  three_bond_finger_print2 = str()
  three_bond_finger_print3 = str()
  three_bond_finger_print4 = str()
  three_bond_finger_print5 = str()
  three_bond_finger_print6 = str()
  three_bond_finger_print7 = str()
  three_bond_finger_print8 = str()
  for s in range(len(three_bond_finger_print)):
    three_bond_finger_print1 = three_bond_finger_print1 + three_bond_finger_print[s]
    if s == 6:
      break
  for s in range(6,len(three_bond_finger_print)):
    three_bond_finger_print2 = three_bond_finger_print2 + three_bond_finger_print[s]
    if s == 12:
      break
  for s in range(12,len(three_bond_finger_print)):
    three_bond_finger_print3 = three_bond_finger_print3 + three_bond_finger_print[s]
    if  s==18:
      break
  for s in range(18,len(three_bond_finger_print)):
    three_bond_finger_print4 = three_bond_finger_print4 + three_bond_finger_print[s]
    if  s==24:
      break
  for s in range(24,len(three_bond_finger_print)):
    three_bond_finger_print5 = three_bond_finger_print5 + three_bond_finger_print[s]
    if  s==30:
      break
  for s in range(30,len(three_bond_finger_print)):
    three_bond_finger_print6 = three_bond_finger_print6 + three_bond_finger_print[s]
    if  s==36:
      break
  for s in range(36,len(three_bond_finger_print)):
    three_bond_finger_print7 = three_bond_finger_print7 + three_bond_finger_print[s]
    if  s==42:
      break
  for s in range(42,len(three_bond_finger_print)):
    three_bond_finger_print8 = three_bond_finger_print8 + three_bond_finger_print[s]
    
  return three_bond_finger_print1,three_bond_finger_print2,three_bond_finger_print3,three_bond_finger_print4,three_bond_finger_print5,three_bond_finger_print6,three_bond_finger_print7,three_bond_finger_print8,
  # return three_bond_finger_print



# def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
#   mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
#   atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
#   four_bond_finger_print = []
#   if atoms_four_bond_away:
#     print("There are atoms one bond away:{}".format(atoms_four_bond_away))
#     for i in SMARTS_FUNCTIONS.keys():
#       temp = False
#       print("started ======={}=======".format(i))
#       match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
#       match_atoms = mol.GetSubstructMatches(match)
#       if match_atoms:
#         print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
#         for j in range(len(match_atoms)):
#           commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
#           if commonalities:
#             print("found match")
#             temp = True
#             break
#           else:
#             print("no match")
#             temp = False
#       else:
#         print("no match atom for this key")
#         temp = False
#       four_bond_finger_print.append(int(temp))
#       print("final finger print in this key:{}".format(four_bond_finger_print))
#   else:
#     four_bond_finger_print.append(999)
#   print("returning four bond away finger print={}".format(four_bond_finger_print))
#   ###### not required ####
#   # four_bond_finger_print1 = str()
#   # four_bond_finger_print2 = str()
#   # four_bond_finger_print3 = str()
#   # four_bond_finger_print4 = str()
#   # four_bond_finger_print5 = str()
#   # four_bond_finger_print6 = str()
#   # four_bond_finger_print7 = str()
#   # four_bond_finger_print8 = str()
#   # for s in range(len(four_bond_finger_print)):
#   #   four_bond_finger_print1 = four_bond_finger_print1 + four_bond_finger_print[s]
#   #   if s == 6:
#   #     break
#   # for s in range(6,len(four_bond_finger_print)):
#   #   four_bond_finger_print2 = four_bond_finger_print2 + four_bond_finger_print[s]
#   #   if s == 12:
#   #     break
#   # for s in range(12,len(four_bond_finger_print)):
#   #   four_bond_finger_print3 = four_bond_finger_print3 + four_bond_finger_print[s]
#   #   if  s==18:
#   #     break
#   # for s in range(18,len(four_bond_finger_print)):
#   #   four_bond_finger_print4 = four_bond_finger_print4 + four_bond_finger_print[s]
#   #   if  s==24:
#   #     break
#   # for s in range(24,len(four_bond_finger_print)):
#   #   four_bond_finger_print5 = four_bond_finger_print5 + four_bond_finger_print[s]
#   #   if  s==30:
#   #     break
#   # for s in range(30,len(four_bond_finger_print)):
#   #   four_bond_finger_print6 = four_bond_finger_print6 + four_bond_finger_print[s]
#   #   if  s==36:
#   #     break
#   # for s in range(36,len(four_bond_finger_print)):
#   #   four_bond_finger_print7 = four_bond_finger_print7 + four_bond_finger_print[s]
#   #   if  s==42:
#   #     break
#   # for s in range(42,len(four_bond_finger_print)):
#   #   four_bond_finger_print8 = four_bond_finger_print8 + four_bond_finger_print[s]
    
#   # return four_bond_finger_print1,four_bond_finger_print2,four_bond_finger_print3,four_bond_finger_print4,four_bond_finger_print5,four_bond_finger_print6,four_bond_finger_print7,four_bond_finger_print8,
#   #### not required #####
#   return four_bond_finger_print

def FourBondAwayFP(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = FourBondAwayAtoms(atom_number_with_zero_index, mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("There are atoms one bond away:{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
          if commonalities:
            print("found match")
            temp = temp + 1
            # break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      four_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(79)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print


def carbon_fp(atom_number_with_zero_index,sdf_file):
  mol = Chem.MolFromMolFile(sdf_file,sanitize=True, removeHs=False)
  atoms_four_bond_away = {atom_number_with_zero_index}
  atoms_one_bond_away = OneBondAwayAtoms(atom_number_with_zero_index,mol)
  four_bond_finger_print = []
  if atoms_four_bond_away:
    print("The atom :{}".format(atoms_four_bond_away))
    for i in SMARTS_FUNCTIONS.keys():
      temp = 0
      print("started ======={}=======".format(i))
      match = Chem.MolFromSmarts(SMARTS_FUNCTIONS[i])
      match_atoms = mol.GetSubstructMatches(match)
      if match_atoms:
        print("There are matches{} for group:{}".format(match_atoms,SMARTS_FUNCTIONS[i]))
        for j in range(len(match_atoms)):
          if i == "C Aromatic N":
            commonalities = atoms_one_bond_away - (atoms_one_bond_away - set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          else:
            commonalities = atoms_four_bond_away - (atoms_four_bond_away - set(match_atoms[j]))
            print("There are commonalities{} for group:{}".format(commonalities,SMARTS_FUNCTIONS[i]))
          if commonalities:
            print("found match")
            temp = temp + 1
            break
          else:
            print("no match")
            temp = temp + 0
      else:
        print("no match atom for this key")
        temp = temp + 0
      four_bond_finger_print.append(int(temp))
      print("final finger print in this key:{}".format(four_bond_finger_print))
  else:
    four_bond_finger_print = [999.0 for i in range(9)]
  print("returning four bond away finger print={}".format(four_bond_finger_print))
  print("returning four bond away finger print len={}".format(len(four_bond_finger_print)))
  return four_bond_finger_print
  











