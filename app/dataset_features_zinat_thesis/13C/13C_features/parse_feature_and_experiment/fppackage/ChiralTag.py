import rdkit
from rdkit.Chem import AllChem as Chem
import numpy as np
import pandas as pd
from rdkit.Chem import rdMolTransforms
import csv,os







def chiraltag(molnamewpath):
  mol = Chem.MolFromMolFile(molnamewpath,removeHs=False)
  mol = Chem.AddHs(mol)
  Chem.EmbedMolecule(mol)
  Chem.AssignStereochemistry(mol)
  Chem.MMFFOptimizeMolecule(mol)
  Chem.AssignAtomChiralTagsFromStructure(mol)
  tag = Chem.FindMolChiralCenters(mol)
  chiral_tag = None
  if tag:
    chiral_tag = 1
    print("is chiral")
  else:
    chiral_tag = 0
    print("not chiral")
  return chiral_tag

def chiraltag_previous(atom_with_nearest_3_atoms_array,molnamewpath):
  mol = Chem.MolFromMolFile(molnamewpath, sanitize=True, removeHs=False)
  Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
  chiral_tags = [int(bool(mol.GetAtomWithIdx(x).GetChiralTag())) for x in atom_with_nearest_3_atoms_array]
  print("chiral tags = {}".format(chiral_tags))
  return chiral_tags


def aromaticAtom(atom_with_nearest_3_atoms_array,molnamewpath):
  mol = Chem.MolFromMolFile(molnamewpath, sanitize=True, removeHs=False)
  # Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
  aromacity = [int(bool(mol.GetAtomWithIdx(int(x)).GetIsAromatic())) for x in atom_with_nearest_3_atoms_array]
  print("aromacity = {}".format(aromacity))
  return aromacity




# def is_prochiral_H(molnamewpath,idx):
#     #does changing H to D make the molecule or attatched atom R or S?
#     #use He instead of D, rdkit problem?
#     mol = Chem.MolFromMolFile(molnamewpath,removeHs=False)

#     #stereochemistry must be specified if using SMILES
#     #mol = Chem.MolFromSmiles('CCC') #not chiral
#     #mol = Chem.MolFromSmiles('CCO') #enantiomeric CH2
#     #mol = Chem.MolFromSmiles('CCO[C@H](O)C') #diasterotopic CH2
#     #mol = Chem.MolFromSmiles('CCO[C@H](C)OCC') #diasterotopic CH2
#     #mol = Chem.MolFromSmiles('O=C(O)C[C@H](C)CC(O)=O') 
   
#     mol = Chem.AddHs(mol)
#     Chem.EmbedMolecule(mol)
#     Chem.MMFFOptimizeMolecule(mol)
#     # Chem.MolToMolFile(mol,'/Users/zinatsayeeda/Documents/CH2/'+single_file+'.sdf')

#     #sometimes needed
#     Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
#     mol2 = Chem.Mol(mol)
#     a = mol2.GetAtomWithIdx(idx)
#     neigh = a.GetNeighbors()[0].GetIdx()

#     #only for H
#     if a.GetSymbol() != "H": return 0
#     #only for SP3
#     if mol2.GetAtomWithIdx(neigh).GetDegree() != 4: return 0

#     c0 = set(Chem.FindMolChiralCenters(mol2))

#     a.SetAtomicNum(2)
#     #a.SetIsotope(2)
#     Chem.AssignAtomChiralTagsFromStructure(mol2,replaceExistingTags=True)
    
#     c1 = set(Chem.FindMolChiralCenters(mol2))

#     diff = c1.difference(c0)
#     #print(c0,c1,diff,len(diff))
    
#     #not prochiral, homotopic 
#     if len(diff) == 0: return 0
    
#     #prochiral
#     if len(diff) >= 1:
        
#         #enantiotopic  (doesnt affect chemical shift)
#         if len(c1) == 1:
#             pass
#             #for itm in diff:
#             #    if itm[1] == "R": return 1 #'pro-R'
#             #    elif itm[1] == "S": return 2 #'pro-S'
            
#         #diastereotopic (can affect chemical shift)
#         if len(c1) > 1:
#             if len(diff) == 1:
#                 for itm in diff:
#                     if itm[1] == "R": return 1 #'pro-R'
#                     elif itm[1] == "S": return 2 #'pro-S'
#             elif len(diff) > 1:
#                 if (neigh,"R") in diff: return 1 #'pro-R'
#                 elif (neigh,"S") in diff: return 2 #'pro-S'
                
#     return 0
def is_prochiral_H(molnamewpath,idx):
    #does changing H to D make the molecule or attatched atom R or S?
    #use He instead of D, rdkit problem?

    mol = Chem.MolFromMolFile(molnamewpath,removeHs=False)
    mol = Chem.AddHs(mol)
    Chem.EmbedMolecule(mol)
    Chem.MMFFOptimizeMolecule(mol)
    Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)

    mol2 = Chem.Mol(mol)
    a = mol2.GetAtomWithIdx(idx)
    neigh = a.GetNeighbors()[0].GetIdx()

    #only for H
    if a.GetSymbol() != "H": return 0
    #only for SP3
    if mol2.GetAtomWithIdx(neigh).GetDegree() != 4: return 0

    c0 = set(Chem.FindMolChiralCenters(mol2))

    a.SetAtomicNum(2)
    #a.SetIsotope(2)
    Chem.AssignAtomChiralTagsFromStructure(mol2,replaceExistingTags=True)
    
    c1 = set(Chem.FindMolChiralCenters(mol2))

    diff = c1.difference(c0)
    #print(c0,c1,diff,len(diff))
    
    #not prochiral, homotopic 
    if len(diff) == 0: return 0
    
    #prochiral
    if len(diff) >= 1:
        
        #enantiotopic  (doesnt affect chemical shift)
        if len(c1) == 1:
            #pass
            for itm in diff:
                if itm[1] == "R": return 3 #'pro-R'
                elif itm[1] == "S": return 4 #'pro-S'
            
        #diastereotopic (can affect chemical shift)
        if len(c1) > 1:
            if len(diff) == 1:
                for itm in diff:
                    if itm[1] == "R": return 1 #'pro-R'
                    elif itm[1] == "S": return 2 #'pro-S'
            elif len(diff) > 1:
                if (neigh,"R") in diff: return 1 #'pro-R'
                elif (neigh,"S") in diff: return 2 #'pro-S'
                
    return 0


def dist_from_chiral(molnamewpath,idx):
    mol = Chem.MolFromMolFile(molnamewpath,removeHs=False)
    Chem.EmbedMolecule(mol)
    Chem.MMFFOptimizeMolecule(mol)
    Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
    chiral = Chem.FindMolChiralCenters(mol)
    if len(chiral) == 0: return 0
    path = [len(Chem.GetShortestPath(mol,idx,chi[0])) for chi in chiral]
    print("path is :{}".format(path))
    return min(path)