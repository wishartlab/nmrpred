#import numpy as np
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import rdMolTransforms
import pandas as pd
import csv, math




def aromaticAtom(atom,molnamewpath):

    mol = Chem.MolFromMolFile(molnamewpath, sanitize=True, removeHs=False)
    # Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
    aromacity = int(bool(mol.GetAtomWithIdx(int(atom)).GetIsAromatic()))
    print("aromacity = {}".format(aromacity))
    return aromacity

def conjugate_bond(atom,molnamewpath):

    mol = Chem.MolFromMolFile(molnamewpath, sanitize=True, removeHs=False)
    # Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
    conjugate = int( bool(any((b.GetIsAromatic() for b in mol.GetAtomWithIdx(int(atom)).GetBonds()))))
    print("conjugate = {}".format(conjugate))
    return conjugate

def explicit_H(atom,molnamewpath):

    mol = Chem.MolFromMolFile(molnamewpath, sanitize=True, removeHs=False)
    # Chem.ComputeGasteigerCharges(mol,throwOnParamFailure=True)
    explicit_h =  mol.GetAtomWithIdx(int(atom)).GetTotalNumHs(includeNeighbors=True)
    print("explicit_h = {}".format(explicit_h))
    return explicit_h









def is_mol_chiral(sdf_file_with_path):
    mol = Chem.MolFromMolFile(sdf_file_with_path,removeHs=False)
    Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
    if len(Chem.FindMolChiralCenters(mol)): return 1
    else: return 0


##def is_chiral(mol,idx):
##    #is the atom, or the atom H is attatched to, chiral?
##    
##    a = mol.GetAtomWithIdx(idx)
##    neigh = a.GetNeighbors()[0].GetIdx()
##    c = Chem.FindMolChiralCenters(mol)
##    
##    if a.GetSymbol() != "H": 
##        if (idx,"R") in c: return 1
##        elif (idx,"S") in c: return 2
##        else: return 0
##
##    if a.GetSymbol() == "H": 
##        if (neigh,"R") in c: return 1
##        elif (neigh,"S") in c: return 2
##        else: return 0


def is_prochiral_H(mol,idx):
    #does changing H to D make the molecule or attatched atom R or S?
    #use He instead of D, rdkit problem?
    
    mol2 = Chem.Mol(mol)
    a = mol2.GetAtomWithIdx(idx)
    neigh = a.GetNeighbors()[0].GetIdx()

    #only for H
    if a.GetSymbol() != "H": return 0
    #only for SP3
    if mol2.GetAtomWithIdx(neigh).GetDegree() != 4: return 0

    c0 = set(Chem.FindMolChiralCenters(mol2))

    a.SetAtomicNum(2)
    #a.SetIsotope(2)
    Chem.AssignAtomChiralTagsFromStructure(mol2,replaceExistingTags=True)
    
    c1 = set(Chem.FindMolChiralCenters(mol2))

    diff = c1.difference(c0)
    #print(c0,c1,diff,len(diff))
    
    #not prochiral, homotopic 
    if len(diff) == 0: return 0
    
    #prochiral
    if len(diff) >= 1:
        
        #enantiotopic  (doesnt affect chemical shift)
        if len(c1) == 1:
            #pass
            for itm in diff:
                if itm[1] == "R": return 3 #'pro-R'
                elif itm[1] == "S": return 4 #'pro-S'
            
        #diastereotopic (can affect chemical shift)
        if len(c1) > 1:
            if len(diff) == 1:
                for itm in diff:
                    if itm[1] == "R": return 1 #'pro-R'
                    elif itm[1] == "S": return 2 #'pro-S'
            elif len(diff) > 1:
                if (neigh,"R") in diff: return 1 #'pro-R'
                elif (neigh,"S") in diff: return 2 #'pro-S'
                
    return 0


def dist_from_chiral(sdf_file_with_path,idx):
    mol = Chem.MolFromMolFile(sdf_file_with_path,removeHs=False)
    Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
    chiral = Chem.FindMolChiralCenters(mol)
    if len(chiral) == 0: return 0
    path = [len(Chem.GetShortestPath(mol,idx,chi[0])) for chi in chiral]
    print("path is :{}".format(path))
    return min(path)

##def is_pro_EZ_H(mol,idx):
##
##    mol2 = Chem.Mol(mol)
##    a = mol2.GetAtomWithIdx(idx)
##
##    #only for H
##    if a.GetSymbol() != "H": return 0
##
##    #get attatched heavy atom
##    neigh = a.GetNeighbors()[0]
##    n1idx = neigh.GetIdx()
##    
##    SP2 = Chem.rdchem.HybridizationType.SP2
##
##    #only for =NH2, =CH2
##    if neigh.GetSymbol() not in ("N","C"): return 0
##    if neigh.GetHybridization() != SP2: return 0
##    if neigh.GetTotalNumHs(includeNeighbors=True) != 2: return 0
##
##    #get X from X=CH2/NH2
##    n2idx = [n.GetIdx() for n in neigh.GetNeighbors() if n.GetSymbol() != "H"][0]
##    
##    #get A,B from ABX=CH2/NH2
##    n31idx = [n.GetIdx() for n in mol2.GetAtomWithIdx(n2idx).GetNeighbors() if n.GetIdx() != n1idx]
##    #priority of A,B
##    n31_p = [2**mol2.GetAtomWithIdx(n).GetAtomicNum()  for n in n31idx]
##    
##    #neighbors of A,B plus A,B
##    n32idx = [list(expand(mol,[n3],exclude=[n2idx]))+[n3] for n3 in n31idx]
##    #priority of A,B + neighbors
##    n32_p = [sum([2**mol2.GetAtomWithIdx(n).GetAtomicNum() for n in ns ]) for ns in n32idx]
##
##    largest = None
##    if n31_p[0] > n31_p[1]:
##        largest = n31idx[0]
##    elif n31_p[0] < n31_p[1]:
##        largest = n31idx[1]
##    elif n32_p[0] > n32_p[1]:
##        largest = n31idx[0]
##    elif n32_p[0] < n32_p[1]:
##        largest = n31idx[1]
##    else:
##        return 0
##
##    path = Chem.GetShortestPath(mol,idx,largest)
##    dih = rdMolTransforms.GetDihedralDeg(mol2.GetConformer(),*path)
##    
##    if abs(dih) > 90: return 3 #trans (E)
##    if abs(dih) < 90: return 4 #cis (Z)
##    return 0 #bad geometry?, or priority needs more atoms
##
##
##def expand(mol,sele,exclude=[]):
##    new = set()
##    for s in sele:
##        new.update(i.GetIdx() for i in mol.GetAtomWithIdx(s).GetNeighbors())
##    return new - set(sele) - set(exclude)

def ReadShiftFile1(file_name):
 
  hydrogen = []
  shift = []
  with open(file_name+".txt", "r") as fp:
    lines = fp.readlines()
    for i in range(2,len(lines)):
      splited_line = lines[i].split("\t")
      len_of_splitted_line = len(splited_line)
      # print("H:{}".format(splited_line[0].strip()))
      # print("S:{}".format(splited_line[len_of_splitted_line-1].strip()))
      hydrogen.append(int(splited_line[0].strip()))
      shift.append(float(splited_line[len_of_splitted_line-1].strip()))
  return hydrogen, shift
  

def ChiralCH2(sdf_file_with_path,idx):

    mol = Chem.MolFromMolFile(sdf_file_with_path,removeHs=False)
    #sometimes needed
    Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
    return is_prochiral_H(mol,idx)



