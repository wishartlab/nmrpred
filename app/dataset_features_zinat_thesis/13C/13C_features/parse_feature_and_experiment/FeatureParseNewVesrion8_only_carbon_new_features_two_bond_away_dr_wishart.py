from rdkit import Chem
import os
import numpy as np
import operator
import csv
import json, ast
import pandas as pd
from fppackage import BondAwayPathVersion8_only_for_carbon_new_features as fingerprint
from fppackage import chiral_v2_CH2_only_for_carbon_version2 as chiral
from pathlib import Path
import os.path
import math


# with open('HMDB0000001') as json_file:
#   data = json.loads(json_file)

# print("data={}".format(data))

# jdata = ast.literal_eval(json.dumps(data))
# print("jdata={}".format(jdata))

def ReadShiftFile1(file_name, h_position):
  # print("h_position is:{} and length:{}".format(h_position,len(h_position)))
  H_position_and_chemicalshift_in_shift_file = [] # this will be returned. it is an hash of array [{}]
  hydrogen = [] # in the shift file the carbon position
  shift = [] # in the shift file the chemical shift values
  with open(file_name+".txt", "r") as fp:
    lines = fp.readlines()
    for i in range(2,len(lines)):
      splited_line = lines[i].split("\t")
      len_of_splitted_line = len(splited_line)
      # print("H:{}".format(splited_line[0].strip()))
      # print("S:{}".format(splited_line[len_of_splitted_line-1].strip()))
      hydrogen.append(int(splited_line[0].strip())-1)
      shift.append(float(splited_line[len_of_splitted_line-1].strip()))
    # print("hydrogen:{}, shif:{}".format(hydrogen, shift))
  for i in range(len(h_position)):
    if h_position[i] in hydrogen:
      index_of_hydrogen_array = hydrogen.index(h_position[i])
      H_position_and_chemicalshift_in_shift_file.append({"H_position": hydrogen[index_of_hydrogen_array], "chemical_shift": shift[index_of_hydrogen_array]})
  print("returning H_position_and_chemicalshift_in_shift_file:{}".format(H_position_and_chemicalshift_in_shift_file))
  return H_position_and_chemicalshift_in_shift_file




#### FOR HMDB COMPOUNDS #########


# file_name_panda = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/readfile_train.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/readfile_holdout.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/readfile_holdout.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/readfile_train_new.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/readfile_train_new.txt", header = None ).values
# prineeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/readfile_train.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/carbon/read_file_train.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/carbon/train_final_water_after_filter_duplicate_atoms.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_swapped_excluded_atoms.txt", header = None ).values ### ALWAYS USE THIS ONE
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms.txt", header = None ).values ### ALWAYS USE THIS ONE
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/readfile_train.txt", header = None ).values #### ALWAYS USE THIS ONE
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/mofile_mar18.txt", header = None ).values ### ALWAYS USE THIS ONE
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/clean_data.txt", header = None ).values ### ALWAYS USE THIS ONE


###### for JEOL #####
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/mofile.txt", header = None ).values ### ALWAYS USE THIS ONE

### for jeol, hmdb, bmrb ######
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/clean_data_hmdb_jeol.txt", header = None ).values ### ALWAYS USE THIS ONE
file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/dft.txt", header = None ).values ### ALWAYS USE THIS ONE
# file_name_panda_hmdb = pd.read_csv("/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/clean_data_hmdb_jeol_test.txt", header = None ).values ### ALWAYS USE THIS ONE

print("file name panda ={}".format(file_name_panda_hmdb))
file_name_panda_hmdb.sort(axis=0)
file_name_hmdb = []
for i in range(len(file_name_panda_hmdb)):
  file_name_hmdb.append(file_name_panda_hmdb[i][0])

print("to be collected files form hmdb:{}".format(file_name_hmdb))


# file_names = {"file_name_hmdb":file_name_hmdb,"file_name_gissmo":file_name_gissmo, "file_name_gissmo_uncommon":file_name_gissmo_uncommon}  ####################################### uncomment when parsing for tainset
## uncomment when parsing for train (depending on situation)
file_names = {"file_name_hmdb":file_name_hmdb}
print("the file names hash = {} ".format(file_names))

for key,file_name in file_names.items():
  print("keys are = {}".format(key))

# file_names = {"file_name_hmdb":file_name_hmdb}   ##### comment out when parse for the trainset

HEADER1 = [ "atomic_number1", "atomic_number2", "atomic_number3", "atomic_number4","aNeg", "aHyb", "Gastieger_charge", "elecPiA", "AromaticSystem", "ConjugatedSystem", "ExplicitH", "size_of_the_ring", "cripenLogP", "Molar_Refractivity","TPSA", "LASA"]
# HEADER2 =  ["Carbonyl", "CH3", "CH2", "CH", "Aldehyde", "-CH2OH","phenyl", "-OH", "-NH2", "-COOH", "-CONH2", "-C=C-", "-C triple bond C-", "-CO-", "-COOR", "-C-O-", "[-C-Cl]", "[-C-Br]", "[-C-I]", "[-C-]", "ChemicalShift", "HMDBID", "CPosition" ]
# HEADER2 = ["Aldehydes","Ketones","Carboxylic_Acid","Ester","Amide", "Urethanes", "Olefinic","Nitrile","Anomeric", "CR2OH","CH2OH", "CH3OH", "COH", "C-O", "C-N", "Carbonyl","C", "CH", "CH2", "CH3", "connected_with_aromatic_atom", "connected_with_aliphatic_ring_atom", "S_O", "SCH3", "SCH2", "CH3_with_N+", "pyridine", "C=N", "N_CH3", "Two_N_ring_=O_NH2", "NH", "NH2", "NH3", "N", "PO4", "PO4-", "Aldehydes_1","Ketones_1","Carboxylic_Acid_1","Ester_1","Amide_1", "Urethanes_1", "Olefinic_1","Nitrile_1","Anomeric_1", "CR2OH_1","CH2OH_1", "CH3OH_1", "COH_1", "C-O_1", "C-N_1", "Carbonyl_1","C_1", "CH_1", "CH2_1", "CH3_1", "[Cl]_1", "[F]_1", "[Br]_1", "[I]_1", "connected_with_aromatic_atom_1", "connected_with_aliphatic_ring_atom_1", "S_O_1", "SCH3_1", "SCH2_1", "CH3_with_N+_1", "pyridine_1", "C=N_1", "N_CH3_1", "Two_N_ring_=O_NH2_1", "NH_1", "NH2_1", "NH3_1", "N_1", "PO4_1", "PO4-_1", "Hydroxyl_1", "Aldehydes_2","Ketones_2","Carboxylic_Acid_2","Ester_2","Amide_2", "Urethanes_2", "Olefinic_2","Nitrile_2","Anomeric_2", "CR2OH_2","CH2OH_2", "CH3OH_2", "COH_2", "C-O_2", "C-N_2", "Carbonyl_2","C_2", "CH_2", "CH2_2", "CH3_2", "[Cl]_2", "[F]_2", "[Br]_2", "[I]_2", "connected_with_aromatic_atom_2", "connected_with_aliphatic_ring_atom_2", "S_O_2", "SCH3_2", "SCH2_2", "CH3_with_N+_2", "pyridine_2", "C=N_2", "N_CH3_2", "Two_N_ring_=O_NH2_2", "NH_2", "NH2_2", "NH3_2", "N_2", "PO4_2", "PO4-_2", "Hydroxyl_2", "ChemicalShift", "HMDBID", "CPosition"]
HEADER2 = ["Aldehydes","Ketones","Carboxylic_Acid","Ester","Amide", "Urethanes", "Olefinic","Nitrile","Anomeric", "CR2OH","CH2OH", "CH3OH", "COH", "C-O", "C-N", "Carbonyl", "CH", "CH2", "CH3", "connected_with_aromatic_atom", "connected_with_aliphatic_ring_atom", "SCH3", "SCH2", "CH3_with_N+", "pyridine", "C=N", "N_CH3", "Two_N_ring_=O_NH2", "Alkene_in_ring", "Alkyne", "CHCOOH", "COO-", "CH2N+", "NHCO", "O_inside_6_ring", "CHNH2", "CH2NH2", "CH2NH", "C=NOH", "Benzene", "N_3CH3", "N+_3CH3", "O_inside_5_ring", "Two_N_in_aromatic_with_=O_and_-NH2", "Furan", "6_ring_without_any_double_bond", "5_ring_with_2N_6_ring_with_2N", "glucose_4OH", "5_ring_with_one_nitroen", "Two_N_in_aromatic_with_2_=O", "Two_N_in_5_ring", "Imidazole", "C-S-C", "5ring_with_2N_6_ring_with_2N", "infused_benzene_ring_one_with_5ring_N"] 
HEADER3 = ["Aldehydes_1","Ketones_1","Carboxylic_Acid_1","Ester_1","Amide_1", "Urethanes_1", "Olefinic_1","Nitrile_1","Anomeric_1", "CR2OH_1","CH2OH_1", "CH3OH_1", "COH_1", "C-O_1", "C-N_1", "Carbonyl_1", "CH_1", "CH2_1", "CH3_1", "[Cl]_1", "[F]_1", "[Br]_1", "[I]_1", "connected_with_aromatic_atom_1", "connected_with_aliphatic_ring_atom_1", "S_O_1", "SCH3_1", "SCH2_1", "CH3_with_N+_1", "pyridine_1", "C=N_1", "N_CH3_1", "Two_N_ring_=O_NH2_1", "NH_1", "NH2_1", "NH3_1", "N_1", "PO4_1", "PO4-_1", "Hydroxyl_1", "Alkene_in_ring_1", "Alkyne_1", "CHCOOH_1", "COO-_1", "CH2N+_1", "N_in_6_ring_1", "NHCO_1", "O_inside_6_ring_1", "CHNH2_1", "CH2NH2_1", "CH2NH_1", "C=NOH_1", "PO_OH2_1", "SO2OH_1", "Benzene_1", "N_3CH3_1", "N+_3CH3_1", "O_inside_5_ring_1", "Two_N_in_aromatic_with_=O_and_-NH2_1", "Furan_1", "6_ring_without_any_double_bond_1", "As=O-OH_1", "5_ring_with_2N_6_ring_with_2N_1", "glucose_4OH_1", "5_ring_with_one_nitroen_1", "Imidazole_1", "Two_N_in_aromatic_with_2_=O_1", "Two_N_in_5_ring_1", "C-S-C_1", "5ring_with_2N_6_ring_with_2N_1", "infused_benzene_ring_one_with_5ring_N_1"]
HEADER4 = ["Aldehydes_2","Ketones_2","Carboxylic_Acid_2","Ester_2","Amide_2", "Urethanes_2", "Olefinic_2","Nitrile_2","Anomeric_2", "CR2OH_2","CH2OH_2", "CH3OH_2", "COH_2", "C-O_2", "C-N_2", "Carbonyl_2", "CH_2", "CH2_2", "CH3_2", "[Cl]_2", "[F]_2", "[Br]_2", "[I]_2", "connected_with_aromatic_atom_2", "connected_with_aliphatic_ring_atom_2", "S_O_2", "SCH3_2", "SCH2_2", "CH3_with_N+_2", "pyridine_2", "C=N_2", "N_CH3_2", "Two_N_ring_=O_NH2_2", "NH_2", "NH2_2", "NH3_2", "N_2", "PO4_2", "PO4-_2", "Hydroxyl_2", "Alkene_in_ring_2", "Alkyne_2", "CHCOOH_2", "COO-_2", "CH2N+_2", "N_in_6_ring_2", "NHCO_2", "O_inside_6_ring_2", "CHNH2_2", "CH2NH2_2", "CH2NH_2", "C=NOH_2", "PO_OH2_2", "SO2OH_2", "Benzene_2", "N_3CH3_2", "N+_3CH3_2", "O_inside_5_ring_2", "Two_N_in_aromatic_with_=O_and_-NH2_2", "Furan_2", "6_ring_without_any_double_bond_2", "As=O-OH_2", "5_ring_with_2N_6_ring_with_2N_2", "glucose_4OH_2", "5_ring_with_one_nitroen_2", "Imidazole_2", "Two_N_in_aromatic_with_2_=O_2", "Two_N_in_5_ring_2", "C-S-C_2", "5ring_with_2N_6_ring_with_2N_2", "infused_benzene_ring_one_with_5ring_N_2", "ChemicalShift", "HMDBID", "CPosition"]


HEADER = HEADER1 + HEADER2 + HEADER3 + HEADER4
# HEADER = HEADER1 + HEADER2


# with open('/Volumes/Samsung_T5/Mac_Contents/rdkit/carbon/train_carbon_water_after_filter_duplicate_atoms_after_one_example_test.csv', 'w') as csv_file: 
# with open('/Volumes/Samsung_T5/Mac_Contents/rdkit/carbon/train_carbon_water_after_filter_duplicate_atoms_only_carbon_featues_version1.csv', 'w') as csv_file:
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_with_34_f_grp.csv', 'w') as csv_file:  
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_with_112_f_group.csv', 'w') as csv_file:  
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms_version2_with_112_f_group.csv', 'w') as csv_file:  
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_Train2_fixed_HMDB.csv', 'w') as csv_file:
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/train_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_Train2_fixed_HMDB_swapped_excluded_atoms.csv', 'w') as csv_file:    
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/holdout_final_water_after_filter_duplicate_atoms_version2_with_112_f_group_Train2_fixed_HMDB.csv', 'w') as csv_file:
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/train_clean_dataset1_less_feature.csv', 'w') as csv_file:    
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_less_feature.csv', 'w') as csv_file:    

### for JEOL ####

# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_holdout_jeol.csv', 'w') as csv_file:    
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_less_feature_clean_data_tms_to_dss.csv', 'w') as csv_file:       
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_less_feature_clean_data_tms.csv', 'w') as csv_file:      

# for jeol, hmdb, bmrb ####
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_less_feature_clean_data_tms.csv', 'w') as csv_file:      
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss.csv', 'w') as csv_file:      
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_2.csv', 'w') as csv_file:      
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_3_test.csv', 'w') as csv_file:      
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_2_corrected.csv', 'w') as csv_file:      
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_one_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number.csv', 'w') as csv_file:      
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_two_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_modified_features.csv', 'w') as csv_file: 
# with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/train_plus_holdout_jeol_hmdb_bmrb_new_features_two_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_modified_features_with_more_features_1st_phase_outliers_checking_correct.csv', 'w') as csv_file: 
with open('/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/holdout_jeol_hmdb_bmrb_new_features_two_bond_away_clean_data_tms_to_dss_2_corrected_sorted_atomic_number_modified_features_with_more_features_1st_phase_outliers_checking_correct_dr_wishart_conference.csv', 'w') as csv_file: 
   
  writer = csv.writer(csv_file)
  writer.writerow(HEADER)


  for key,file_name in file_names.items():
    for single_file in file_name:
      print("single_file = {}".format(single_file))
      print("key={}".format(key))
      if key == "file_name_hmdb":
        #single_file_with_path = feature file
        # single_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/features_real_cdkdescriptor_train/"+single_file
        # single_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/feature_jeol_carbon_jar/"+single_file
        ## For JEOL ###
        # single_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/featur_jeol/"+single_file
        # for jeol, hmdb, bmrb ####
        # single_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/feature_jeol_carbon_jar/"+single_file
        single_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/dft_13c_features/"+single_file
        
      print("Found the single file={}".format(single_file_with_path))
      
      if os.path.isfile(single_file_with_path):
        with open(single_file_with_path) as json_file:
          data = json.load(json_file)
          jdata = ast.literal_eval(json.dumps(data))
          data_row_hash_with_h_positions = jdata["AtomDescriptorValue"]
          
          if key == "file_name_hmdb":
            hmdb_id = single_file
            print("HMDBID={}".format(hmdb_id))
          
          #hydrogen_position refers to carbon position
          hydrogen_position = []
          atom_positions = [int(m) for m in jdata["AtomSymbol"].keys()]
          atom_symbols = [m for m in jdata["AtomSymbol"].values()]
          for one_symbol in range(len(atom_symbols)):
            if atom_symbols[one_symbol] == "C":
              hydrogen_position.append(atom_positions[one_symbol])
            else:
              print("No Carbon")
            
          
          print("hydrogen position={}".format(hydrogen_position))
        
          if key == "file_name_hmdb":
            # chem_file_path = chemical shift file path
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Train/"+single_file
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Holdout/"+single_file
            
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Train2_fixed_HMDB/used_train_file/"+single_file
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Train2_fixed_HMDB/used_train_file_after_swapped_some_excluded_atoms/"+single_file
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Holdout2_fixed_HMDB/used_holdout_file/"+single_file
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/train/"+single_file
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/JEOL_assignemnts/cdcl3_tms/"+single_file

            ### For JEOL ####
            chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/amy_tabbed/"+single_file

            # for jeol, hmdb, bmrb ####
            # chem_file_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/clean_data_jeol_hmdb_bmrb_d2o/"+single_file
            # sdf_file_with_path = "/Volumes/Samsung_T5/Mac_Contents/rdkit/carbon/all_compounds/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Train/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Train2_fixed_HMDB/used_train_file/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Train2_fixed_HMDB/used_train_file_after_swapped_some_excluded_atoms/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/Holdout2_fixed_HMDB/used_holdout_file/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/clean_data_experiment/train/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/JEOL_molfiles/"+single_file+".sdf" #change the path here
            
            ### For JEOL ###
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/JEOL_molfiles/"+single_file+".sdf" #change the path here
            ### for jeol,hmdb,bmrb #####
            # sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/clean_data_jeol_hmdb_bmrb_d2o/"+single_file+".sdf" #change the path here
            sdf_file_with_path = "/Volumes/T7_SSD2/MAC_Contents/rdkit/carbon/D2O_Experiment/data/paper_data/jeol/dft_sdf/"+single_file+".sdf" #change the path here
            
          print("chem file path ={}".format(chem_file_path))
          H_position_and_chemicalshift_in_shift_file = ReadShiftFile1(chem_file_path,hydrogen_position)

  
          for i in hydrogen_position:
            data_row_temp = data_row_hash_with_h_positions[str(i)]
            print("data_row_temp = {}".format(data_row_temp))
            data_row = chiral.atomic_number_with_neighbors(i,sdf_file_with_path)
            print("data_row after atomic number = {}".format(data_row))
            data_row.append(data_row_temp[0])
            data_row.append(data_row_temp[1])
            gc = chiral.GastierCharge(i,sdf_file_with_path)
            if math.isnan(float(gc)):
              data_row.append(0)
            else:
              data_row.append(gc)
            elecpi = data_row_temp[18]
            if math.isnan(float(elecpi)):
              data_row.append(0)
            else:
              data_row.append(data_row_temp[18])
            print("data_row after atomic number and next 4 = {}".format(data_row))
            data_row.append(chiral.aromaticAtom(i,sdf_file_with_path))
            data_row.append(chiral.conjugate_bond(i,sdf_file_with_path))
            data_row.append(chiral.explicit_H(i,sdf_file_with_path))
            data_row.append(chiral.size_of_the_ring_if_in_a_ring(i,sdf_file_with_path))
            logp, mr = chiral.CalcCrippenLogPAndMR(i,sdf_file_with_path)
            data_row.append(logp)
            data_row.append(mr)
            data_row.append(chiral.tpsa(i,sdf_file_with_path))
            data_row.append(chiral.LASA(i,sdf_file_with_path))
            print("data_row of HEADER1 = {}".format(data_row))
            h_p = i
            for d in H_position_and_chemicalshift_in_shift_file:
              if  d["H_position"] == h_p:
                # a = fingerprint.OneBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                #   data_row.append(a[i])
                # a = fingerprint.TwoBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                #   data_row.append(a[i])
                # a = fingerprint.ThreeBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                #   data_row.append(a[i])
                print("position {}  sdf_file_path {}".format(d["H_position"],sdf_file_with_path))
                print("started zero bond for atom ={}",format(d["H_position"]))
                #####
                #####
                a = fingerprint.carbon_fp_zero_bond(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                for i in a:
                  # data_row.append(a[i])
                  data_row.append(i)
                print("data_row after zero bond = {}".format(data_row))
                print("started one bond for atom ={}",format(d["H_position"]))
                b = fingerprint.carbon_fp_one_bond_correct(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(b)):
                #   data_row.append(b[i])
                for j in b:
                  data_row.append(j)
                print("data_row after one bond = {}".format(data_row))
                print("started two bond for atom ={}",format(d["H_position"]))
                c = fingerprint.carbon_fp_two_bond_correct(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(b)):
                #   data_row.append(b[i])
                for j in c:
                  data_row.append(j)
                print("data_row after two bond = {}".format(data_row))
                #####
                #####
                # ProChiralH = chiral.is_prochiral_H(sdf_file_with_path,int(h_p))
                # ProChiralH = chiral.ChiralCH2(sdf_file_with_path,int(h_p))
                # print("ProChiralH = {}".format(ProChiralH))
                # if ProChiralH == 1 or ProChiralH == 2:
                #   data_row.append(1)
                # else:
                #   data_row.append(0)
                # data_row.append(ProChiralH)

                # distance_from_chiral_centre = chiral.dist_from_chiral(sdf_file_with_path,int(h_p))
                # distance_from_chiral_centre = chiral.dist_from_chiral(sdf_file_with_path,int(h_p))
                # data_row.append(distance_from_chiral_centre)
                
                
                # for l in range(len(atoms_aromacity)):
                
                data_row.append(d["chemical_shift"])
                data_row.append(hmdb_id)
                data_row.append(d["H_position"])
                print("data row={}".format(data_row))
                print("length of data row={}".format(len(data_row)))
            writer.writerow(data_row)




