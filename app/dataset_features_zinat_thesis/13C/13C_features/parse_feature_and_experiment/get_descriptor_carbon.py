import os,sys
import subprocess
from pathlib import Path




def get_descriptor_atom(sdf_file_path,nearest_atom,file_name,atom):

	sdf_file = Path(sdf_file_path)
  # feature_file = "/Users/zinatsayeeda/anaconda3/envs/get_descriptors/Xuam_new_descriptor/Latest/features/" + file_name + ".txt"
	if sdf_file.is_file():
    # log_file_name = "/Users/zinatsayeeda/anaconda3/envs/get_descriptors/Xuam_new_descriptor/Latest/"+file_name+".txt"
    
		subprocess.call(['java', '-jar', 'carbon_2.jar', '-i', sdf_file_path, '-o', file_name, '-a', atom, '-n', str(nearest_atom)])
	else:
		print("{} doesn't exist!".format(sdf_file_path))

