# This script is to parse the trainset mol ids out of the train_holdout.txt
# and create a new file called trainset.txt with only training mol ids
train_and_holdout = []
holdout = []
train = []

with open('train_holdout.txt', 'r') as f:
  ids = f.readlines()
  for id in ids: train_and_holdout.append(id)

with open('holdout1.txt', 'r') as f:
  ids = f.readlines()
  for id in ids: holdout.append(id)

train = list(set(train_and_holdout) - set(holdout))

with open('trainset.txt', 'w') as f:
  for id in train: f.write(id)
