
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.datasets import load_digits
from sklearn.model_selection import validation_curve
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error,r2_score
from sklearn.model_selection import StratifiedKFold
import copy
from sklearn.model_selection import KFold
from decimal import *
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
import time
from sklearn.ensemble import RandomForestRegressor
from xgboost import XGBRFRegressor
import plotly.graph_objects as go
import plotly
from operator import itemgetter
from collections import OrderedDict






def main():
   
    # trainingSetFile = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/Not_overfitting_exp/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2.csv"
    # trainingSetFile = "/Volumes/T7_SSD2/Mac_Contents/rdkit/experiment_final_scripts/Not_overfitting_exp/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_with_nan.csv"
    trainingSetFile = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_with_nan.csv"
    dataset = pd.read_csv(trainingSetFile)
    print("dataset shape:{}".format(dataset.shape))
    dataset = dataset.drop(columns=['protonTotalPartialCharge2', 'protonTotalPartialCharge3', 'protonTotalPartialCharge4', 'protonTotalPartialCharge5','protonTotalPartialCharge2.1', 'protonTotalPartialCharge3.1', 'protonTotalPartialCharge4.1', 'protonTotalPartialCharge5.1','protonTotalPartialCharge2.2', 'protonTotalPartialCharge3.2', 'protonTotalPartialCharge4.2', 'protonTotalPartialCharge5.2','protonTotalPartialCharge2.3', 'protonTotalPartialCharge3.3', 'protonTotalPartialCharge4.3', 'protonTotalPartialCharge5.3'])
    ### protonTotalPartialCharge2 3, 4,5 were all nan column
    print("dataset shape:{}".format(dataset.shape))
    dataset = dataset.dropna(axis=0, subset=["HMDBID"])
    print("dataset shape:{}".format(dataset.shape))
    dataset = dataset.drop(columns=['Unnamed: 0'])
    # print("dataset shape after dropping Unnamed:{}".format(dataset.shape))
    dataset = dataset.drop(columns=["aHyb", "val"])
    dataset = dataset.drop(columns=['pepe', 'pepe.1', 'pepe.2', 'pepe.3','ipAtomicLearning', 'ipAtomicLearning.1', 'ipAtomicLearning.2', 'ipAtomicLearning.3'])
    print("dataset shape after dropping aHyb and val:{}".format(dataset.shape))


    ### dropping the holdout data from BMRB/GISSMO datatset ###
    dataset = dataset[~dataset['HMDBID'].isin(['bmse000300_simulation_1_nmredata','bmse000322_simulation_1_nmredata','bmse000330_simulation_1_nmredata','bmse000184_simulation_1_nmredata','bmse000389_simulation_1_nmredata','bmse000435_simulation_1_nmredata','bmse000856_simulation_1_nmredata','bmse000364_simulation_1_nmredata','bmse000337_simulation_1_nmredata','bmse000044_simulation_1_nmredata','bmse000915_simulation_1_nmredata','bmse000404_simulation_1_nmredata'])]
    print("final train dataset shape after dropping common holdout:{}".format(dataset.shape))
    #################################



    hmdb_ids = dataset.iloc[ :,-2].values
    hmdb_ids = hmdb_ids.reshape(hmdb_ids.shape[0],1)
    hydrogen_positions = dataset.iloc[ :,-1].values
    hydrogen_positions = hydrogen_positions.reshape(hydrogen_positions.shape[0],1)

    print("nan value: {}".format(dataset.isnull().sum()))
    X = dataset.iloc[ : , 0:-3].values

    X = X.astype(float)

    y =  dataset.iloc[ :,-3].values
    y = y.reshape(y.shape[0],1)
    y = y.astype(float)
    Y = y
    # Y = classLabel(y)
    # print("Y after classLabel:{}".format(Y))
    # Y = Y.reshape(Y.shape[0],1)
    # Y = Y.astype(int)

    folds = 5

    cv = KFold(n_splits=folds, random_state=1, shuffle=True)
    parameter_name1 = "C"

    # parameter_range1= [600]
    # parameter_range2= [75,95]

  
    parameter_range1= [400,500,600,700,800,900,1000,1100,1200]
    parameter_range2= [25,30,35,40,45,50,55,60,65,70,75,80,85,90,95]


    file_name_group_by_panda_series = dataset.groupby('HMDBID')
    file_name_group_by_dict = dict(list(file_name_group_by_panda_series))
    file_name = np.array(list(file_name_group_by_dict.keys()))
    file_name = file_name.reshape(file_name.shape[0],1)


    # itertation = len(parameter_range2)
    # itertation = len(parameter_range1)
    mean_scores ={}
    mean_scores_mse = {}
    mean_scores_r2 = {}
    mean_scores_train ={}
    mean_scores_r2_train = {}
    mean_scores_mse_train ={}
    saved_models = {}
    diff_btw_train_test_mean_score = {}
    for ep in parameter_range2: # ep = max_depth
      # for i in range(itertation):
      y_train_mae_vales = []
      y_test_mae_vales = []
      for c in parameter_range1: # c = n_estimators
        cv_scores = []
        r2_scores = []
        cv_scores_mse = []
        saved_models_cv = []
        cv_scores_fold_pred_for_train = []
        cv_scores_mse_fold_pred_for_train = []
        r2_scores_fold_pred_for_train = []
        for (inner_train_index_filename, inner_test_index_filename), j in zip(cv.split(file_name), range(folds)):
          file_name_inner_train = file_name[inner_train_index_filename]
          file_name_inner_test = file_name[inner_test_index_filename]
  
          X_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
          X_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
  
          X_train_inner_temp = dataset[X_train_inner_new_filter] #this is always the datset format after cv
          X_test_inner_temp = dataset[X_test_inner_new_filter] #this is always the datset format after cv
          y_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
          y_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
  
          y_train_inner_temp = dataset[y_train_inner_new_filter] #this is always the datset format after cv
          y_test_inner_temp = dataset[y_test_inner_new_filter] #this is always the datset format after cv
  
          X_train_inner = X_train_inner_temp.drop(columns=["ChemicalShift","HMDBID","HPosition"])
          X_train_inner = X_train_inner.iloc[: , :].values 
          X_train_inner = X_train_inner.astype(float)
  
  
          X_test_inner = X_test_inner_temp.drop(columns=["ChemicalShift","HMDBID","HPosition"])
          X_test_inner = X_test_inner.iloc[: , :].values 
          X_test_inner = X_test_inner.astype(float)
  
  
          y_train_inner = y_train_inner_temp["ChemicalShift"].values
          y_train_inner = y_train_inner.astype(float)
          y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
          # y_train_inner = classLabel(y_train_inner)
          # y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
          # y_train_inner = y_train_inner.astype(int)
  
          y_test_inner = y_test_inner_temp["ChemicalShift"].values
          y_test_inner = y_test_inner.astype(float)
          y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)
          # y_test_inner = classLabel(y_test_inner)
          # y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)
          # y_test_inner = y_test_inner.astype(int)

  
  
          hmdb_ids_inner_fold_train = X_train_inner_temp["HMDBID"].values
          hmdb_ids_inner_fold_train = hmdb_ids_inner_fold_train.reshape(hmdb_ids_inner_fold_train.shape[0],1)
  
          hmdb_ids_inner_fold_test = X_test_inner_temp["HMDBID"].values
          hmdb_ids_inner_fold_test = hmdb_ids_inner_fold_test.reshape(hmdb_ids_inner_fold_test.shape[0],1)
  
  
          hydrogen_positions_inner_fold_train = X_train_inner_temp["HPosition"].values
          hydrogen_positions_inner_fold_train = hydrogen_positions_inner_fold_train.reshape(hydrogen_positions_inner_fold_train.shape[0],1)
  
          hydrogen_positions_inner_fold_test = X_test_inner_temp["HPosition"].values
          hydrogen_positions_inner_fold_test = hydrogen_positions_inner_fold_test.reshape(hydrogen_positions_inner_fold_test.shape[0],1)
  
          print("fold {} X_train_inner shape ={} and y_test_inner shape ={} ".format(j,X_train_inner.shape,y_test_inner.shape))
          model_inner = XGBRFRegressor(n_estimators= c, max_depth= ep, subsample=0.9, colsample_bynode=0.05)
          print("model creation done")
          # print(type(X_train_inner))
          # print(type(X_train_inner[:5,:]))
          t1 = time.time()
          model_inner.fit(X_train_inner, y_train_inner.ravel())
          print("model fitting done:{}".format(time.time()-t1))
          y_pred_inner = model_inner.predict(X_test_inner)
          y_pred_inner = y_pred_inner.astype(float)
          # y_pred_inner = y_pred_inner/float(100)
          y_test_inner = y_test_inner.astype(float)
          # y_test_inner = y_test_inner/float(100)
          meanAbsError_inner = mean_absolute_error(y_test_inner, y_pred_inner) # sum up of all the mae of y_pred and y_test
          cv_scores.append(meanAbsError_inner)
  
          mSError_inner = mean_squared_error(y_test_inner, y_pred_inner)
          cv_scores_mse.append(mSError_inner)
  
          r2 = r2_score(y_test_inner, y_pred_inner)
          r2_scores.append(r2)
  
          y_pred_inner_for_fold_train = model_inner.predict(X_train_inner)
          y_pred_inner_for_fold_train = y_pred_inner_for_fold_train.astype(float)
          # y_pred_inner_for_fold_train = y_pred_inner_for_fold_train/float(100)

          # y_train_inner = y_train_inner.astype(float)
          # y_train_inner = y_train_inner/float(100)
          # y_train_inner = np.ravel(y_train_inner)
  
          meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner, y_pred_inner_for_fold_train) 
          cv_scores_fold_pred_for_train.append(meanAbsError_inner_fold_pred_for_train)
  
          mSError_inner_fold_pred_for_train = mean_squared_error(y_train_inner, y_pred_inner_for_fold_train) 
          cv_scores_mse_fold_pred_for_train.append(mSError_inner_fold_pred_for_train)
  
          r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner, y_pred_inner_for_fold_train)
          r2_scores_fold_pred_for_train.append(r2_scores_inner_fold_pred_for_train)
  
          print("started prediction for n_estimators:{}   max_depth:{}".format(c,ep))
          for k in range(len(y_pred_inner)):
            print("Printing  prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred_inner[k], y_test_inner[k],hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k]))
            if float(abs(y_pred_inner[k]-y_test_inner[k])) >= 1.0 :
              print("outliers{} {} true:{}  pred:{}".format(hmdb_ids_inner_fold_test[k][0],hydrogen_positions_inner_fold_test[k][0],y_test_inner[k],y_pred_inner[k]))
          print("for th:{} cv of n_estimators:{}   max_depth:{}, MAE  is:{} ".format(j,c,ep,cv_scores[j]))
  
        mean_scores[str(c)+"_"+str(ep)] = np.mean(cv_scores)
        print("And ALL MAEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores))
        print("And AVG MAE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores)))
        y_test_mae_vales.append(np.mean(cv_scores))
        print("mean y_test_mae_vales = {}".format(y_test_mae_vales))

  
        mean_scores_mse[str(c)+"_"+str(ep)] = np.mean(cv_scores_mse)
        print("And ALL MSEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores_mse))
        print("And AVG MSE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores_mse)))
  
        mean_scores_r2[str(c)+"_"+str(ep)] = np.mean(r2_scores)
        print("And ALL R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,r2_scores))
        print("And AVG R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(r2_scores)))

        mean_scores_r2_train[str(c)+"_"+str(ep)] = np.mean(r2_scores_fold_pred_for_train)
        print("And ALL R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,r2_scores_fold_pred_for_train))
        print("And AVG R2s for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(r2_scores_fold_pred_for_train)))  
  
        mean_scores_train[str(c)+"_"+str(ep)] = np.mean(cv_scores_fold_pred_for_train)
        print("And ALL Train MAEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores_fold_pred_for_train))
        print("And AVG Train MAE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores_fold_pred_for_train)))
        y_train_mae_vales.append(np.mean(cv_scores_fold_pred_for_train))
        print("mean y_train_mae_vales = {}".format(y_train_mae_vales))
  
        mean_scores_mse_train[str(c)+"_"+str(ep)] = np.mean(cv_scores_mse_fold_pred_for_train)
        print("And ALL Train MSEs for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,cv_scores_mse_fold_pred_for_train))
        print("And AVG Train MSE for n_estimators:{}   max_depth:{}  is:{}".format(c,ep,np.mean(cv_scores_mse_fold_pred_for_train)))

        train_mean = np.mean(cv_scores_fold_pred_for_train)
        test_mean = np.mean(cv_scores)
        diff_btw_train_test_mean_score[str(c)+"_"+str(ep)] = float(abs(train_mean-test_mean))
        print("And ALL diff_btw_train_test_mean_score for C:{}   ep:{}  is:{}".format(c,ep,diff_btw_train_test_mean_score))


        print("for max_depth = {}, the graph is getting created:".format(ep))
        x_axis = parameter_range1
        y_train_mae_vales = [ np.round(i,2) for i in y_train_mae_vales]
        y_test_mae_vales = [ np.round(i,2) for i in y_test_mae_vales]
        print("inside graph y_train_mae_vales = {}".format(y_train_mae_vales))
        print("inside graph y_test_mae_vales = {}".format(y_test_mae_vales))

      fig = go.Figure()
      fig.add_trace(go.Scatter(x=x_axis, y=y_train_mae_vales,
                      mode='lines+markers',
                      name='Train MAE'))
      fig.add_trace(go.Scatter(x=x_axis, y=y_test_mae_vales,
                      mode='lines+markers',
                      name='Test MAE'))



      fig.update_xaxes(showgrid=True,tickmode ='array',tickvals = x_axis)
      fig.update_yaxes(showgrid=True,tickangle=45,tickmode ='array',tickvals = [0,0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.2,0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29,0.30])
      file_name_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/"+str(ep)+".html"
      plotly.offline.plot(fig, filename=file_name_graph,auto_open=False)




    keys_of_mean_scores = list(mean_scores.keys())
    values_of_mean_scores = list(mean_scores.values())
    values_of_mean_scores_temp = copy.copy(values_of_mean_scores)
    values_of_mean_scores_temp.sort()
    index_of_lowest_mae = values_of_mean_scores.index(values_of_mean_scores_temp[0])
    best_mae = values_of_mean_scores[index_of_lowest_mae]
    print("index of lowest MAE ={}".format(index_of_lowest_mae))
    print("BEST AVG MAE:{}".format(best_mae))
    print("BEST MAX_DEPTH:{}".format(keys_of_mean_scores[index_of_lowest_mae].split("_")[1]))
    print("BEST N_ESTIMATORS:{}".format(keys_of_mean_scores[index_of_lowest_mae].split("_")[0]))
    print("All MAEs from all parametes:{}".format(mean_scores))
    print("All Train MAEs from all parametes:{}".format(mean_scores_train))
    print("All R2s from all parametes:{}".format(mean_scores_r2))

    sorted_d = OrderedDict(sorted(diff_btw_train_test_mean_score.items(), key=lambda t: t[1])) 
    again_d = dict(sorted_d)
    print("The differences between Train and Test AVG MAEs: {}".format(again_d))



      
    ##### SAVE MODELS #####
    # with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/saved_model_RFC_best_param.pickle', 'wb') as handle:
    #   pickle.dump(saved_models, handle, protocol=pickle.HIGHEST_PROTOCOL)

def classLabel(y_value):      
  cls = [] 
  for i in range(1,1001): #for 1000 class        
    cls.append({"value": float(i)/float(100), "class": i})       
  y_value_temp = y_value[:]
  y_value_classlabel = np.zeros(len(y_value_temp))
  for i in range(len(y_value_temp)):
    y_value_temp[i] = np.round(y_value_temp[i],2) # changed it from 1 to 2
  cls_value = [cls[i]["value"] for i in range(len(cls))]
  cls_class = [cls[i]["class"] for i in range(len(cls))]
  cls_value_tuple = tuple(cls_value)
  cls_class_tuple = tuple(cls_class)
  cls_class_value_dict = dict(zip(cls_class_tuple,cls_value_tuple))
  for i in range(len(y_value_temp)):
    for k, v in cls_class_value_dict.items():
      if v == y_value_temp[i]:
        y_value_classlabel[i] = k
        break
      else:
        y_value_classlabel[i] = 1001.0 
  print("y value has been transferred into class:{}".format(y_value_classlabel)) 
  return y_value_classlabel








if __name__ == '__main__':
  main()
