from rdkit import Chem
import os
import numpy as np
import operator
import csv
import json, ast
import pandas as pd
from fppackage import BondAwayPathVersion8 as fingerprint
from fppackage import chiral_v2_CH2 as chiral
from pathlib import Path
import os.path


# with open('HMDB0000001') as json_file:
#   data = json.loads(json_file)

# print("data={}".format(data))

# jdata = ast.literal_eval(json.dumps(data))
# print("jdata={}".format(jdata))

def ReadShiftFile1(file_name, h_position):
  # print("h_position is:{} and length:{}".format(h_position,len(h_position)))
  H_position_and_chemicalshift_in_shift_file = [] # this returns after checking "O" and "N" as neighbor atoms
  hydrogen_positions_in_mol_file = []
  hydrogen = []
  shift = []
  with open(file_name+".txt", "r") as fp:
    lines = fp.readlines()
    for i in range(2,len(lines)):
      splited_line = lines[i].split("\t")
      len_of_splitted_line = len(splited_line)
      # print("H:{}".format(splited_line[0].strip()))
      # print("S:{}".format(splited_line[len_of_splitted_line-1].strip()))
      hydrogen.append(int(splited_line[0].strip()))
      shift.append(float(splited_line[len_of_splitted_line-1].strip()))
    # print("hydrogen:{}, shif:{}".format(hydrogen, shift))
  for i in range(len(h_position)):
    if h_position[i] in hydrogen:
      index_of_hydrogen_array = hydrogen.index(h_position[i])
      H_position_and_chemicalshift_in_shift_file.append({"H_position": hydrogen[index_of_hydrogen_array], "chemical_shift": shift[index_of_hydrogen_array]})
  print("returning H_position_and_chemicalshift_in_shift_file:{}".format(H_position_and_chemicalshift_in_shift_file))
  return H_position_and_chemicalshift_in_shift_file




#### FOR HMDB COMPOUNDS #########


# file_name_panda = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/readfile_train.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/readfile_holdout.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/readfile_holdout.txt", header = None ).values
file_name_panda_hmdb = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/readfile_train_new.txt", header = None ).values
# file_name_panda_hmdb = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/readfile_train_new.txt", header = None ).values
# prineeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/readfile_train.txt", header = None ).values

print("file name panda ={}".format(file_name_panda_hmdb))
file_name_panda_hmdb.sort(axis=0)
file_name_hmdb = []
for i in range(len(file_name_panda_hmdb)):
  file_name_hmdb.append(file_name_panda_hmdb[i][0])

print("to be collected files form hmdb:{}".format(file_name_hmdb))

###############################################


#### FOR GISSMO COMPOUND ########     ### uncomment this when parse for train set

# file_name_panda = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/readfile_train.txt", header = None ).values
# file_name_panda_gissmo = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/water_solvent_compounds_gissmo.txt", header = None ).values
# file_name_panda_gissmo = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/water_solvent_compounds_gissmo_bmrb_filtered.txt", header = None ).values
# file_name_panda_gissmo = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/water_solvent_compounds_gissmo_bmrb_filtered.txt", header = None ).values
file_name_panda_gissmo = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/water_solvent_compounds_gissmo_bmrb_filtered.txt", header = None ).values
# prineeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/readfile_train.txt", header = None ).values
print("file name panda gissmo ={}".format(file_name_panda_gissmo))
file_name_panda_gissmo.sort(axis=0)
file_name_gissmo = []
for i in range(len(file_name_panda_gissmo)):
  file_name_gissmo.append(file_name_panda_gissmo[i][0])

print("to be collected files:{}".format(file_name_gissmo))

##############################################################

'''
#### FOR GISSMO COMPOUND ########     ### uncomment this when parse for train set


# file_name_panda_gissmo_uncommon = pd.read_csv("/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/uncommon_bmrb_gissmo.txt", header = None ).values
file_name_panda_gissmo_uncommon = pd.read_csv("/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/uncommon_bmrb_gissmo.txt", header = None ).values
# prineeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/readfile_train.txt", header = None ).values
print("file name panda gissmo uncommon ={}".format(file_name_panda_gissmo_uncommon))
file_name_panda_gissmo_uncommon.sort(axis=0)
file_name_gissmo_uncommon = []
for i in range(len(file_name_panda_gissmo_uncommon)):
  file_name_gissmo_uncommon.append(file_name_panda_gissmo_uncommon[i][0])

print("to be collected files:{}".format(file_name_gissmo_uncommon))

##############################################################
'''
# file_names = {"file_name_hmdb":file_name_hmdb,"file_name_gissmo":file_name_gissmo, "file_name_gissmo_uncommon":file_name_gissmo_uncommon}  ####################################### uncomment when parsing for tainset
## uncomment when parsing for train (depending on situation)
file_names = {"file_name_hmdb":file_name_hmdb, "file_name_gissmo":file_name_gissmo} 
print("the file names hash = {} ".format(file_names))

for key,file_name in file_names.iteritems():
  print("keys are = {}".format(key))

# file_names = {"file_name_hmdb":file_name_hmdb}   ##### comment out when parse for the trainset

HEADER1 = [ "aNeg", "aHyb", "hybr", "val", "bondsToAtom", "covalentRadius", "distanceToAtom", "effAtomPol", "indAtomHardnesss", "indAtomSoftness", "ipAtomicHOSE", "protonInArmaticSystem", "protonInConjSystem", "pepe", "partialSigmaCharge", "partialTCMMFF94", "pepeT", "periodicTablePosition", "elecPiA", "protonAffiHOSE", "protonTotalPartialCharge1", "protonTotalPartialCharge2", "protonTotalPartialCharge3", "protonTotalPartialCharge4", "protonTotalPartialCharge5", "g3r_1", "g3r_2", "g3r_3", "g3r_4", "g3r_5", "g3r_6", "g3r_7", "g3r_8", "g3r_9", "g3r_10", "g3r_11", "g3r_12", "g3r_13", "gDr_1", "gDr_2", "gDr_3", "gDr_4", "gDr_5", "gDr_6", "gDr_7", "RDF_GHR_0", "RDF_GHR_1", "RDF_GHR_2", "RDF_GHR_3", "RDF_GHR_4", "RDF_GHR_5", "RDF_GHR_6", "RDF_GHR_7", "RDF_GHR_8", "RDF_GHR_9", "RDF_GHR_10", "RDF_GHR_11", "RDF_GHR_12", "RDF_GHR_13", "RDF_GHR_14", "gHrTop_1", "gHrTop_2", "gHrTop_3", "gHrTop_4", "gHrTop_5", "gHrTop_6", "gHrTop_7", "gHrTop_8", "gHrTop_9", "gHrTop_10", "gHrTop_11", "gHrTop_12", "gHrTop_13", "gHrTop_14", "gHrTop_15", "gSr_1", "gSr_2", "gSr_3", "gSr_4", "gSr_5", "gSr_6", "gSr_7", "elecSigmA", "stabilPlusC", "vdwRadius", "ipAtomicLearning", "aNeg", "aHyb", "hybr", "val", "bondsToAtom", "covalentRadius", "distanceToAtom", "effAtomPol", "indAtomHardnesss", "indAtomSoftness", "ipAtomicHOSE", "protonInArmaticSystem", "protonInConjSystem", "pepe", "partialSigmaCharge", "partialTCMMFF94", "pepeT", "periodicTablePosition", "elecPiA", "protonAffiHOSE", "protonTotalPartialCharge1", "protonTotalPartialCharge2", "protonTotalPartialCharge3", "protonTotalPartialCharge4", "protonTotalPartialCharge5", "g3r_1", "g3r_2", "g3r_3", "g3r_4", "g3r_5", "g3r_6", "g3r_7", "g3r_8", "g3r_9", "g3r_10", "g3r_11", "g3r_12", "g3r_13", "gDr_1", "gDr_2", "gDr_3", "gDr_4", "gDr_5", "gDr_6", "gDr_7", "RDF_GHR_0", "RDF_GHR_1", "RDF_GHR_2", "RDF_GHR_3", "RDF_GHR_4", "RDF_GHR_5", "RDF_GHR_6", "RDF_GHR_7", "RDF_GHR_8", "RDF_GHR_9", "RDF_GHR_10", "RDF_GHR_11", "RDF_GHR_12", "RDF_GHR_13", "RDF_GHR_14", "gHrTop_1", "gHrTop_2", "gHrTop_3", "gHrTop_4", "gHrTop_5", "gHrTop_6", "gHrTop_7", "gHrTop_8", "gHrTop_9", "gHrTop_10", "gHrTop_11", "gHrTop_12", "gHrTop_13", "gHrTop_14", "gHrTop_15", "gSr_1", "gSr_2", "gSr_3", "gSr_4", "gSr_5", "gSr_6", "gSr_7", "elecSigmA", "stabilPlusC", "vdwRadius", "ipAtomicLearning", "aNeg", "aHyb", "hybr", "val", "bondsToAtom", "covalentRadius", "distanceToAtom", "effAtomPol", "indAtomHardnesss", "indAtomSoftness", "ipAtomicHOSE", "protonInArmaticSystem", "protonInConjSystem", "pepe", "partialSigmaCharge", "partialTCMMFF94", "pepeT", "periodicTablePosition", "elecPiA", "protonAffiHOSE", "protonTotalPartialCharge1", "protonTotalPartialCharge2", "protonTotalPartialCharge3", "protonTotalPartialCharge4", "protonTotalPartialCharge5", "g3r_1", "g3r_2", "g3r_3", "g3r_4", "g3r_5", "g3r_6", "g3r_7", "g3r_8", "g3r_9", "g3r_10", "g3r_11", "g3r_12", "g3r_13", "gDr_1", "gDr_2", "gDr_3", "gDr_4", "gDr_5", "gDr_6", "gDr_7", "RDF_GHR_0", "RDF_GHR_1", "RDF_GHR_2", "RDF_GHR_3", "RDF_GHR_4", "RDF_GHR_5", "RDF_GHR_6", "RDF_GHR_7", "RDF_GHR_8", "RDF_GHR_9", "RDF_GHR_10", "RDF_GHR_11", "RDF_GHR_12", "RDF_GHR_13", "RDF_GHR_14", "gHrTop_1", "gHrTop_2", "gHrTop_3", "gHrTop_4", "gHrTop_5", "gHrTop_6", "gHrTop_7", "gHrTop_8", "gHrTop_9", "gHrTop_10", "gHrTop_11", "gHrTop_12", "gHrTop_13", "gHrTop_14", "gHrTop_15", "gSr_1", "gSr_2", "gSr_3", "gSr_4", "gSr_5", "gSr_6", "gSr_7", "elecSigmA", "stabilPlusC", "vdwRadius", "ipAtomicLearning", "aNeg", "aHyb", "hybr", "val", "bondsToAtom", "covalentRadius", "distanceToAtom", "effAtomPol", "indAtomHardnesss", "indAtomSoftness", "ipAtomicHOSE", "protonInArmaticSystem", "protonInConjSystem", "pepe", "partialSigmaCharge", "partialTCMMFF94", "pepeT", "periodicTablePosition", "elecPiA", "protonAffiHOSE", "protonTotalPartialCharge1", "protonTotalPartialCharge2", "protonTotalPartialCharge3", "protonTotalPartialCharge4", "protonTotalPartialCharge5", "g3r_1", "g3r_2", "g3r_3", "g3r_4", "g3r_5", "g3r_6", "g3r_7", "g3r_8", "g3r_9", "g3r_10", "g3r_11", "g3r_12", "g3r_13", "gDr_1", "gDr_2", "gDr_3", "gDr_4", "gDr_5", "gDr_6", "gDr_7", "RDF_GHR_0", "RDF_GHR_1", "RDF_GHR_2", "RDF_GHR_3", "RDF_GHR_4", "RDF_GHR_5", "RDF_GHR_6", "RDF_GHR_7", "RDF_GHR_8", "RDF_GHR_9", "RDF_GHR_10", "RDF_GHR_11", "RDF_GHR_12", "RDF_GHR_13", "RDF_GHR_14", "gHrTop_1", "gHrTop_2", "gHrTop_3", "gHrTop_4", "gHrTop_5", "gHrTop_6", "gHrTop_7", "gHrTop_8", "gHrTop_9", "gHrTop_10", "gHrTop_11", "gHrTop_12", "gHrTop_13", "gHrTop_14", "gHrTop_15", "gSr_1", "gSr_2", "gSr_3", "gSr_4", "gSr_5", "gSr_6", "gSr_7", "elecSigmA", "stabilPlusC", "vdwRadius", "ipAtomicLearning"]
HEADER2 =  ["Benzene ring", "Carbonyl", "Carbonyl with aromatic", "COO-", "CH3 with aromatin +n", "CHCOOH", "Carboxylic acid", "Carboxylic acid with aromatic", "CHNHC=O", "CH2CONH", "Aldehyde", "Aldehyde with aromatic", "Alkene without explicit H", "Alkyne", "Halozens", "Halozens with aromatic", "CH2", "CH", "CH3 with N", "CH3 with aromatic", "Hydroxyl", "Hydroxyl with aromatic", "NH", "NH2", "NH3", "N", "Amide, NH2C=O", "CH2NH", "NHCO", "NNO", "Amino acid", "CNNN", "double bond O as aromatic branch", "PO4", "PO4-", "benzene ring with N+", "pyridine, benzene ring with N", "benzene ring with N =O", "Two N in ring, aromatic", "Two N in ring, double aromatic", "Two N in aromatic with =O and -NH2", "Two N in aromatic with 2 =O", "Two N in aromatic with single =O", "Two N in 5 ring", "two ring, benzene with 2N and O, 2N in 5ring", "Imidazole, aromatic 5 rign with two nitrozens", "O inside 5 ring", "O inside 5 ring with 3 =O", "O inside 5 ring with two OH", "O inside 6 ring", "O inside 6 ring with =O", "Furan, 5 ring aromatic with O", "5 ring with one nitroen", "5 ring with N+ S", "5 ring with S,5 ring with 2 N", "S in a ring", "N in 6 ring", "N in 5 ring", "glucose", "ribose", "sugar1", "sugar2", "6 ring with one double bond", "6 ring without any double bond", "6 ring without any double bond with O", "infused benzene ring, one ring with nitrozen", "infused benzene ring, one ring with nitrozen and O", "infused benzene ring, 5 ring with 2N, 6 ring with 2N", "infused benzene ring, 5 ring with 2N, 6 ring with 2N O1", "infused benzene ring", "Nitrate Ester 1", "oxygen with C and P", "Ester", "HMDB640", "O=S=O", "6 ring with two O", "C-S-C", "CH2NH2", "SCH3", "CH3 with N+", "methane", "infused benzene ring, both with 2N", "connected with aromatic atom", "aliphatic ring atom", "n and s in a 5 ring", "SO3", "PO3", "C triple bond N", "C(OH)2", "chirality", "ProChiralH", "DistanceToChiralCentre", "ChemicalShift", "HMDBID", "HPosition" ]
HEADER = HEADER1 + HEADER2
# print("header = {}".format(HEADER))
# print("length header={}".format(len(HEADER)))


# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/features/training_new_feature_3near_fp_one_hot_most_updated.csv', 'w') as csv_file:
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/features/holdout_new_feature_3near_fp_one_hot.csv', 'w') as csv_file: #change path and file name here too
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/Holdout_new_feature_3near_fp_one_hot_35holdout_most_updated.csv', 'w') as csv_file: #change path and file name here too
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/train_chiral_new_code.csv', 'w') as csv_file: 
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/holdout_chiral_new_code.csv', 'w') as csv_file: 
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/train_chiral_pror_pros_gissmo.csv', 'w') as csv_file: 
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/train_chiral_prochiral_distancetochiral_bmrb_hmdb.csv', 'w') as csv_file: #########  uncomment when parsing for tainset
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/holdout_chiral_prochiral_distancetochiral_bmrb_hmdb_2.csv', 'w') as csv_file:  ###### comment out when parsing for tainset
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/holdout_hmdb_onlybmrb_swapped_COH2.csv', 'w') as csv_file:  ###### comment out when parsing for tainset
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/train_hmdb_onlybmrb_swapped_COH2.csv', 'w') as csv_file:  #########  uncomment when parsing for tainset
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92.csv', 'w') as csv_file:  #########  uncomment when parsing for tainset
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon.csv', 'w') as csv_file:  #########  uncomment when parsing for tainset
with open('/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2.csv', 'w') as csv_file:  #########  uncomment when parsing for tainset
# with open('/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2.csv', 'w') as csv_file:  #########  uncomment when parsing for tainset
# with open('/Users/zinatsayeeda/anaconda3/envs/rdkit/GISSMO/data_handling/holdout_hmdb_onlybmrb_swapped_COH2_fixed_7point92.csv', 'w') as csv_file:  #########  uncomment when parsing for holdout  
# with open('/Volumes/Samsung_T5/Mac_Contents/rdkit/GISSMO/data_handling/holdout_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2.csv', 'w') as csv_file:  #########  uncomment when parsing for holdou  
  writer = csv.writer(csv_file)
  writer.writerow(HEADER)


  for key,file_name in file_names.iteritems():
    for single_file in file_name:
      print("single_file = {}".format(single_file))
      print("key={}".format(key))
      if key == "file_name_hmdb":
        # single_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/features/"+single_file
        # single_file_with_path = "/Volumes/Zinat_MAC_Backup/backup_get_descriptor_two/get_descriptor/Xuan_new_descriptor/Latest/features/"+single_file
        single_file_with_path = "/Volumes/Samsung_T5/Timemachine/backup_get_descriptor_two/get_descriptor/Xuan_new_descriptor/Latest/features/"+single_file
      
      if key == "file_name_gissmo":
        sp = single_file.split("/")[-2]
        # single_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/features/"+sp
        single_file_with_path = "/Volumes/Samsung_T5/Timemachine/backup_get_descriptor_two/get_descriptor/Xuan_new_descriptor/Latest/features/"+sp
      '''
      if key == "file_name_gissmo_uncommon":
        print("reached uncommon")
        # single_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/features/"+sp
        # single_file_with_path = "/Volumes/Zinat_MAC_Backup/backup_get_descriptor_two/get_descriptor/Xuan_new_descriptor/Latest/features/"+single_file
        single_file_with_path = "/Volumes/Samsung_T5/Timemachine/backup_get_descriptor_two/get_descriptor/Xuan_new_descriptor/Latest/features/"+single_file
      '''
      print("Found the single file={}".format(single_file_with_path))
      
      if os.path.isfile(single_file_with_path):
        with open(single_file_with_path) as json_file:
          data = json.load(json_file)
          jdata = ast.literal_eval(json.dumps(data))
          data_row_hash_with_h_positions = jdata['Descriptors']
          
          if key == "file_name_hmdb":
            hmdb_id = single_file
            print("HMDBID={}".format(hmdb_id))
          
          if key == "file_name_gissmo":
            hmdb_id = single_file.split("/")[-2]
            print("HMDBID={}".format(hmdb_id))
          '''
          if key == "file_name_gissmo_uncommon":
            hmdb_id = single_file
            print("HMDBID={}".format(hmdb_id))
          '''
          hydrogen_position = [int(m) for m in data_row_hash_with_h_positions.keys()]
          print("hydrogen position={}".format(hydrogen_position))
        
          if key == "file_name_hmdb":
            # chem_file_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+single_file # change the file path here
            # chem_file_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file # change the file path here
            # chem_file_path = "/Volumes/Samsung_T5/Mac_Contents/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file # change the file path here
            # chem_file_path = "/Volumes/Zinat_MAC_Backup/backup_dataset/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+single_file
            chem_file_path = "/Volumes/Samsung_T5/Timemachine/backup_dataset/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+single_file
            # sdf_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/Samsung_T5/Mac_Contents/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/Zinat_MAC_Backup/backup_dataset/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+single_file+".sdf" #change the path here
            sdf_file_with_path = "/Volumes/Samsung_T5/Timemachine/backup_dataset/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+single_file+".sdf" #change the path here
          
          if key == "file_name_gissmo":
            # chem_file_path = single_file+"chemicalshift_water_final" # change the file path here
            # chem_file_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file # change the file path here
            # chem_file_path = "/Volumes/Zinat_MAC_Backup/GISSMO_With_data_handling/GISSMO/"+single_file.split("/")[-2]+"/chemicalshift_water_final" # change the file path here
            chem_file_path = "/Volumes/Samsung_T5/Timemachine/GISSMO_With_data_handling/GISSMO/"+single_file.split("/")[-2]+"/chemicalshift_water_final" # change the file path here
            # sdf_file_with_path = single_file+"compound1.sdf" #change the path here
            # sdf_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/Zinat_MAC_Backup/GISSMO_With_data_handling/GISSMO/"+single_file.split("/")[-2]+"/compound1.sdf" #change the path here
            sdf_file_with_path = "/Volumes/Samsung_T5/Timemachine/GISSMO_With_data_handling/GISSMO/"+single_file.split("/")[-2]+"/compound1.sdf" #change the path here
          '''
          if key == "file_name_gissmo_uncommon":
            # chem_file_path = single_file+"chemicalshift_water_final" # change the file path here
            # chem_file_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file # change the file path here
            # chem_file_path = "/Volumes/Zinat_MAC_Backup/GISSMO_With_data_handling_only_gissmo/GISSMO/"+single_file+"/chemicalshift_water_final" # change the file path here
            chem_file_path = "/Volumes/Samsung_T5/Timemachine/GISSMO_With_data_handling_only_gissmo/GISSMO/"+single_file+"/chemicalshift_water_final" # change the file path here
            # sdf_file_with_path = single_file+"compound1.sdf" #change the path here
            # sdf_file_with_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"+single_file+".sdf" #change the path here
            # sdf_file_with_path = "/Volumes/Zinat_MAC_Backup/GISSMO_With_data_handling_only_gissmo/GISSMO/"+single_file+"/compound1.sdf" #change the path here
            sdf_file_with_path = "/Volumes/Samsung_T5/Timemachine/GISSMO_With_data_handling_only_gissmo/GISSMO/"+single_file+"/compound1.sdf" #change the path here
          '''
          print("chem file path ={}".format(chem_file_path))
          H_position_and_chemicalshift_in_shift_file = ReadShiftFile1(chem_file_path,hydrogen_position)

  
          for i in hydrogen_position:
            data_row = data_row_hash_with_h_positions[str(i)]
            h_p = i
            for d in H_position_and_chemicalshift_in_shift_file:
              if  d["H_position"] == h_p:
                # a = fingerprint.OneBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                #   data_row.append(a[i])
                # a = fingerprint.TwoBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                #   data_row.append(a[i])
                # a = fingerprint.ThreeBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                # for i in range(len(a)):
                #   data_row.append(a[i])
                a = fingerprint.FourBondAwayFP(int(d["H_position"]),sdf_file_with_path)
                for i in range(len(a)):
                  data_row.append(a[i])
                neares_atoms_for_h_p = jdata["AtomNearestAtoms"][str(d["H_position"])]
                nearest_three_atom = neares_atoms_for_h_p[0:3]
                nearest_three_atom.append(int(d["H_position"])) # added the atom itself together with it's nearest three atoms

                # chirality = chiral.chiraltag(sdf_file_with_path)
                chirality = chiral.is_mol_chiral(sdf_file_with_path)
                data_row.append(chirality)
  
                # ProChiralH = chiral.is_prochiral_H(sdf_file_with_path,int(h_p))
                ProChiralH = chiral.ChiralCH2(sdf_file_with_path,int(h_p))
                # if ProChiralH == 1 or ProChiralH == 2:
                #   data_row.append(1)
                # else:
                #   data_row.append(0)
                data_row.append(ProChiralH)

                # distance_from_chiral_centre = chiral.dist_from_chiral(sdf_file_with_path,int(h_p))
                distance_from_chiral_centre = chiral.dist_from_chiral(sdf_file_with_path,int(h_p))
                data_row.append(distance_from_chiral_centre)
                
                atoms_aromacity = chiral.aromaticAtom(nearest_three_atom,sdf_file_with_path)
                # for l in range(len(atoms_aromacity)):
                data_row[11] = atoms_aromacity[3]
                data_row[97] = atoms_aromacity[0]
                data_row[183] = atoms_aromacity[1]
                data_row[269] = atoms_aromacity[2]
  
                data_row.append(d["chemical_shift"])
                data_row.append(hmdb_id)
                data_row.append(d["H_position"])
                print("data row={}".format(data_row))
                print("length of data row={}".format(len(data_row)))
            writer.writerow(data_row)




