import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.metrics import mean_absolute_error
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
import copy
from sklearn.metrics import mean_squared_error,r2_score
import seaborn as sns
import plotly
from plotly import figure_factory as FF
# import chart_studio.plotly as py
import plotly.graph_objs as go
from decimal import *
# import pickle
import os, sys
import requests
import urllib, json, csv
from fppackage import chiral_v2_CH2 as chiral
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import rdMolTransforms
import pandas as pd
import csv, math
import pickle
import os, sys
import urllib, json, csv
import csv, math
import sklearn.externals
import joblib
from xgboost import XGBRFRegressor




# f = f[f['HMDBID'].str.match('HMDB')]

def main():
    
    # trainingSetFile = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null.csv"
    #trainingSetFile = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_delete_4_features_no_null.csv"
    trainingSetFile = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/train_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_with_nan.csv"
    # testData  = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/holdout_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2_no_null.csv"
    #testData  = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/holdout_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2_delete_4_features_no_null.csv"
    testData =  "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2_no_null_with_nan.csv"
    # preparation for Final training ##
    dataset = pd.read_csv(trainingSetFile)
    print("dataset shape:{}".format(dataset.shape))
    dataset = dataset.drop(columns=['protonTotalPartialCharge2', 'protonTotalPartialCharge3', 'protonTotalPartialCharge4', 'protonTotalPartialCharge5','protonTotalPartialCharge2.1', 'protonTotalPartialCharge3.1', 'protonTotalPartialCharge4.1', 'protonTotalPartialCharge5.1','protonTotalPartialCharge2.2', 'protonTotalPartialCharge3.2', 'protonTotalPartialCharge4.2', 'protonTotalPartialCharge5.2','protonTotalPartialCharge2.3', 'protonTotalPartialCharge3.3', 'protonTotalPartialCharge4.3', 'protonTotalPartialCharge5.3'])
    dataset = dataset.drop(columns=['pepe', 'pepe.1', 'pepe.2', 'pepe.3','ipAtomicLearning', 'ipAtomicLearning.1', 'ipAtomicLearning.2', 'ipAtomicLearning.3'])
    dataset = dataset.drop(columns=["aHyb", "val"])
    dataset = dataset.drop(columns=['Unnamed: 0'])
    # dataset = dataset.drop(columns=['Unnamed: 0', 'chirality', 'ProChiralH', 'DistanceToChiralCentre'])
    print("dataset shape after dropping partial charge:{}".format(dataset.shape))
    # dataset = dataset.dropna(axis=1,how='all')
    # print("dataset shape after dropping Nan columns:{}".format(dataset.shape))
    # dataset = dataset.dropna(subset=['HMDBID'])
    # dataset.fillna(999.0, inplace=True)
    # dataset[dataset.columns[0:-34]] = dataset[dataset.columns[0:-34]].astype(float)

    ### dropping the holdout data from BMRB/GISSMO datatset ###
    dataset = dataset[~dataset['HMDBID'].isin(['bmse000300_simulation_1_nmredata','bmse000322_simulation_1_nmredata','bmse000330_simulation_1_nmredata','bmse000184_simulation_1_nmredata','bmse000389_simulation_1_nmredata','bmse000435_simulation_1_nmredata','bmse000856_simulation_1_nmredata','bmse000364_simulation_1_nmredata','bmse000337_simulation_1_nmredata','bmse000044_simulation_1_nmredata','bmse000915_simulation_1_nmredata','bmse000404_simulation_1_nmredata'])]
    print("dataset shape after dropping common holdout:{}".format(dataset.shape))
    #################################



    hmdb_ids_training = dataset.iloc[ :,-2].values
    hmdb_ids_training = hmdb_ids_training.reshape(hmdb_ids_training.shape[0],1)
    hydrogen_positions_training = dataset.iloc[ :,-1].values
    hydrogen_positions_training = hydrogen_positions_training.reshape(hydrogen_positions_training.shape[0],1)
    print("nan value: {}".format(dataset.isnull().sum()))

    X_training = dataset.iloc[ : , 0:-3].values
    # dup = X_training[ :, -31:]
    # for i in range(dup.shape[0]):
    #   for j in range(dup.shape[1]):
    #     dup[i][j] = Decimal(dup[i][j])
    # X_training[ :, -31:] = dup
    # print("X_training data type 274:{}".format(type(X_training[0][274])))
    X_training = X_training.astype(float)

    y_training =  dataset.iloc[ :,-3].values
    y_training = y_training.reshape(y_training.shape[0],1)
    y_training = y_training.astype(float)
    Y_training = y_training
    #Y_training = classLabel(y_training)
    # print("Y after classLabel:{}".format(Y))
    # Y_training = Y_training.reshape(Y_training.shape[0],1)
    # Y_training = Y_training.astype(int)

    # print("Y_training after classLabel:{}".format(Y_training))
    # Y_training = Y_training.reshape(Y_training.shape[0],1)
    # Y_training = Y_training.astype(int)

    ################
    ###############

    # checking the CV results with final parameter
    folds = 5
    cv = KFold(n_splits=folds, random_state=1, shuffle=True)

    file_name_group_by_panda_series = dataset.groupby('HMDBID')
    file_name_group_by_dict = dict(list(file_name_group_by_panda_series))
    file_name = np.array(list(file_name_group_by_dict.keys()))
    file_name = file_name.reshape(file_name.shape[0],1)

    cv_scores = []
    cv_scores_train = []
    cv_scores_swapped = []
    cv_scores_swapped_train = []
    cv_scores_mse = []
    cv_scores_mse_train = []
    cv_scores_mse_swap = []
    cv_scores_mse_swapped_train = []
    r2_scores = []
    r2_scores_train = []
    r2_scores_swapped = []
    r2_scores_swapped_train = []

    HEAD = ["MoleculeId", "HPosition", "TrueVal", "PredictedVal", "Fold"]

    # with open("/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold_600_10_3molecule_xuan_new_descriptor_all_features_2/1200_70_hmdb_onlybmrb_swapped_COH2_fixed_7point92_without_uncommon_no_null/outliers_1200_70_hmdb_onlybmrb_swapped_COH2_fixed_7point92_without_uncommon_no_null.csv", "w") as csv_file:
    # with open("/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold_600_10_3molecule_xuan_new_descriptor_all_features_2/800_40_hmdb_onlybmrb_swapped_COH2_fixed_7point92_with_uncommon_consistentCH2_no_null_3/outliers_700_50_hmdb_onlybmrb_swapped_COH2_fixed_7point92_consistentCH2_no_null.csv", "w") as csv_file:
    with open("/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/outliers.csv", "w") as csv_file:

        writer = csv.writer(csv_file)
        writer.writerow(HEAD)

        for (inner_train_index_filename, inner_test_index_filename), j in zip(cv.split(file_name), range(folds)):
    
           file_name_inner = []
#
           file_name_inner_train = file_name[inner_train_index_filename]
           file_name_inner_test = file_name[inner_test_index_filename]
    
           X_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
           X_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
    
           X_train_inner_temp = dataset[X_train_inner_new_filter] #this is always the datset format after cv
           X_test_inner_temp = dataset[X_test_inner_new_filter] #this is always the datset format after cv
           y_train_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_train))
           y_test_inner_new_filter = dataset["HMDBID"].isin(np.ravel(file_name_inner_test))
    
           y_train_inner_temp = dataset[y_train_inner_new_filter] #this is always the datset format after cv
           y_test_inner_temp = dataset[y_test_inner_new_filter] #this is always the datset format after cv
    
           X_train_inner = X_train_inner_temp.drop(columns=["ChemicalShift","HMDBID","HPosition"])
           X_train_inner = X_train_inner.iloc[: , :].values 
           # print("X_train_inner data type 274:{}".format(type(X_train_inner[0][274])))
           X_train_inner = X_train_inner.astype(float)
    
           X_test_inner = X_test_inner_temp.drop(columns=["ChemicalShift","HMDBID","HPosition"])
           X_test_inner = X_test_inner.iloc[: , :].values 
           # print("X_test_inner data type 274:{}".format(type(X_test_inner[0][274])))
           X_test_inner = X_test_inner.astype(float)
    
           y_train_inner = y_train_inner_temp["ChemicalShift"].values
           y_train_inner = y_train_inner.astype(float)
           y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
           # y_train_inner = classLabel(y_train_inner)
           # y_train_inner = y_train_inner.reshape(y_train_inner.shape[0],1)
           # y_train_inner = y_train_inner.astype(int)
    
           y_test_inner = y_test_inner_temp["ChemicalShift"].values
           y_test_inner = y_test_inner.astype(float)
           y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)
           # y_test_inner = classLabel(y_test_inner)
           # y_test_inner = y_test_inner.reshape(y_test_inner.shape[0],1)
           # y_test_inner = y_test_inner.astype(int)
    
           hmdb_ids_inner_fold_train = X_train_inner_temp["HMDBID"].values
           hmdb_ids_inner_fold_train = hmdb_ids_inner_fold_train.reshape(hmdb_ids_inner_fold_train.shape[0],1)
    
           hmdb_ids_inner_fold_test = X_test_inner_temp["HMDBID"].values
           hmdb_ids_inner_fold_test = hmdb_ids_inner_fold_test.reshape(hmdb_ids_inner_fold_test.shape[0],1)
           hmdb_ids_inner_fold_test = np.ravel(hmdb_ids_inner_fold_test)
#
#
           file_name_group_by_panda_series_inner = X_test_inner_temp.groupby('HMDBID')
           file_name_group_by_dict_inner = dict(list(file_name_group_by_panda_series_inner))
           file_name_inner = np.array(list(file_name_group_by_dict_inner.keys()))
           file_name_inner = file_name_inner.reshape(file_name_inner.shape[0],1)  # this is crossval holdout file names unique
           file_name_inner = np.ravel(file_name_inner)
           print("unique inner file name to be predicted:{}".format(file_name_inner))
    
    
           hydrogen_positions_inner_fold_train = X_train_inner_temp["HPosition"].values
           hydrogen_positions_inner_fold_train = hydrogen_positions_inner_fold_train.reshape(hydrogen_positions_inner_fold_train.shape[0],1)
    
           hydrogen_positions_inner_fold_test = X_test_inner_temp["HPosition"].values
           hydrogen_positions_inner_fold_test = hydrogen_positions_inner_fold_test.reshape(hydrogen_positions_inner_fold_test.shape[0],1)
           
           # model_inner = RandomForestClassifier(n_estimators=500,max_depth=50)
           # model_inner = RandomForestClassifier(n_estimators=800,max_depth=40)
           # model_inner = RandomForestClassifier(n_estimators=600,max_depth=95)
           model_inner = XGBRFRegressor(n_estimators= 600, max_depth= 25, subsample=0.9, colsample_bynode=0.05) # put the parameter values here
           # model_inner = RandomForestClassifier(n_estimators=700,max_depth=50)
           # model_inner = RandomForestClassifier(n_estimators=1200,max_depth=70)
           # model_inner = RandomForestClassifier(n_estimators=700,max_depth=95)
           # model_inner = RandomForestClassifier(n_estimators=500,max_depth=55)
           # model_inner = RandomForestClassifier(n_estimators=600,max_depth=10)
           # model_inner = RandomForestClassifier(n_estimators=500,max_depth=70)
           y_train_inner = np.ravel(y_train_inner)
           # print("y_train_inner:{}".format(y_train_inner))
           model_inner.fit(X_train_inner, y_train_inner)
           y_pred_inner = model_inner.predict(X_test_inner)
           y_pred_inner = y_pred_inner.astype(float)
           # y_pred_inner = y_pred_inner/float(100)
           y_test_inner = y_test_inner.astype(float)
           # y_test_inner = y_test_inner/float(100)
           y_test_inner = np.ravel(y_test_inner)
           print("in fold {} shape of X_train_inner = {} and shape of y_train_inner ={}".format(j,X_train_inner.shape,y_train_inner.shape))
           meanAbsError_inner = mean_absolute_error(y_test_inner, y_pred_inner) # sum up of all the mae of y_pred and y_test
           cv_scores.append(meanAbsError_inner)

           r2 = r2_score(y_test_inner, y_pred_inner)
           r2_scores.append(r2)
           # print("y_test_inner:{} and y_pred_inner:{}".format(y_test_inner,y_pred_inner))

           mSError_inner = mean_squared_error(y_test_inner, y_pred_inner)
           # mSError_inner = mean_squared_error(y_test_inner_original, y_pred_inner_original)
           cv_scores_mse.append(mSError_inner)

           y_pred_inner_for_fold_train = model_inner.predict(X_train_inner)
           y_pred_inner_for_fold_train = y_pred_inner_for_fold_train.astype(float)
            # y_pred_inner_for_fold_train_original = (y_pred_inner_for_fold_train*Y_scaled_scale)+Y_scaled_mean
            # y_train_inner_original = (y_train_inner*Y_scaled_scale)+Y_scaled_mean

           meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner, y_pred_inner_for_fold_train) 
            # meanAbsError_inner_fold_pred_for_train = mean_absolute_error(y_train_inner_original, y_pred_inner_for_fold_train_original) 
           cv_scores_train.append(meanAbsError_inner_fold_pred_for_train)
    
           mSError_inner_fold_pred_for_train = mean_squared_error(y_train_inner, y_pred_inner_for_fold_train) 
           cv_scores_mse_train.append(mSError_inner_fold_pred_for_train)

           r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner, y_pred_inner_for_fold_train)
            # r2_scores_inner_fold_pred_for_train = r2_score(y_train_inner_original, y_pred_inner_for_fold_train_original)
           r2_scores_train.append(r2_scores_inner_fold_pred_for_train)



    
           print("started prediction for fold:{}".format(j))
           for k in range(len(y_pred_inner)):
#
               datarow = []
               print("Printing  prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred_inner[k], y_test_inner[k],hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k]))
               if float(abs(y_pred_inner[k]-y_test_inner[k])) >= 1.0 :
                   datarow.append(hmdb_ids_inner_fold_test[k])
                   datarow.append(hydrogen_positions_inner_fold_test[k][0])
                   datarow.append(y_test_inner[k])
                   datarow.append(y_pred_inner[k])
                   datarow.append(j+1)
                   writer.writerow(datarow)
                   print("outliers{} {}".format(hmdb_ids_inner_fold_test[k],hydrogen_positions_inner_fold_test[k][0]))
    
           print("for th:{} cv  MAE  is:{} and r2_score:{} ".format(j,cv_scores[j],r2_scores[j]))
#
#
           #### after swap MAE calculation ##### ########################
           #############################################################
           for m in file_name_inner:
#
               print("started filename m = {}".format(m))
               hold_out_prediction_file_name_before_swap = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/fold"+str(j+1)+"/"+m
               hold_out_prediction_file_before_swap = open(hold_out_prediction_file_name_before_swap,"w")
               hold_out_prediction_file_before_swap.write("Table\nPos\tPred\tTest")
#
               index_of_file_name_inner = np.where(hmdb_ids_inner_fold_test == m)
               index_of_file_name_inner = index_of_file_name_inner[0]
               print("index_of_file_name_inner:{}".format(index_of_file_name_inner))
#
               pred_test_values_per_file = {}
#
               control_var = 0
               for n in index_of_file_name_inner:
                   pred_test_values_per_file[str(y_pred_inner[n])+"_"+str(y_test_inner[n])+"_"+str(control_var)] = int(hydrogen_positions_inner_fold_test[n][0])
                   print("hydrogen_positions_inner_fold_test[n][0]={}".format(int(hydrogen_positions_inner_fold_test[n][0])))
                   control_var = control_var + 1
#
               print("pred_test_values_per_file={}".format(pred_test_values_per_file))
               sorted_pred_test_values_per_file = sorted(pred_test_values_per_file.items(), key=lambda x: x[1])
               print("sorted vlues for file{} = {}".format(m,sorted_pred_test_values_per_file))
#
               for v in sorted_pred_test_values_per_file:
#
                   hold_out_prediction_file_before_swap.write("\n"+str(v[1])+"\t"+str(v[0].split("_")[0])+"\t"+str(v[0].split("_")[1]))
               hold_out_prediction_file_before_swap.close()
#
#
               if "bmse" in m.lower():
                   f_sdf_path = "/Volumes/Zinat_MAC_Backup/GISSMO_With_data_handling/GISSMO/"+m.split("GISSMO")[-1]+"/compound1.sdf"
                   # f_chem_path = "/Volumes/Samsung_T5/Timemachine/GISSMO_With_data_handling/GISSMO/"+m.split("GISSMO")[-1]+"/chemicalshift_water_final"
#
               if "hmdb" in m.lower():
                   f_sdf_path = "/Volumes/Zinat_MAC_Backup/backup_dataset/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+m+".sdf" #change the path here
                   # f_chem_path = "/Volumes/Samsung_T5/Timemachine/backup_dataset/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"+m #change the path here
#
#
               mol = Chem.MolFromMolFile(f_sdf_path,removeHs=False)
               # mol = Chem.AddHs(mol)
               # Chem.EmbedMolecule(mol)
               # Chem.MMFFOptimizeMolecule(mol)
#
#
               Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
               match = Chem.MolFromSmarts("[CX4H2]")  # for CH2
               # match = Chem.MolFromSmarts("[CX3;R0]=[CX3;R0]") # for alkene
#
               h_pos_per_file = []
               tag_per_file = []
               match_atoms = mol.GetSubstructMatches(match)
               if match_atoms:
                   #collect tags and swap the file:
                   for a in match_atoms:
                        # for each "C" the ids of "H are stored"
                       print("a = {}".format(a[0]))
                       tags_per_neigh = []
                       centre_atom = mol.GetAtomWithIdx(a[0])
                       neigh_atoms = centre_atom.GetNeighbors()
                       idx_per_neigh = []
                       for nt in neigh_atoms:
                           idx_temp = nt.GetIdx()
                           sym = nt.GetSymbol()
                           if sym != "H": 
                               continue
                           else:
                               idx_per_neigh.append(idx_temp)
                               tags_per_neigh.append(chiral.ChiralCH2(f_sdf_path,idx_temp))
                               print("tags_per_neigh={}".format(tags_per_neigh))
                       if (len(set(tags_per_neigh)) == 1) or ( 3 in tags_per_neigh ) or ( 4 in tags_per_neigh ) or ( 0 in tags_per_neigh ):
                           print("no change required in the atom")
                       else:
                           print("change required for:{}   tag:{}".format(idx_per_neigh,tags_per_neigh))
                           for ids in range(len(idx_per_neigh)):
                               h_pos_per_file.append(idx_per_neigh[ids])
                               tag_per_file.append(tags_per_neigh[ids])
#
               print("for file {} h_pos= {} tags={}".format(m,h_pos_per_file,tag_per_file))
#
               # hydro, shift = ReadShiftFile1(hold_out_prediction_file_name_before_swap)
               hold_out_prediction_file_name_after_swap = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/fold"+str(j+1)+"/"+m+"_after_swap"
               hold_out_prediction_file_after_swap = open(hold_out_prediction_file_name_after_swap,"w")
               hold_out_prediction_file_after_swap.write("Table\nPos\tPred\tTest")
               hydro, pred_shift, test_shift = ReadShiftFile1(hold_out_prediction_file_name_before_swap)
               print("before swap, retrieved values for hydro={}, pred_shift={}, test-shift={}".format(hydro,pred_shift,test_shift))
               print("len of H position per file={}".format(len(h_pos_per_file)))
#
#
               if len(h_pos_per_file) >= 1:
                   #change the file
#
                   for hy in range(0,len(h_pos_per_file),2):
                       print("hy={}".format(hy))
                       if hy != len(h_pos_per_file)-1:
                           position1 = h_pos_per_file[int(hy)]
                           position2 = h_pos_per_file[int(hy)+1]
                           tag1 = tag_per_file[int(hy)]
                           tag2 = tag_per_file[int(hy)+1]
                           index_of_greater_tag = None
                           index_of_lower_tag = None
                           if tag1 > tag2:
                               index_of_greater_tag = int(hy)
                               index_of_lower_tag = int(hy)+1
                           else:
                               index_of_greater_tag = int(hy)+1
                               index_of_lower_tag = int(hy)
                           ## decide which H position should have greater value    
                           h_pos_for_greater_value = h_pos_per_file[index_of_greater_tag]
                           h_pos_for_lower_value = h_pos_per_file[index_of_lower_tag]
#
                           if (h_pos_for_greater_value in hydro) and (h_pos_for_lower_value in hydro):
                               index_of_greater_hydrogen_in_before_swap_file = hydro.index(h_pos_for_greater_value)
                               index_of_lower_hydrogen_in_before_swap_file = hydro.index(h_pos_for_lower_value)
#
                               greater_hydrgen_predicted_value = pred_shift[index_of_greater_hydrogen_in_before_swap_file]
                               lower_hydrgen_predicted_value = pred_shift[index_of_lower_hydrogen_in_before_swap_file]
#
                               if greater_hydrgen_predicted_value >= lower_hydrgen_predicted_value:
                                   print("value already greater")
                               else:
                                   print("value not greater")
                                   pred_shift[index_of_greater_hydrogen_in_before_swap_file] = lower_hydrgen_predicted_value
                                   pred_shift[index_of_lower_hydrogen_in_before_swap_file] = greater_hydrgen_predicted_value
#
#
               else:
                   # keep the file same
                   print("no change required in the file")
               for i in range(len(hydro)):
                   hold_out_prediction_file_after_swap.write("\n"+str(hydro[i])+"\t"+str(pred_shift[i])+"\t"+str(test_shift[i]))
               hold_out_prediction_file_after_swap.close()
#
           # calculate new MAE
           y_pred_swap = []
           y_test_swap = []
           hmdb_id_swap = []
           hydro_pos_swap = []
           for single_file in file_name_inner:
               swap_chemical_path = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/fold"+str(j+1)+"/"+single_file+"_after_swap"
               pos,pred,test = ReadShiftFile1(swap_chemical_path)
               for r in range(len(pos)):
                   y_pred_swap.append(pred[r])
                   y_test_swap.append(test[r])
                   hmdb_id_swap.append(single_file)
                   hydro_pos_swap.append(pos[r])
           
           meanAbsError_swapped = mean_absolute_error(y_test_swap, y_pred_swap) # sum up of all the mae of y_pred and y_test
           cv_scores_swapped.append(meanAbsError_swapped)
           print("in j = {} cv_scores_swapped = {}".format(j,cv_scores_swapped))

           r2_swap = r2_score(y_test_swap, y_pred_swap)
           r2_scores_swapped.append(r2_swap)
           print("in j = {} r2_scores_swapped = {}".format(j,r2_scores_swapped))

           
           mSError_inner_swap = mean_squared_error(y_test_swap, y_pred_swap)
           # mSError_inner = mean_squared_error(y_test_inner_original, y_pred_inner_original)
           cv_scores_mse_swap.append(mSError_inner_swap)
           print("in j = {} cv_scores_mse_swap = {}".format(j,cv_scores_mse_swap))


    
           print("started prediction for fold:{} after swap".format(j))
           for s in range(len(y_pred_swap)):
#
               datarow_s = []
               print("Printing  prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred_swap[s], y_test_swap[s],hmdb_id_swap[s],hydro_pos_swap[s]))
               if float(abs(y_pred_swap[s]-y_test_swap[s])) >= 1.0 :
                   datarow_s.append(hmdb_id_swap[s])
                   datarow_s.append(hydro_pos_swap[s])
                   datarow_s.append(y_test_swap[s])
                   datarow_s.append(y_pred_swap[s])
                   datarow_s.append(j+1)
                   writer.writerow(datarow_s)
                   print("outliers swap{} {}".format(hmdb_id_swap[s],hydro_pos_swap[s]))
    
           print("for th:{} cv  SWAP MAE  is:{} and r2_score_Swap:{}  and MSE: {}".format(j,cv_scores_swapped,r2_scores_swapped,mSError_inner_swap))
           print("for th:{} cv Train MAE  is:{} and r2_score:{} MSE is:{}".format(j,cv_scores_train[j],r2_scores_train[j],cv_scores_mse_train[j]))

#
#
           #########
           ###############
    
           ### this is using matplotlib #####
           # plt.plot(y_test_inner,y_pred_inner,'r+')
           # plt.plot([y_test_inner.min(),y_pred_inner.max()],[y_test_inner.min(),y_pred_inner.max()], 'b--', lw=2)
           # plt.title('True Vs Predicted', fontsize=18)
           # plt.xlabel('True')
           # plt.ylabel('Predicted')
           # plt.show()
           ###  #####
    
           #### this is using using plotly ##
           # file_name_graph = "/Users/zinatsayeeda/anaconda3/envs/rdkit/experimental_results/molecule_3/after_fold/fold"+str(j+1)+"/trueVspredict.html"
           file_name_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/fold"+str(j+1)+"/trueVspredict"
           draw(y_test_inner,y_pred_inner,file_name_graph)
#
#
           file_name_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/fold"+str(j+1)+"/trueVspredict_swap"
           draw(y_test_swap,y_pred_swap,file_name_graph)
#
#
           ########
    
           ##### this is using matplotlib ###
           # data_for_graph = pd.DataFrame(X_train_inner_temp['ChemicalShift'])
           # chemical_shift = data_for_graph['ChemicalShift'].values
           # chemical_shift = np.round(chemical_shift,2)
           # chemical_shift = chemical_shift*100
           # data_for_graph['ChemicalShift']=chemical_shift
           # sns.set(font_scale=0.5)
           # countplt=sns.countplot(x='ChemicalShift', data=data_for_graph, palette ='hls')
           # plt.title('Distribution of Chemical Shift Classes in training set', fontsize=18)
           # plt.xlabel('Chemical Shift Class', fontsize=16)
           # plt.ylabel('Frequency', fontsize=16)
           # plt.show()
           ######   #####
    
           ### this is using plotly #####
        #   data_for_graph = pd.DataFrame(X_train_inner_temp['ChemicalShift'])
        #   # table = FF.create_table(data_for_graph)
        #   # py.iplot(table, filename='chemical shift class frequency in training set')
        #   file_name_graph_training_class = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/holdout_dataset/holdout_600_95_7_prediction_chiral/fold"+str(j+1)+"/Chemicalshift-Training-freq-count.html"
        #   classfrequency(data_for_graph,file_name_graph_training_class)
        #   ####  ########
    #
    #
        #   min_chem_shift_train =  (data_for_graph['ChemicalShift'].values).min()
        #   max_chem_shift_train =  (data_for_graph['ChemicalShift'].values).max()
        #   print("for fold {} min Chemical Shift Classes in training set:{} and max Chemical Shift Classes in training set:{}".format(j,min_chem_shift_train,max_chem_shift_train))
    #
        #   #### this is using matplotlib #####
        #   # data_for_graph = pd.DataFrame(y_test_inner_temp['ChemicalShift'])
        #   # chemical_shift = data_for_graph['ChemicalShift'].values
        #   # chemical_shift = np.round(chemical_shift,2)
        #   # chemical_shift = chemical_shift*100
        #   # data_for_graph['ChemicalShift']=chemical_shift
        #   # sns.set(font_scale=0.5)
        #   # countplt=sns.countplot(x='ChemicalShift', data=data_for_graph, palette ='hls')
        #   # plt.title('Distribution of Chemical Shift Classes in test set', fontsize=18)
        #   # plt.xlabel('Chemical Shift Class', fontsize=16)
        #   # plt.ylabel('Frequency', fontsize=16)
        #   # plt.show()
        #   #####   #########
    #
        #   #### this is using plotly ####
        #   data_for_graph = pd.DataFrame(y_test_inner_temp['ChemicalShift'])
        #   file_name_graph_test_class = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/holdout_dataset/holdout_600_95_7_prediction_chiral/fold"+str(j+1)+"/Chemicalshift-Test-freq-count.html"
        #   classfrequency(data_for_graph,file_name_graph_test_class)
        #   ########
    #
        #   min_chem_shift_test =  (data_for_graph['ChemicalShift'].values).min()
        #   max_chem_shift_test =  (data_for_graph['ChemicalShift'].values).max()
           # print("for fold {} min Chemical Shift Classes in test set:{} and max Chemical Shift Classes in test set:{}".format(j,min_chem_shift_test,max_chem_shift_test))
      



        #final training
        # model = RandomForestClassifier(n_estimators=1000,max_depth=10) #change the value here
        print("AVG MAE from cross validation:{}".format(np.mean(cv_scores)))
        print("AVG MSE from cross validation:{}".format(np.mean(cv_scores_mse)))
        print("AVG R2 scores from cross validation:{}".format(np.mean(r2_scores)))

        print("AVG MAE from cross validation after swap:{}".format(np.mean(cv_scores_swapped)))
        print("AVG MSE from cross validation after swap:{}".format(np.mean(cv_scores_mse_swap)))
        print("AVG R2 scores from cross validation after swap:{}".format(np.mean(r2_scores_swapped)))

        print("AVG Train MAE from cross validation:{}".format(np.mean(cv_scores_train)))
        print("AVG Train MSE from cross validation:{}".format(np.mean(cv_scores_mse_train)))
        print("AVG Train R2 scores from cross validation:{}".format(np.mean(r2_scores_train)))


 
        # ##########
        # ##########
  
  
      
        #final training
        # model = RandomForestClassifier(n_estimators=500,max_depth=50)
        # model = RandomForestClassifier(n_estimators=800,max_depth=40)
        # model = RandomForestClassifier(n_estimators=600,max_depth=95)
        model = XGBRFRegressor(n_estimators= 600, max_depth= 25, subsample=0.9, colsample_bynode=0.05) #change the parameter here
        # model = RandomForestClassifier(n_estimators=700,max_depth=50)
        # model = RandomForestClassifier(n_estimators=1200,max_depth=70)
        # model = RandomForestClassifier(n_estimators=700,max_depth=95)
        # model = RandomForestClassifier(n_estimators=500,max_depth=55)
        # model = RandomForestClassifier(n_estimators=500,max_depth=70)
        # model = RandomForestClassifier(n_estimators=700,max_depth=35) #change the value here
        # model = RandomForestClassifier(n_estimators=600,max_depth=10)
        Y_training = np.ravel(Y_training)
        model.fit(X_training,Y_training)
        #### save model correct way ####
        # my_path = "/Volumes/Samsung_T5/Mac_Contents/rdkit/experiment_final_scripts/deleting_4_features/saved_models"
        # my_file = open(my_path, 'wb')
        # my_file = pickle.dump(model, my_file)
        # my_file.close()
        joblib.dump(model, '/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/NmrPred_XGBRFR.pkl')
        ##########
        #### Train set prediction####
        training_set_prediction = model.predict(X_training)
        training_set_prediction = training_set_prediction.astype(float)
        training_set_prediction = np.ravel(training_set_prediction)
        true_value_of_whole_train_set = Y_training.astype(float)
        true_value_of_whole_train_set = np.ravel(true_value_of_whole_train_set)
        training_set_mean_absolute_error = mean_absolute_error(true_value_of_whole_train_set, training_set_prediction)
        training_set_prediction_r2 = r2_score(true_value_of_whole_train_set, training_set_prediction)
        training_set_mse_error = mean_squared_error(true_value_of_whole_train_set, training_set_prediction)

        print(" WHOLE TAIN SET  MAE  is:{}".format(training_set_mean_absolute_error))
        print(" WHOLE TAIN SET  MSE  is:{} ".format(training_set_mse_error))
        print(" WHOLE TAIN SET  R2  is:{}".format(training_set_prediction_r2))

        file_name_holdout_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/trainerror/TrueVsPredict"
        draw_train(true_value_of_whole_train_set,training_set_prediction,file_name_holdout_graph)
    
       

        ##### SAVE MODELS #####
        # with open('/Users/venisuresh/opt/anaconda3/envs/rdkit/get_descriptor/Xuan_new_descriptor/Latest/saved_model_RFC_best_param_one_hot_700_35.pickle', 'wb') as handle:
        #   pickle.dump(saved_models, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
    
        dataset = pd.read_csv(testData)
        print("This is how the dataset looks like after converting to np array:{}".format(dataset))
        print("dataset shape:{}".format(dataset.shape))
        # dataset = dataset.drop(columns=['protonTotalPartialCharge2', 'protonTotalPartialCharge3', 'protonTotalPartialCharge4', 'protonTotalPartialCharge5','protonTotalPartialCharge2.1', 'protonTotalPartialCharge3.1', 'protonTotalPartialCharge4.1', 'protonTotalPartialCharge5.1','protonTotalPartialCharge2.2', 'protonTotalPartialCharge3.2', 'protonTotalPartialCharge4.2', 'protonTotalPartialCharge5.2','protonTotalPartialCharge2.3', 'protonTotalPartialCharge3.3', 'protonTotalPartialCharge4.3', 'protonTotalPartialCharge5.3'])
        dataset = dataset.drop(columns=['pepe', 'pepe.1', 'pepe.2', 'pepe.3','ipAtomicLearning', 'ipAtomicLearning.1', 'ipAtomicLearning.2', 'ipAtomicLearning.3'])
        dataset = dataset.drop(columns=['Unnamed: 0'])
        dataset = dataset.drop(columns=['Unnamed: 0.1'])
        # dataset= dataset[~dataset['HMDBID'].isin(['HMDB0000174_beta_L_fucose','HMDB00003254_beta_Muramic_acid'])]
        print("dataset shape after dropping partial charge:{}".format(dataset.shape))
        # dataset = dataset.dropna(axis=1,how='all')
        # print("dataset shape after dropping Nan columns:{}".format(dataset.shape))
        # dataset.fillna(999.0, inplace=True)
        # dataset[dataset.columns[0:-34]] = dataset[dataset.columns[0:-34]].astype(float)

        file_name_group_by_panda_series_holdout = dataset.groupby('HMDBID')
        file_name_group_by_dict_hold = dict(list(file_name_group_by_panda_series_holdout))
        file_name_hold = np.array(list(file_name_group_by_dict_hold.keys()))
        file_name_hold = file_name_hold.reshape(file_name_hold.shape[0],1)  # this is crossval holdout file names unique
        file_name_hold = np.ravel(file_name_hold)
        print("unique holdout file name to be predicted:{}".format(file_name_hold))

        hmdb_ids = dataset.iloc[ :,-2].values
        hmdb_ids = hmdb_ids.reshape(hmdb_ids.shape[0],1)
        hmdb_ids = np.ravel(hmdb_ids)

        hydrogen_positions = dataset.iloc[ :,-1].values
        hydrogen_positions = hydrogen_positions.reshape(hydrogen_positions.shape[0],1)
        print("nan value: {}".format(dataset.isnull().sum()))

        X = dataset.iloc[ : , 0:-3].values
        # dup = X[ :, -31:]
        # for i in range(dup.shape[0]):
        #   for j in range(dup.shape[1]):
        #     dup[i][j] = Decimal(dup[i][j])
        # X[ :, -31:] = dup
        # print("X data type 274:{}".format(type(X[0][274])))
        X = X.astype(float)
        print("X portion of dataset:{}".format(X))

        y =  dataset.iloc[ :,-3].values
        y = y.reshape(y.shape[0],1)
        y = y.astype(float)
        # Y = classLabel(y)
        Y = y
        # print("Y after classLabel:{}".format(Y))
        # Y = Y.reshape(Y.shape[0],1)
        # Y = Y.astype(int)


        X_test = X # change here . uncommit when pca is not used
           
        y_test = Y
        y_pred = model.predict(X_test)
    
        y_pred = y_pred.astype(float)
        # y_pred = y_pred/float(100)
        # y_pred = y_pred.reshape(y_pred.shape[0],1)
    
        y_test = y_test.astype(float)
        # y_test = y_test/float(100)
        y_test = np.ravel(y_test)
        meanAbsError = mean_absolute_error(y_test, y_pred)
        r2 = r2_score(y_test, y_pred)
        mse_holout = mean_squared_error(y_test, y_pred)
        print("y_test={}".format(y_test))
        print("y_pred={}".format(y_pred)) 

        # new_file = open("/Users/zinatsayeeda/anaconda3/envs/rdkit/experiment_after_1334ppm/prediction_print_latest_final.txt", "w")
        # outliers_hmdb_id = []
        # outliers_h_position = []
        hold_out_prediction_file_name = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/holdout_prediction.txt"
        hold_out_prediction_file = open(hold_out_prediction_file_name,"w")
        hold_out_prediction_file.write("Table\nAssignment")
        for k in range(len(y_pred)):

            data_row_holdout = []
            print("Printing holdout prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred[k], y_test[k],hmdb_ids[k],hydrogen_positions[k][0]))
            # new_file.write(str(hydrogen_positions[k][0])+"\t"+str(y_pred[k])+"\t"+str(hmdb_ids[k][0]+"\n"))
            if float(abs(y_pred[k]-y_test[k])) >= 1.0 :
                data_row_holdout.append(hmdb_ids[k])
                data_row_holdout.append(hydrogen_positions[k][0])
                data_row_holdout.append(y_test[k])
                data_row_holdout.append(y_pred[k])
                data_row_holdout.append("Holdout")
                writer.writerow(data_row_holdout)
                # outliers_hmdb_id.append(hmdb_ids[k][0])
                # outliers_h_position.append(hydrogen_positions[k][0])

            ### start creating holdout file####
            hold_out_prediction_file.write("\n"+str(hmdb_ids[k])+"\t"+str(hydrogen_positions[k][0])+"\t"+str(y_pred[k]))
        hold_out_prediction_file.close()
        print("MAE={}".format(meanAbsError))
        print("R2={}".format(r2))
        print("MSE={}".format(mse_holout))
        

        #### after swap MAE calculation #####
        for m in file_name_hold:
            print("started m={}".format(m))
            hold_out_prediction_file_name_before_swap = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/holdout/"+m
            hold_out_prediction_file_before_swap = open(hold_out_prediction_file_name_before_swap,"w")
            hold_out_prediction_file_before_swap.write("Table\nAssignment\tPred\tTest")
            index_of_file_name_inner = np.where(hmdb_ids == m)
            index_of_file_name_inner = index_of_file_name_inner[0]
            pred_test_values_per_file = {}
            control_var = 0
            for n in index_of_file_name_inner:
                pred_test_values_per_file[str(y_pred[n])+"_"+str(y_test[n])+"_"+str(control_var)] = int(hydrogen_positions[n][0])
                control_var = control_var +1
            sorted_pred_test_values_per_file = sorted(pred_test_values_per_file.items(), key=lambda x: x[1])
            print("sorted vlues for file{} = {}".format(m,sorted_pred_test_values_per_file))
            for v in sorted_pred_test_values_per_file:
                hold_out_prediction_file_before_swap.write("\n"+str(v[1])+"\t"+str(v[0].split("_")[0])+"\t"+str(v[0].split("_")[1]))
            hold_out_prediction_file_before_swap.close()
    
            f_sdf_path = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_dataset/"+m+".sdf"
            mol = Chem.MolFromMolFile(f_sdf_path,removeHs=False)
            # mol = Chem.AddHs(mol)
            # Chem.EmbedMolecule(mol)
            # Chem.MMFFOptimizeMolecule(mol)
    

            Chem.AssignAtomChiralTagsFromStructure(mol,replaceExistingTags=True)
            match = Chem.MolFromSmarts("[CX4H2]")  # for aldehyde
            # match = Chem.MolFromSmarts("[CX3;R0]=[CX3;R0]") # for alkene
    
            h_pos_per_file = []
            tag_per_file = []
            match_atoms = mol.GetSubstructMatches(match)
    
            if match_atoms:
                #collect tags and swap the file:
                for a in match_atoms:
                     # for each "C" the ids of "H are stored"
                    tags_per_neigh = []
                    centre_atom = mol.GetAtomWithIdx(a[0])
                    neigh_atoms = centre_atom.GetNeighbors()
                    idx_per_neigh = []
                    for nt in neigh_atoms:
                        idx_temp = nt.GetIdx()
                        sym = nt.GetSymbol()
                        if sym != "H": 
                            continue
                        else:
                            idx_per_neigh.append(idx_temp)
                            tags_per_neigh.append(chiral.ChiralCH2(f_sdf_path,idx_temp))
                            print("tags_per_neigh={}".format(tags_per_neigh))
                    if (len(set(tags_per_neigh)) == 1) or ( 3 in tags_per_neigh ) or ( 4 in tags_per_neigh ) or ( 0 in tags_per_neigh ):
                        print("no change required in the atom")
                    else:
                        print("changed required for:{}   tag:{}".format(idx_per_neigh,tags_per_neigh))
                        for ids in range(len(idx_per_neigh)):
                            h_pos_per_file.append(idx_per_neigh[ids])
                            tag_per_file.append(tags_per_neigh[ids])
    
            # hydro, shift = ReadShiftFile1(hold_out_prediction_file_name_before_swap)
            hold_out_prediction_file_name_after_swap = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/holdout/"+m+"_after_swap"
            hold_out_prediction_file_after_swap = open(hold_out_prediction_file_name_after_swap,"w")
            hold_out_prediction_file_after_swap.write("Table\nPosition\tPred\tTest")
            hydro, pred_shift, test_shift = ReadShiftFile1(hold_out_prediction_file_name_before_swap)
    
    
            if len(h_pos_per_file) >= 1:
                #change the file
    
                for hy in range(0,len(h_pos_per_file),2):
                    if hy != len(h_pos_per_file) -1:
                        position1 = h_pos_per_file[int(hy)]
                        position2 = h_pos_per_file[int(hy)+1]
                        tag1 = tag_per_file[int(hy)]
                        tag2 = tag_per_file[int(hy)+1]
                        index_of_greater_tag = None
                        index_of_lower_tag = None
                        if tag1 > tag2:
                            index_of_greater_tag = int(hy)
                            index_of_lower_tag = int(hy)+1
                        else:
                            index_of_greater_tag = int(hy)+1
                            index_of_lower_tag = int(hy)
                        ## decide which H position should have greater value    
                        h_pos_for_greater_value = h_pos_per_file[index_of_greater_tag]
                        h_pos_for_lower_value = h_pos_per_file[index_of_lower_tag]
    
                        if (h_pos_for_greater_value in hydro) and (h_pos_for_lower_value in hydro):
                            index_of_greater_hydrogen_in_before_swap_file = hydro.index(h_pos_for_greater_value)
                            index_of_lower_hydrogen_in_before_swap_file = hydro.index(h_pos_for_lower_value)
    
                            greater_hydrgen_predicted_value = pred_shift[index_of_greater_hydrogen_in_before_swap_file]
                            lower_hydrgen_predicted_value = pred_shift[index_of_lower_hydrogen_in_before_swap_file]
    
                            if greater_hydrgen_predicted_value >= lower_hydrgen_predicted_value:
                                print("value already greater")
                            else:
                                pred_shift[index_of_greater_hydrogen_in_before_swap_file] = lower_hydrgen_predicted_value
                                pred_shift[index_of_lower_hydrogen_in_before_swap_file] = greater_hydrgen_predicted_value
    
    
            else:
                # keep the file same
                print("no change required in the file")
            for i in range(len(hydro)):
                hold_out_prediction_file_after_swap.write("\n"+str(hydro[i])+"\t"+str(pred_shift[i])+"\t"+str(test_shift[i]))
            hold_out_prediction_file_after_swap.close()

        # calculate new MAE
        y_pred_swap = []
        y_test_swap = []
        hmdb_id_swap = []
        hydro_pos_swap = []
        for single_file in file_name_hold:
            print("started single file ={}".format(single_file))
            swap_chemical_path = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/holdout/"+single_file+"_after_swap"
            pos,pred,test = ReadShiftFile1(swap_chemical_path)
            for r in range(len(pos)):
                y_pred_swap.append(pred[r])
                y_test_swap.append(test[r])
                hmdb_id_swap.append(single_file)
                hydro_pos_swap.append(pos[r])
            print("for file={} the pred={} the test={}".format(single_file,pred,test))
    
        meanAbsError_swapped = mean_absolute_error(y_test_swap, y_pred_swap) # sum up of all the mae of y_pred and y_test
        
        r2_swap = r2_score(y_test_swap, y_pred_swap)
        mseError_swapped = mean_squared_error(y_test_swap,y_pred_swap)
        
        # print("y_test_inner:{} and y_pred_inner:{}".format(y_test_inner,y_pred_inner))
        
        
        for s in range(len(y_pred_swap)):
    
            datarow_s = []
            print("Printing  hold out prediction:{}------true:{}, HMDB_ID:{} and H position is:{}".format(y_pred_swap[s], y_test_swap[s],hmdb_id_swap[s],hydro_pos_swap[s]))
            if float(abs(y_pred_swap[s]-y_test_swap[s])) >= 1.0 :
                datarow_s.append(hmdb_id_swap[s])
                datarow_s.append(hydro_pos_swap[s])
                datarow_s.append(y_test_swap[s])
                datarow_s.append(y_pred_swap[s])
                datarow_s.append("holdout")
                writer.writerow(datarow_s)
                print("outliers swap{} {}".format(hmdb_id_swap[s],hydro_pos_swap[s]))
        
        print(" Holdout SWAP MAE  is:{} and SWAP r2_score:{} ".format(meanAbsError_swapped,r2_swap))







        #########
    
        #print("\n\n######   started printing outliers for Holdoutset #######\n\n")
    
        # print("outliers id:{} and position:{}".format(outliers_hmdb_id,outliers_h_position))
        # for l in range(len(outliers_hmdb_id)):
          # print("outliers id:{} and position:{}".format(outliers_hmdb_id[l],outliers_h_position[l]))
    
        # new_file.colse()
    
        ###### using matplotlib #####
        # plt.plot(y_test,y_pred,'r+')
        # plt.plot([y_test.min(),y_pred.max()],[y_test.min(),y_pred.max()], 'b--', lw=2)
        # plt.title('True Vs Predicted for Hold Out Set', fontsize=18)
        # plt.xlabel('True')
        # plt.ylabel('Predicted')
        # plt.show()
        #### ####
    
    
        #### using plotly #####
        file_name_holdout_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/holdout/TrueVsPredict"
        draw(y_test,y_pred,file_name_holdout_graph)
    
        file_name_holdout_graph = "/Users/zinatsayeeda/Documents/Thesis_always_use_this_folder/experimental_files/chapter_two/not_overfitting/XGBRFRegressor/holdout_prediction/holdout/TrueVsPredict_swapped"
        draw(y_test_swap,y_pred_swap,file_name_holdout_graph)
        ####  ########







def classLabel(y_value):      
  cls = [] 
  for i in range(1,1001): #for 1000 class        
    cls.append({"value": float(i)/float(100), "class": i})       
  y_value_temp = y_value[:]
  y_value_classlabel = np.zeros(len(y_value_temp))
  for i in range(len(y_value_temp)):
    y_value_temp[i] = np.round(y_value_temp[i],2) # changed it from 1 to 2
  cls_value = [cls[i]["value"] for i in range(len(cls))]
  cls_class = [cls[i]["class"] for i in range(len(cls))]
  cls_value_tuple = tuple(cls_value)
  cls_class_tuple = tuple(cls_class)
  cls_class_value_dict = dict(zip(cls_class_tuple,cls_value_tuple))
  for i in range(len(y_value_temp)):
    for k, v in cls_class_value_dict.items():
      if v == y_value_temp[i]:
        y_value_classlabel[i] = k
        break
      else:
        y_value_classlabel[i] = 1001.0 
  # print("y value has been transferred into class:{}".format(y_value_classlabel)) 
  return y_value_classlabel


def draw_without_regression(y_test_data,y_pred_data,file_name_graph):
    trace_data = []


    tracer_scatter = go.Scatter(x = y_test_data,y = y_pred_data,mode = 'markers',marker = dict(color = "OrangeRed"))
                        # mode = "lines + markers")
    tracer_line    = go.Scatter(x = y_test_data,y = y_test_data,mode = 'lines',line = dict(color = 'blue'))
    trace_data.append(tracer_scatter)
    trace_data.append(tracer_line)
    layout = go.Layout(title="True Vs Prediction",xaxis={'title':'True'},yaxis={'title':'Prediction'})

    prediction_fig = go.Figure(data=trace_data,layout=layout)

    plotly.offline.plot(prediction_fig, filename=file_name_graph,auto_open=False)
    print("R2 drawing is done")





def draw(y_test_data,y_pred_data,file_name_graph):

    # sns.regplot(x,y, scatter_kws={"color": "red", "alpha": 0.3}, line_kws={"color": "green"},marker="+",label="Training Data")

    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":1})
    data = {"test": y_test_data, "pred": y_pred_data}
    sns.set_style('ticks')
    df = pd.DataFrame(data)

    g = sns.lmplot(x = "test", y="pred", data =df,markers = "x",scatter_kws={"color": "red","alpha": 0.3},line_kws={"color":"blue","lw":3},ci=None)
    plt.xlabel("True Chemical Shift Value in ppm",fontweight ="bold")
    plt.ylabel("Predicted Chemical Shift Value in ppm",fontweight ="bold")
    plt.title("True Vs Predicted Chemical Shift",fontweight ="bold", color ="purple")
    plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    plt.yticks(np.arange(0.0, 11.0, 0.5))
    g.figure.set_size_inches(18.5, 10.5)
    sns.despine()
    plt.savefig(file_name_graph)

    print("graph save done")


    # plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    # plt.yticks(np.arange(0.0, 11.0, 0.5))
    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":0.5})
    # plt.xlabel("True Chemical Shift Value")
    # plt.ylabel("Predicted Chemical Shift Value")
    # plt.title("True Vs Predicted Chemical Shift")
    # plt.savefig(file_name_graph)

    # print("graph save done")


def draw_train(y_test_data,y_pred_data,file_name_graph):

    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red"}, line_kws={"color": "blue"},marker="+",label="True Vs Predicted Chemical Shift Plot for Using Training Dataset")
    # sns.regplot(np.ravel(y_test_data),np.ravel(y_pred_data), scatter_kws={"color": "red","alpha": 0.3},marker="+",line_kws={"color":"blue","alpha":0.4,"lw":1})

    data = {"test": y_test_data, "pred": y_pred_data}
    sns.set_style('ticks')
    df = pd.DataFrame(data)

    g = sns.lmplot(x = "test", y="pred", data =df,markers = "x",scatter_kws={"color": "red","alpha": 0.3},line_kws={"color":"blue","lw":3},ci=None)
    plt.xlabel("True Chemical Shift Value in ppm",fontweight ="bold")
    plt.ylabel("Predicted Chemical Shift Value in ppm",fontweight ="bold")
    plt.title("True Vs Predicted Chemical Shift Plot Using Training Dataset",fontweight ="bold", color ="purple")
    plt.xticks(np.arange(0.0, 11.0, 0.5),rotation=90)
    plt.yticks(np.arange(0.0, 11.0, 0.5))
    g.figure.set_size_inches(18.5, 10.5)
    sns.despine()
    plt.savefig(file_name_graph)

    print("graph save done")

def classfrequency(data_for_graph, file_name_frequency):
    x = data_for_graph['ChemicalShift'].values
    x = np.round(x,2)
    x = x*100
    x = x.tolist()
    # x_data = np.sort([str(r)+"c_s" for r in x])
    # trace = go.Histogram(x=[str(r)+"c_s" for r in x],
    trace = go.Histogram(x=x,
        xbins=dict(start=np.min(x),
            size=0.25,end=np.max(x)),
        marker=dict(color='rgb(25, 25, 100)'))
    # layout = go.Layout(title="Distribution of Chemical Shift Classes in training set",xaxis={'title':'Chemical Class','tickmode':'linear'},yaxis={'title':'Frequency'})
    layout = go.Layout(title="Distribution of Chemical Shift Classes in training set",xaxis=dict(title='Chemical Class'),yaxis=dict(title='Frequency'))
    fig = go.Figure(data=go.Data([trace]), layout=layout)
    # py.iplot(fig, filename=file_name_graph_training_class)
    plotly.offline.plot(fig, filename=file_name_frequency,auto_open=False)
    print("chemical shift drawing is done")


def ReadShiftFile1(file_name): # with path
  # print("h_position is:{} and length:{}".format(h_position,len(h_position)))
  H_position_and_chemicalshift_in_shift_file = [] # this returns after checking "O" and "N" as neighbor atoms
  
  hydrogen = []
  shift_pred = []
  shift_true = []
  with open(file_name, "r") as fp:
    lines = fp.readlines()
    for i in range(2,len(lines)):
      splited_line = lines[i].split("\t")
      len_of_splitted_line = len(splited_line)
      # print("H:{}".format(splited_line[0].strip()))
      # print("S:{}".format(splited_line[len_of_splitted_line-1].strip()))
      hydrogen.append(int(splited_line[0].strip()))
      shift_temp1 = float(splited_line[len_of_splitted_line-2].strip())
      shift_temp2 = float(splited_line[len_of_splitted_line-1].strip())
      shift_pred.append(round(shift_temp1,2))
      shift_true.append(round(shift_temp2,2))
  
  print("returning H_position:{} , predicted chemicalshift:{}, True chemicalshift:{}".format(hydrogen,shift_pred,shift_true))
  return hydrogen, shift_pred, shift_true









if __name__ == '__main__':
  main()
