
# updated this file so that the 2D and holdout dataset is not calculated




from get_descriptor import get_descriptor_atom
import os
import sys



def main():
  #sdf_path = "/mnt/c/Users/Danis/Desktop/get_descriptor/HMDB00453.sdf"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_1/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_2/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_3/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_4/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_2nd_priority/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_priority/"
  #sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_priority_with_all_atoms/"
  # sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"
  # sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/with_ion_map/"
  # sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_neighbor/"
  #sdf_path = "/cshome/zinat/hmdb_3d_batch2/6/"
  #sdf_path = "/cshome/zinat/hmdbmol/jeol/JEOL_molfiles/"
  #sdf_path = "/cshome/zinat/hmdbmol/get_descriptor/Xuan_new_descriptor/Latest/non_lipid_comp_3d_mol/"
  sdf_path = "/cshome/zinat/hmdbmol/dft_sdf/"
  #sdf_path = "/cshome/zinat/hmdbmol/"
  # sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/dataset/dataset_1st_2nd_3rd_4th_partial_with_all_atoms/"
  # sdf_path = "/Users/zinatsayeeda/anaconda3/envs/rdkit/test_5/test_5_xuan_new_descriptor_all_features_2/"

  nearest_atom = 3
  atom = "H"
  #f = open("/Users/zinatsayeeda/anaconda3/envs/rdkit/test_1/test.out", 'w')
  #sys.stdout = f
  included_compound =[]
  #with open("/cshome/zinat/hmdbmol/readfile_6.txt", "r") as fp:
  #with open("/cshome/zinat/hmdbmol/jeol/mofile.txt", "r") as fp:
  #with open("/cshome/zinat/hmdbmol/get_descriptor/Xuan_new_descriptor/Latest/non_lipid_comp_3d_mol.txt", "r") as fp:
  with open("/cshome/zinat/hmdbmol/dft.txt", "r") as fp:
  # with open("/Users/zinatsayeeda/anaconda3/envs/rdkit/bad_predicted_atoms.tsv", "r") as fp:
    lines = fp.readlines()
    for line in lines:
      included_compound.append(line.split("\n")[0])
  print("included_compound={}".format(included_compound))
  for fileEntry in os.listdir(sdf_path):
    # print(fileEntry)
    if fileEntry.endswith( ".sdf" ):
      file_name = fileEntry.split(".sdf")[0]
      #print("file entry in create structure:{}".format(fileEntry))
      #sys.stdout.flush()
      # if file_name not in excluded_compound:
      # if file_name in excluded_compound: 
      #if (file_name  in included_compound) and (file_name not in excluded_compound): #while doing for hold out set remove "not"
      if (file_name  in included_compound):
        print("file entry in create structure:{}".format(fileEntry))
        sys.stdout.flush()
        full_sdf_path = sdf_path+fileEntry
        get_descriptor_atom(full_sdf_path,nearest_atom,file_name,atom)
        sys.stdout.flush()
      
  #f.close()   



if __name__ == '__main__':
	main()
