import torch
from torch import nn
from torch_geometric.nn import TransformerConv
import torch_geometric
#from .create_dataset import GraphDataset

import rdkit
from rdkit import Chem
from rdkit.Chem import AllChem, rdMolDescriptors

import numpy as np
from itertools import combinations
import pandas as pd
import torch.utils.data
import torch.nn.functional as F
# from mordred import Calculator, descriptors
# from padelpy import from_smiles


class GraphTransformer(nn.Module):
    def __init__(self, num_node_features, num_edge_features, hidden_channels, num_layers, dropout=0.3):
        super(GraphTransformer, self).__init__()

        self.conv1 = TransformerConv(num_node_features, hidden_channels, heads=4, concat=True)
        self.convs = nn.ModuleList()
        for i in range(num_layers - 1):
            self.convs.append(TransformerConv(hidden_channels * 4, hidden_channels, heads=4, concat=True))
        self.lin1 = nn.Linear(hidden_channels * 4, hidden_channels)
        self.lin2 = nn.Linear(hidden_channels, 1)

        self.lns_conv = nn.ModuleList([nn.LayerNorm(hidden_channels*4) for _ in range(num_layers)])
        self.ln1 = nn.LayerNorm(hidden_channels*1)

        self.dropout = nn.Dropout(dropout)

    def forward(self, x, edge_index):
        x = self.conv1(x, edge_index)
        x = self.lns_conv[0](x)
        x = x.relu()
        x = self.dropout(x)
        h = x
        for i, conv in enumerate(self.convs):
            x = conv(x, edge_index)
            x = self.lns_conv[i+1](x)
            x = x.relu()
            x = self.dropout(x)
            x = h + x
            h = x
        x = self.lin1(x)
        x = self.ln1(x)
        x = x.relu()
        x = self.dropout(x)
        x = self.lin2(x)
        return x



# class GraphTransformer(nn.Module):
#     def __init__(self, num_node_features, num_edge_features, hidden_channels, num_layers):
#         super(GraphTransformer, self).__init__()
#         self.conv1 = TransformerConv(num_node_features, hidden_channels, heads=4, concat=True)
#         self.convs = nn.ModuleList()
#         for i in range(num_layers - 1):
#             self.convs.append(TransformerConv(hidden_channels * 4, hidden_channels, heads=4, concat=True))
#         self.lin1 = nn.Linear(hidden_channels * 4, hidden_channels)
#         self.lin2 = nn.Linear(hidden_channels, 1)

#     def forward(self, x, edge_index):
#         x = self.conv1(x, edge_index)
#         x = x.relu()
#         for conv in self.convs:
#             x = conv(x, edge_index)
#             x = x.relu()
#         x = self.lin1(x)
#         x = x.relu()
#         x = self.lin2(x)
#         return x


from torch.nn.utils.rnn import pad_sequence


from rdkit import Chem
from rdkit.Chem import rdMolDescriptors

def generate_features(rdkit_mols, max_atoms, num_bonds=50, num_feats=58):
    atomic_features_list = []
    for mol in rdkit_mols:
        if mol is not None:
            num_atoms = mol.GetNumAtoms()
            atom_features_list = []
            for atom in mol.GetAtoms():
                features = [0] * num_feats

                # atom type
                features[atom.GetAtomicNum() - 1] = 1

                # hybridization
                hybridization = atom.GetHybridization()
                features[9 + hybridization] = 1

                # degree
                degree = atom.GetDegree()
                features[15 + degree] = 1

                # number of pi electrons
                n_pi = atom.GetNumRadicalElectrons()
                features[21 + n_pi] = 1

                # number of lone pairs
                n_lone_pairs = atom.GetNumImplicitHs()
                features[25 + n_lone_pairs] = 1

                # formal charge
                formal_charge = atom.GetFormalCharge()
                features[31 + formal_charge] = 1

                # in-ring
                in_ring = atom.IsInRing()
                features[39 + int(in_ring)] = 1

                # number of bonds to an atom with atomic number > 7
                num_bonds_higher_than_7 = 0
                for neighbor in atom.GetNeighbors():
                    if neighbor.GetAtomicNum() > 7:
                        num_bonds_higher_than_7 += 1
                features[41 + num_bonds_higher_than_7] = 1

                # Morgan fingerprint
                radius = 2
                morgan = list(rdMolDescriptors.GetMorganFingerprintAsBitVect(mol, radius, nBits=num_bonds, 
                  useFeatures=True, invariants=[atom.GetIdx()]*num_atoms).ToBitString())
                morgan = [int(bit) for bit in morgan]
                features += morgan

                atom_features_list.append(features)
                

            # Pad the list of atomic features to the maximum number of atoms
            atom_features_list += [[0] * (num_feats + num_bonds)] * (max_atoms - num_atoms)

            # Append the list of atomic features to the list of atomic features for all molecules
            atomic_features_list.append(atom_features_list)

    # Convert the list of atomic features for all molecules to a tensor
    atomic_features_tensor = torch.tensor(atomic_features_list, dtype=torch.float32)

    # Reshape the tensor to (num_molecules, max_atoms, num_features)
    atomic_features_tensor = atomic_features_tensor.view(-1, max_atoms, num_feats + num_bonds)

    return atomic_features_tensor




def mask_shifts(df):
  # Create a list to store the chemical shifts for each molecule
  shifts_list = []

  # Loop over each molecule and construct a list of chemical shifts for the atoms in the molecule
  for i, row in df.iterrows():
      shifts_dict = row['labels']
      num_atoms = row['num_atoms']
      # Create a list of chemical shifts for the atoms in the molecule
      shifts = []
      for atom_idx in range(num_atoms):
          # Check if the current atom has a chemical shift
          if atom_idx in shifts_dict:
              shifts.append(shifts_dict[atom_idx])
          else:
              # If the atom does not have a chemical shift, append a dummy value (e.g., 0)
              shifts.append(0)
      # Append the list of chemical shifts to the shifts_list
      shifts_list.append(shifts)

  max_atoms = df['num_atoms'].max()
  # Pad the sequences in the shifts_list with a dummy value to make them all the same length
  shifts_padded = pad_sequence([torch.tensor(shifts) for shifts in shifts_list], batch_first=True, padding_value=0)

  # Create a new mask tensor that has the same shape as shifts_padded and contains ones for valid atoms and zeros for padded atoms
  mask_padded = torch.zeros(shifts_padded.shape)
  for i, shifts in enumerate(shifts_list):
      for j, shift in enumerate(shifts):
          if shift != 0:
              mask_padded[i, j] = 1

  # Convert mask_padded to a boolean tensor
  mask = mask_padded.long()
  # Pad the mask tensor
  #mask = nn.functional.pad(mask, pad=(0, max_atoms - mask.shape[1]))

  return shifts_padded, mask

def create_graph_data(mols):
    # Initialize the edge adjacency matrix
    num_molecules = len(mols)
    max_atoms = max([mol.GetNumAtoms() for mol in mols])
    edge_adj_matrix = np.zeros((num_molecules, max_atoms, max_atoms))

    # Loop over each molecule and construct the edge adjacency matrix
    for i, mol in enumerate(mols):
        if mol is not None:
            # Extract the atom indices and distances for the edges
            dist_mat = Chem.Get3DDistanceMatrix(mol)
            for j, k in combinations(range(mol.GetNumAtoms()), 2):
                if dist_mat[j, k] <= 2:
                    edge_adj_matrix[i, j, k] = 1
                    edge_adj_matrix[i, k, j] = 1

    # Generate a list of atomic features for each molecule
    atomic_features_list = generate_features(mols, max_atoms)

    return atomic_features_list, edge_adj_matrix


def collate_fn(batch):
    # Pad the input data and labels to the maximum number of atoms in the batch
    x = [data[0] for data in batch]
    y = [data[1] for data in batch]
    adj = [data[2] for data in batch]
    mask = [data[3] for data in batch]
    num_atoms = [data[1].shape[0] for data in batch]
    max_atoms = max(num_atoms)

    # Pad the labels tensor to have the same number of atoms for all samples in the batch
    y_padded = torch.zeros(len(batch), max_atoms)
    for i in range(len(batch)):
        y_padded[i, :num_atoms[i]] = y[i]

    # Pad the input data, adjacency, and mask tensors
    x_padded = torch.zeros(len(batch), max_atoms, x[0].shape[1])
    adj_padded = torch.zeros(len(batch), max_atoms, max_atoms)
    mask_padded = torch.zeros(len(batch), max_atoms)
    for i in range(len(batch)):
        x_padded[i, :num_atoms[i], :] = x[i]
        adj_padded[i, :num_atoms[i], :num_atoms[i]] = torch.tensor(adj[i], dtype=torch.int64)
        mask_padded[i, :num_atoms[i]] = mask[i]

    y_masked = y_padded * mask_padded
    edge_index = []
    edge_attr = []
    for i in range(len(batch)):
        edge = adj_padded[i].nonzero(as_tuple=False)
        row, col = edge[:, 0], edge[:, 1]
        edge_index.append(torch.stack([row, col], dim=0))
        edge_attr.append(torch.tensor(adj_padded[i, row, col]))
    edge_index = torch.cat(edge_index, dim=1)
    edge_attr = torch.cat(edge_attr, dim=0).unsqueeze(-1)
    edge_attr = edge_attr.unsqueeze(-1)
    mask_padded = mask_padded.unsqueeze(-1)
    y_masked = y_masked.unsqueeze(-1)

    # print('Adjacency matrix padded', adj_padded.shape)
    # print('Edge attr shape', edge_attr.shape)
    # print('Edge index shape', edge_index.shape)
    # print('Mask padded shape', mask_padded.shape)
    # print('X padded shape', x_padded.shape)
    # print('Y padded shape', y_padded.shape)

    # Apply the mask to the padded labels tensor
    ##y_masked = y_padded * mask_padded

    return x_padded, y_masked, edge_index, edge_attr, mask_padded.long()




from torch_geometric.utils import to_dense_batch
best_model = None

def train_graph_transformer(train_data_loader, valid_data_loader, model, optimizer, num_epochs=100, batch_size=1):
    # Train the model
    best_val_loss = float('inf')
    for epoch in range(num_epochs):
        model.train()
        total_loss = 0.0
        for x, y, edge_indices, edge_attrs, train_mask in train_data_loader:
            # Forward pass
            out = model(x, edge_indices)#, edge_attrs)
            #loss = nn.functional.mse_loss(out, y)
            loss = mse_loss(out, y, train_mask)
            total_loss += loss.item() * x.shape[0]

            # Backward pass
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        avg_train_loss = total_loss / len(train_data_loader)

        # Evaluate the model on the validation set
        model.eval()
        with torch.no_grad():
            total_loss = 0.0
            for x, y, edge_indices, edge_attrs, valid_mask in valid_data_loader:
                out = model(x, edge_indices)#, edge_attrs)
                #loss = nn.functional.mse_loss(out, y)
                loss = mse_loss(out, y, valid_mask)
                total_loss += loss.item() * x.shape[0]

            # Compute the average validation loss
            avg_valid_loss = total_loss / len(valid_data_loader)
            if avg_valid_loss < best_val_loss:
              best_val_loss = avg_valid_loss
              best_model = model.state_dict()
              test(model, test_loader)

        print(f"Epoch {epoch + 1}/{num_epochs}: validation loss={avg_valid_loss:.4f}")
        print(f"Epoch {epoch + 1}/{num_epochs}: train loss={avg_train_loss:.4f}")

    return model


def test(model, loader, best_model=None):
   # Evaluate the model on the validation set
    model.load_state_dict(best_model) if best_model else None
    model.eval()
    with torch.no_grad():
        total_loss = 0.0
        for x, y, edge_indices, edge_attrs, mask in loader:
            out = model(x, edge_indices)#, edge_attrs)
            #loss = nn.functional.mse_loss(out, y)
            loss = mse_loss(out, y, mask)
            total_loss += loss.item() * x.shape[0]

        # Compute the average validation loss
        avg_loss = total_loss / len(loader)
      
    print(f"Test loss={avg_loss:.4f}")



def mse_loss(y_pred, y_true, atom_indices):
    #atom_indices = atom_indices.unsqueeze(0)  # Add a new dimension at the beginning
    #print(y_true.nonzero())
    mask = atom_indices.bool().squeeze(2)  # Convert atom_indices to a boolean mask
    y_pred = y_pred.squeeze(2)[mask]  # Select relevant elements of y_pred
    y_true = y_true.squeeze(2)[mask]  # Select relevant elements of y_true
    #loss = F.mse_loss(y_pred, y_true)
    loss_fn = nn.L1Loss()
    loss = loss_fn(y_pred, y_true)
    # print('y_pred y_true', y_pred, y_true)
    # print(loss)
    return loss


def create_data_loader(df, batch_size=1):
  # Create the train and validation graph data
  atomic_features, edge_adj_matrix = create_graph_data(df['mol'])
  x = atomic_features
  # Create the train and validation label tensors
  y, mask = mask_shifts(df)
  graph_dataset = GraphDataset(x, y, edge_adj_matrix, mask)
  return DataLoader(graph_dataset, batch_size=batch_size, shuffle=True, collate_fn=collate_fn)


# Create train, validation and test dataset
train_loader = create_data_loader(train_df)
valid_loader = create_data_loader(valid_df)
test_loader  = create_data_loader(test_df)

# Define the hyperparameters
num_node_features = 108
num_edge_features = 1
hidden_channels = 64
num_layers = 4
num_epochs = 500
batch_size = 1
learning_rate = 0.001

# Create the model
model = GraphTransformer(num_node_features, num_edge_features, hidden_channels, num_layers)

# Define the optimizer
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

total_params = sum(p.numel() for p in model.parameters())
print(f"Number of parameters: {total_params}")

# Train the model
model = train_graph_transformer(train_loader, valid_loader, model, optimizer, 
                                num_epochs=num_epochs, batch_size=batch_size)


test(model, best_model, test_loader)

