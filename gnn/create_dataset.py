import pandas as pd
from rdkit import Chem

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-valid-num', '--num_valid_samples', type=int, choices=range(1,200), default=60)
args = parser.parse_args()

from torch.utils.data import Dataset


class GraphDataset(Dataset):
    def __init__(self, x, y, edge_adj_matrix, mask):
        self.x = x
        self.y = y
        self.edge_adj_matrix = edge_adj_matrix
        self.mask = mask

    def __getitem__(self, index):
        x = self.x[index]
        y = self.y[index]
        edge_adj = self.edge_adj_matrix[index]
        mask = self.mask[index]
        return x, y, edge_adj, mask

    def __len__(self):
        return self.x.shape[0]


def create_dataset(sdf_file, shift_file):
  df_shifts = pd.read_csv(shift_file, dtype={'atom_index': int, 'Shift': float})
  suppl = Chem.ForwardSDMolSupplier(sdf_file, removeHs=False)

  data = []
  for mol in suppl:
    if mol is not None:
      # Extract the mol_id from the molecule name
      mol_id = str(mol.GetProp('_Name'))

      # Filter out non-Hydrogen atoms
      atom_indexes = [atom.GetIdx() for atom in mol.GetAtoms() if atom.GetSymbol() == 'H']

      # Extract the chemical shifts for the Hydrogen atoms
      shifts = df_shifts.loc[df_shifts['mol_id'] == mol_id]
      shifts = shifts.loc[shifts['atom_index'].isin(atom_indexes)]
      shifts = shifts[['atom_index', 'Shift']].set_index('atom_index')['Shift'].to_dict()

      # Construct a dictionary with the molecular information and chemical shifts
      mol_dict = {'mol_id': mol_id, 'mol': mol, 'num_atoms': len(mol.GetAtoms()), 'smiles': Chem.MolToSmiles(mol)}
      mol_dict.update({'labels': shifts})
      data.append(mol_dict)

  # Construct a dataframe with one row for each molecule in the batch
  return pd.DataFrame(data).dropna()

def split_train_validation_sets(dataset):
  valid = dataset.sample(n=args.num_valid_samples, random_state=125)
  train = dataset[~dataset.mol_id.isin(valid.mol_id)]

  valid.to_pickle('valid.pkl.gz', compression='gzip')
  train.to_pickle('train.pkl.gz', compression='gzip')

if __name__ == '__main__':
  shift_file = '../app/dataset/wishart_train_H_shifts.csv'
  sdf_file = '../app/dataset/wishart_train_H.sdf'

  # Create dataset and split it into train and validation sets pickle files
  dataset = create_dataset(sdf_file, shift_file)
  print(dataset.head())
  split_train_validation_sets(dataset)
